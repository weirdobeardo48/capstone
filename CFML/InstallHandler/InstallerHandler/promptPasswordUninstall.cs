﻿using InstallerHandler.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace InstallerHandler
{
    public partial class promptPasswordUninstall : Form
    {
        private readonly string appID = "{A982188C-8659-4190-B96E-A4E5763A2161}";
        
        public promptPasswordUninstall()
        {
            InitializeComponent();
            this.Icon = Resources.AppIcon;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat"))
            {
                if (Common.Common.readRegistryIfInitialKeyIsSet() == true)
                {
                    //CustomNotificationBox cnbox = new CustomNotificationBox("Masterpassword is corrupted", AlertType.Info);
                    //cnbox.Visible = true;
                    MessageBox.Show("Masterpassword is corrupted");
                    return;
                }
                else
                {
                    doUninstall();
                    try
                    {
                        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat");

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    try
                    {
                        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\loggegInUser.dat");

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    removeRegistry();
                    MessageBox.Show("CFML is now uninstalled");
                }


            }
            else
            {
                if (Common.Common.readRegistryIfInitialKeyIsSet() == false)
                {
                    MessageBox.Show("Masterpassword is corrupted");
                    return;
                }
                try
                {
                    StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat");
                    string line = sr.ReadLine();
                    sr.Close();
                    if (line != null)
                    {
                        if (line.Equals(Common.Common.CreateMD5(inpMasterPassword.Text + "5154bfd9725040c54f5f69c70c61bdc3")))
                        {
                            doUninstall();
                            try
                            {
                                Directory.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            removeRegistry();
                            MessageBox.Show("CFML is now uninstalled");
                        }
                        else
                        {
                            MessageBox.Show("Wrong password");
                            Console.WriteLine("Wrong password");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

        }
        private void doUninstall()
        {
            Process process = new Process();
            process.StartInfo.FileName = "msiexec";
            process.StartInfo.WorkingDirectory = @"C:\temp\";
            process.StartInfo.Arguments = " /x " + appID;
            process.StartInfo.Verb = "runas";
            process.Start();
            process.WaitForExit(60000);
        }
        private void removeRegistry()
        {
            RegistryKey key = null;
            try
            {
                Registry.LocalMachine.DeleteSubKey("Software\\Wow6432Node\\T-Team");
            }
            catch (Exception ex) { Console.WriteLine(ex); }
            finally { key.Close(); }
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private void inpMasterPassword_Enter(object sender, EventArgs e)
        {
            inpMasterPassword.SelectAll();
            pnMasterPasswordBg.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void inpMasterPassword_Leave(object sender, EventArgs e)
        {
            pnMasterPasswordBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void pnMasterPasswordBg_Click(object sender, EventArgs e)
        {
            inpMasterPassword_Enter(null, null);
        }

        private void inpMasterPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOK_Click(null, null);
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
    }
}
