﻿namespace InstallerHandler
{
    partial class promptPasswordUninstall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(promptPasswordUninstall));
            this.btnQuit = new System.Windows.Forms.PictureBox();
            this.btnOK = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label1 = new System.Windows.Forms.Label();
            this.pnMasterPasswordBg = new System.Windows.Forms.Panel();
            this.inpMasterPassword = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).BeginInit();
            this.pnMasterPasswordBg.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnQuit
            // 
            this.btnQuit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuit.BackgroundImage")));
            this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQuit.Location = new System.Drawing.Point(288, 12);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(56, 25);
            this.btnQuit.TabIndex = 7;
            this.btnQuit.TabStop = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // btnOK
            // 
            this.btnOK.ActiveBorderThickness = 1;
            this.btnOK.ActiveCornerRadius = 20;
            this.btnOK.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnOK.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnOK.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnOK.BackColor = System.Drawing.SystemColors.Control;
            this.btnOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOK.BackgroundImage")));
            this.btnOK.ButtonText = "OK";
            this.btnOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOK.Font = new System.Drawing.Font("Lato", 12F);
            this.btnOK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnOK.IdleBorderThickness = 1;
            this.btnOK.IdleCornerRadius = 20;
            this.btnOK.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnOK.IdleForecolor = System.Drawing.Color.White;
            this.btnOK.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnOK.Location = new System.Drawing.Point(116, 169);
            this.btnOK.Margin = new System.Windows.Forms.Padding(5);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(119, 43);
            this.btnOK.TabIndex = 8;
            this.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lato Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(57, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Enter password to uninstall CFML";
            // 
            // pnMasterPasswordBg
            // 
            this.pnMasterPasswordBg.BackgroundImage = global::InstallerHandler.Properties.Resources.TextBoxBg;
            this.pnMasterPasswordBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnMasterPasswordBg.Controls.Add(this.inpMasterPassword);
            this.pnMasterPasswordBg.Location = new System.Drawing.Point(45, 109);
            this.pnMasterPasswordBg.Name = "pnMasterPasswordBg";
            this.pnMasterPasswordBg.Size = new System.Drawing.Size(268, 45);
            this.pnMasterPasswordBg.TabIndex = 15;
            this.pnMasterPasswordBg.Click += new System.EventHandler(this.pnMasterPasswordBg_Click);
            // 
            // inpMasterPassword
            // 
            this.inpMasterPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpMasterPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpMasterPassword.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpMasterPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpMasterPassword.Location = new System.Drawing.Point(13, 12);
            this.inpMasterPassword.Name = "inpMasterPassword";
            this.inpMasterPassword.PasswordChar = '•';
            this.inpMasterPassword.Size = new System.Drawing.Size(242, 16);
            this.inpMasterPassword.TabIndex = 1;
            this.inpMasterPassword.Enter += new System.EventHandler(this.inpMasterPassword_Enter);
            this.inpMasterPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inpMasterPassword_KeyDown);
            this.inpMasterPassword.Leave += new System.EventHandler(this.inpMasterPassword_Leave);
            // 
            // promptPasswordUninstall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 231);
            this.Controls.Add(this.pnMasterPasswordBg);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnQuit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "promptPasswordUninstall";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enter password to uninstall CFML";
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).EndInit();
            this.pnMasterPasswordBg.ResumeLayout(false);
            this.pnMasterPasswordBg.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox btnQuit;
        private Bunifu.Framework.UI.BunifuThinButton2 btnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnMasterPasswordBg;
        private System.Windows.Forms.TextBox inpMasterPassword;
    }
}