﻿using CFML.Capture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    class WindowSnapModel : IComparable<WindowSnapModel>
    {
        private int zIndex;
        private WindowSnap wdSnap;

        public WindowSnapModel()
        {
        }

        public WindowSnapModel(int zIndex, WindowSnap wdSnap)
        {
            this.zIndex = zIndex;
            this.wdSnap = wdSnap;
        }

        public int ZIndex
        {
            get { return zIndex; }
            set { zIndex = value; }
        }

        public WindowSnap WdSnap
        {
            get { return wdSnap; }
            set { wdSnap = value; }
        }

        public int CompareTo(WindowSnapModel other)
        {
            return zIndex.CompareTo(other.zIndex);
        }
    }
}
