﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Configuration
{
    public class Configuration
    {
        private int censorSensitivity;
        private int maximumCensoredObject;
        private bool enableObjectTracking;
        private int objectTrackingScanInterval;
        private bool enableRealTimeMode;
        private int disabledRealTimeScanInterval;
        private bool enableMuteSound;
        private bool checkInactiveApplication;
        private bool censorBgImageEnable;
        private string censorBgImageName;
        private string censorBgColor;
        private bool privacyCheck;
//        censor-bg-picture-enable = 0
//censor-bg-picture-name = CensorBg.png
//censor-bg-color = #000000 

        public Configuration(int censorSensitivity, int maximumCensoredObject, bool enableObjectTracking, int objectTrackingScanInterval, bool enableRealTimeMode, int disabledRealTimeScanInterval, bool enableMuteSound, bool checkInactiveApplication, bool censorBgImageEnable, string censorBgImageName, string censorBgColor)
        {
            this.censorSensitivity = censorSensitivity;
            this.maximumCensoredObject = maximumCensoredObject;
            this.enableObjectTracking = enableObjectTracking;
            this.objectTrackingScanInterval = objectTrackingScanInterval;
            this.enableRealTimeMode = enableRealTimeMode;
            this.disabledRealTimeScanInterval = disabledRealTimeScanInterval;
            this.enableMuteSound = enableMuteSound;
            this.checkInactiveApplication = checkInactiveApplication;
            this.censorBgImageEnable = censorBgImageEnable;
            this.censorBgImageName = censorBgImageName;
            this.censorBgColor = censorBgColor;
        }

        public Configuration()
        {
        }

        public bool CensorBgImageEnable
        {
            get
            {
                return censorBgImageEnable;
            }

            set
            {
                censorBgImageEnable = value;
            }
        }

        public string CensorBgImageName
        {
            get
            {
                return censorBgImageName;
            }

            set
            {
                censorBgImageName = value;
            }
        }

        public string CensorBgColor
        {
            get
            {
                return censorBgColor;
            }

            set
            {
                censorBgColor = value;
            }
        }

        public int CensorSensitivity
        {
            get
            {
                return censorSensitivity;
            }

            set
            {
                censorSensitivity = value;
            }
        }

        public int MaximumCensoredObject
        {
            get
            {
                return maximumCensoredObject;
            }

            set
            {
                maximumCensoredObject = value;
            }
        }

        public bool EnableObjectTracking
        {
            get
            {
                return enableObjectTracking;
            }

            set
            {
                enableObjectTracking = value;
            }
        }

        public int ObjectTrackingScanInterval
        {
            get
            {
                return objectTrackingScanInterval;
            }

            set
            {
                objectTrackingScanInterval = value;
            }
        }

        public bool EnableRealTimeMode
        {
            get
            {
                return enableRealTimeMode;
            }

            set
            {
                enableRealTimeMode = value;
            }
        }

        public int DisabledRealTimeScanInterval
        {
            get
            {
                return disabledRealTimeScanInterval;
            }

            set
            {
                disabledRealTimeScanInterval = value;
            }
        }

        public bool EnableMuteSound
        {
            get
            {
                return enableMuteSound;
            }

            set
            {
                enableMuteSound = value;
            }
        }

        public bool CheckInactiveApplication
        {
            get
            {
                return checkInactiveApplication;
            }

            set
            {
                checkInactiveApplication = value;
            }
        }

        public bool PrivacyCheck
        {
            get
            {
                return privacyCheck;
            }

            set
            {
                privacyCheck = value;
            }
        }
    }
}
