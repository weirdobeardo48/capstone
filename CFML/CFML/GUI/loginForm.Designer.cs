﻿namespace CFML.GUIDesign
{
    partial class loginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(loginForm));
            this.bunifuElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.lbLoginTitle = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lbWelcomeNote = new System.Windows.Forms.Label();
            this.formDragControl = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.pnLoginContainer = new System.Windows.Forms.Panel();
            this.lbRegisterLink = new System.Windows.Forms.LinkLabel();
            this.pnLoginOption = new System.Windows.Forms.Panel();
            this.btnLoginAsGuest = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnLoginAsMember = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lbForgotPasswordLink = new System.Windows.Forms.LinkLabel();
            this.btnBackToLoginOption = new System.Windows.Forms.PictureBox();
            this.btnQuit = new System.Windows.Forms.PictureBox();
            this.btnLoginBtn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnLoginOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBackToLoginOption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse
            // 
            this.bunifuElipse.ElipseRadius = 5;
            this.bunifuElipse.TargetControl = this;
            // 
            // lbLoginTitle
            // 
            this.lbLoginTitle.AutoSize = true;
            this.lbLoginTitle.Font = new System.Drawing.Font("Lato", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbLoginTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbLoginTitle.Location = new System.Drawing.Point(46, 109);
            this.lbLoginTitle.Name = "lbLoginTitle";
            this.lbLoginTitle.Size = new System.Drawing.Size(49, 18);
            this.lbLoginTitle.TabIndex = 3;
            this.lbLoginTitle.Text = "Login";
            // 
            // lbWelcomeNote
            // 
            this.lbWelcomeNote.AutoSize = true;
            this.lbWelcomeNote.Font = new System.Drawing.Font("Lato", 9.75F);
            this.lbWelcomeNote.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbWelcomeNote.Location = new System.Drawing.Point(46, 138);
            this.lbWelcomeNote.Name = "lbWelcomeNote";
            this.lbWelcomeNote.Size = new System.Drawing.Size(180, 16);
            this.lbWelcomeNote.TabIndex = 4;
            this.lbWelcomeNote.Text = "Welcome to CFML Application";
            // 
            // formDragControl
            // 
            this.formDragControl.Fixed = true;
            this.formDragControl.Horizontal = true;
            this.formDragControl.TargetControl = this;
            this.formDragControl.Vertical = true;
            // 
            // pnLoginContainer
            // 
            this.pnLoginContainer.Location = new System.Drawing.Point(12, 166);
            this.pnLoginContainer.Name = "pnLoginContainer";
            this.pnLoginContainer.Size = new System.Drawing.Size(325, 160);
            this.pnLoginContainer.TabIndex = 8;
            // 
            // lbRegisterLink
            // 
            this.lbRegisterLink.AutoSize = true;
            this.lbRegisterLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRegisterLink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbRegisterLink.Location = new System.Drawing.Point(146, 348);
            this.lbRegisterLink.Name = "lbRegisterLink";
            this.lbRegisterLink.Size = new System.Drawing.Size(56, 16);
            this.lbRegisterLink.TabIndex = 9;
            this.lbRegisterLink.TabStop = true;
            this.lbRegisterLink.Text = "Register";
            this.lbRegisterLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbRegisterLink_LinkClicked);
            // 
            // pnLoginOption
            // 
            this.pnLoginOption.Controls.Add(this.btnLoginAsGuest);
            this.pnLoginOption.Controls.Add(this.btnLoginAsMember);
            this.pnLoginOption.Location = new System.Drawing.Point(11, 166);
            this.pnLoginOption.Name = "pnLoginOption";
            this.pnLoginOption.Size = new System.Drawing.Size(325, 160);
            this.pnLoginOption.TabIndex = 9;
            // 
            // btnLoginAsGuest
            // 
            this.btnLoginAsGuest.ActiveBorderThickness = 1;
            this.btnLoginAsGuest.ActiveCornerRadius = 20;
            this.btnLoginAsGuest.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLoginAsGuest.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnLoginAsGuest.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLoginAsGuest.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoginAsGuest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLoginAsGuest.BackgroundImage")));
            this.btnLoginAsGuest.ButtonText = "Login As Guest";
            this.btnLoginAsGuest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLoginAsGuest.Font = new System.Drawing.Font("Lato", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoginAsGuest.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLoginAsGuest.IdleBorderThickness = 1;
            this.btnLoginAsGuest.IdleCornerRadius = 20;
            this.btnLoginAsGuest.IdleFillColor = System.Drawing.SystemColors.ButtonFace;
            this.btnLoginAsGuest.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnLoginAsGuest.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnLoginAsGuest.Location = new System.Drawing.Point(72, 86);
            this.btnLoginAsGuest.Margin = new System.Windows.Forms.Padding(5);
            this.btnLoginAsGuest.Name = "btnLoginAsGuest";
            this.btnLoginAsGuest.Size = new System.Drawing.Size(181, 41);
            this.btnLoginAsGuest.TabIndex = 3;
            this.btnLoginAsGuest.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLoginAsGuest.Click += new System.EventHandler(this.btnLoginAsGuest_Click);
            // 
            // btnLoginAsMember
            // 
            this.btnLoginAsMember.ActiveBorderThickness = 1;
            this.btnLoginAsMember.ActiveCornerRadius = 20;
            this.btnLoginAsMember.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLoginAsMember.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnLoginAsMember.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLoginAsMember.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoginAsMember.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLoginAsMember.BackgroundImage")));
            this.btnLoginAsMember.ButtonText = "Login As Member";
            this.btnLoginAsMember.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLoginAsMember.Font = new System.Drawing.Font("Lato", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoginAsMember.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLoginAsMember.IdleBorderThickness = 1;
            this.btnLoginAsMember.IdleCornerRadius = 20;
            this.btnLoginAsMember.IdleFillColor = System.Drawing.SystemColors.ButtonFace;
            this.btnLoginAsMember.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnLoginAsMember.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnLoginAsMember.Location = new System.Drawing.Point(72, 34);
            this.btnLoginAsMember.Margin = new System.Windows.Forms.Padding(5);
            this.btnLoginAsMember.Name = "btnLoginAsMember";
            this.btnLoginAsMember.Size = new System.Drawing.Size(181, 41);
            this.btnLoginAsMember.TabIndex = 2;
            this.btnLoginAsMember.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLoginAsMember.Click += new System.EventHandler(this.btnLoginAsMember_Click);
            // 
            // lbForgotPasswordLink
            // 
            this.lbForgotPasswordLink.AutoSize = true;
            this.lbForgotPasswordLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbForgotPasswordLink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbForgotPasswordLink.Location = new System.Drawing.Point(124, 382);
            this.lbForgotPasswordLink.Name = "lbForgotPasswordLink";
            this.lbForgotPasswordLink.Size = new System.Drawing.Size(103, 16);
            this.lbForgotPasswordLink.TabIndex = 10;
            this.lbForgotPasswordLink.TabStop = true;
            this.lbForgotPasswordLink.Text = "Forgot Password";
            this.lbForgotPasswordLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbForgotPasswordLink_LinkClicked);
            // 
            // btnBackToLoginOption
            // 
            this.btnBackToLoginOption.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBackToLoginOption.BackgroundImage")));
            this.btnBackToLoginOption.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnBackToLoginOption.Location = new System.Drawing.Point(49, 53);
            this.btnBackToLoginOption.Name = "btnBackToLoginOption";
            this.btnBackToLoginOption.Size = new System.Drawing.Size(28, 38);
            this.btnBackToLoginOption.TabIndex = 7;
            this.btnBackToLoginOption.TabStop = false;
            this.btnBackToLoginOption.Click += new System.EventHandler(this.btnBackToLoginOption_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuit.BackgroundImage")));
            this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQuit.Location = new System.Drawing.Point(280, 12);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(56, 25);
            this.btnQuit.TabIndex = 6;
            this.btnQuit.TabStop = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // btnLoginBtn
            // 
            this.btnLoginBtn.ActiveBorderThickness = 1;
            this.btnLoginBtn.ActiveCornerRadius = 20;
            this.btnLoginBtn.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLoginBtn.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnLoginBtn.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLoginBtn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoginBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLoginBtn.BackgroundImage")));
            this.btnLoginBtn.ButtonText = "Connect";
            this.btnLoginBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLoginBtn.Font = new System.Drawing.Font("Lato", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoginBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnLoginBtn.IdleBorderThickness = 1;
            this.btnLoginBtn.IdleCornerRadius = 20;
            this.btnLoginBtn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnLoginBtn.IdleForecolor = System.Drawing.Color.White;
            this.btnLoginBtn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnLoginBtn.Location = new System.Drawing.Point(107, 334);
            this.btnLoginBtn.Margin = new System.Windows.Forms.Padding(5);
            this.btnLoginBtn.Name = "btnLoginBtn";
            this.btnLoginBtn.Size = new System.Drawing.Size(132, 43);
            this.btnLoginBtn.TabIndex = 5;
            this.btnLoginBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLoginBtn.Click += new System.EventHandler(this.btnLoginBtn_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStrip;
            this.notifyIcon.Text = "CFML - Login";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Font = new System.Drawing.Font("Lato", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(105, 48);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Font = new System.Drawing.Font("Lato Heavy", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Lato", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // loginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 450);
            this.Controls.Add(this.lbForgotPasswordLink);
            this.Controls.Add(this.pnLoginOption);
            this.Controls.Add(this.lbRegisterLink);
            this.Controls.Add(this.pnLoginContainer);
            this.Controls.Add(this.btnBackToLoginOption);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnLoginBtn);
            this.Controls.Add(this.lbWelcomeNote);
            this.Controls.Add(this.lbLoginTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "loginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.loginForm_FormClosing);
            this.pnLoginOption.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBackToLoginOption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse;
        private Bunifu.Framework.UI.BunifuCustomLabel lbLoginTitle;
        private System.Windows.Forms.Label lbWelcomeNote;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLoginBtn;
        private Bunifu.Framework.UI.BunifuDragControl formDragControl;
        private System.Windows.Forms.PictureBox btnQuit;
        private System.Windows.Forms.PictureBox btnBackToLoginOption;
        private System.Windows.Forms.Panel pnLoginContainer;
        private System.Windows.Forms.LinkLabel lbRegisterLink;
        private System.Windows.Forms.Panel pnLoginOption;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLoginAsGuest;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLoginAsMember;
        private System.Windows.Forms.LinkLabel lbForgotPasswordLink;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}