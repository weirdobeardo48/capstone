﻿using CFML.Capture;
using CFML.Common;
using CFML.GUI.CustomizeComponent;
using CFML.GUIDesign.CustomizeComponent;
using CFML.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CFML.GUIDesign
{
    public partial class mainForm : Form
    {
        #region constant
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        #endregion

        #region Variable

        #region User Control Variable
        private mainUserControl MainUserControl;
        private accountUserControl AccountUserControl;
        private configUserControl ConfigurationUserControl;
        private changeKeyUserControl ChangeKeyUserControl;
        private customizeUserControl CustomizeUserControl;
        private privacyUserControl PrivacyUserControl;
        private aboutUserControl AboutUserControl;
        #endregion

        #region Config
        private ConfigurationHandle configHandle;
        private Configuration.Configuration config;
        #endregion

        #region Side Menu
        private List<Bunifu.Framework.UI.BunifuFlatButton> menuItemList;

        private int ActiveIndex;
        #endregion

        #region Login Form
        public loginForm LoginForm;
        #endregion

        #region User Detail
        private userDetail user;
        #endregion

        LoginByMasterPassword mpf;
        #endregion

        public mainForm()
        {
            InitializeComponent();
            Initiate();
        }

        public void SetUser(userDetail user)
        {
            this.user = user;
            updateFormByUserType();
            updateMainFormToChildForm();
        }

        public void CheckForUpdate()
        {
            //Check connection
            if (!Common.Common.validConnection())
            {
                return;
            }
            else
            {
                #region Check for update
                /*
                 * 0: No result > default 
                 * 1: Up to date
                 * 2: Out of date
                */
                appVersionResult versionResult = Common.Common.checkAppVersion();
                switch (versionResult.updateStatus.ToString().Trim())
                {
                    case "1":
                        {
                            break;
                        }
                    case "2":
                        {
                            CustomConfirmationBox ccb = new CustomConfirmationBox(
                                ComponentDescriptionText.VERSION_NEED_TO_UPDATE_NOTIFICATION,
                                ComponentDescriptionText.VERSION_NEED_TO_UPDATE_NOTIFICATION_TITLE);
                            var result = ccb.ShowDialog();
                            WindowSnap.SetForegroundWindow(ccb.getHandle());

                            // result = OK
                            if (result == DialogResult.OK)
                            {
                                try
                                {
                                    Process.Start(versionResult.appLink);
                                }
                                catch (Exception exp)
                                {
                                    Console.WriteLine(exp.Message);
                                    CustomNotificationBox cnb = new CustomNotificationBox(
                                                        ComponentDescriptionText.UPDATE_FAILED_MESSAGE,
                                                        AlertType.Info);
                                    cnb.Visible = true;
                                }
                            }

                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
                #endregion
            }
        }

        private void updateMainFormToChildForm()
        {
            AccountUserControl.SetMainForm(this);
            ChangeKeyUserControl.SetMainForm(this);
            ConfigurationUserControl.SetMainForm(this);
            CustomizeUserControl.SetMainForm(this);
            PrivacyUserControl.SetMainForm(this);
            MainUserControl.SetMainForm(this);
        }

        public void updateAIRunningStatusToUserControl(bool isAIThreadRunning)
        {
            ConfigurationUserControl.setAIThreadRunningStatus(isAIThreadRunning);
            CustomizeUserControl.setAIThreadRunningStatus(isAIThreadRunning);
            PrivacyUserControl.setAIThreadRunningStatus(isAIThreadRunning);
        }

        private void updateFormByUserType()
        {
            //if User Name have 1 word
            if (user.OwnerName == null) user.OwnerName = "Unknown";
            string uName = user.OwnerName.Trim();
            if (uName.IndexOf(" ") < 0)
            {
                uName += " ";
            }

            lbWelcomeNote.Text = /*"Welcome " +*/ uName.Substring(0, uName.IndexOf(" "));
            switch (user.UserType.ToString())
            {
                // Guest
                case "0":
                    {
                        lbVIPStatus.Text = "Guest";
                        break;
                    }
                // Normal
                case "1":
                    {
                        lbVIPStatus.Text = "Normal User";
                        break;
                    }
                // VIP
                case "2":
                    {
                        
                        lbVIPStatus.Text = "VIP User";
                        break;
                    }
            }
            updateUserToUserControl();
        }

        private void updateUserToUserControl()
        {
            AccountUserControl.SetUser(user);
            ChangeKeyUserControl.SetUser(user);
            ConfigurationUserControl.SetUser(user);
            CustomizeUserControl.SetUser(user);
            PrivacyUserControl.SetUser(user);
            MainUserControl.SetUser(user);
        }

        private void Initiate()
        {
            #region Config
            config = new Configuration.Configuration();
            configHandle = new ConfigurationHandle();
            updateConfig();
            #endregion

            #region Exit button

            #endregion

            #region User Control
            AccountUserControl = new accountUserControl();
            MainUserControl = new mainUserControl();
            ChangeKeyUserControl = new changeKeyUserControl();
            CustomizeUserControl = new customizeUserControl();
            PrivacyUserControl = new privacyUserControl();
            AboutUserControl = new aboutUserControl();
            ConfigurationUserControl = new configUserControl();
            
            pnUserControl.Controls.Add(MainUserControl);
            pnUserControl.Controls.Add(AccountUserControl);
            pnUserControl.Controls.Add(ConfigurationUserControl);
            pnUserControl.Controls.Add(ChangeKeyUserControl);
            pnUserControl.Controls.Add(CustomizeUserControl);
            pnUserControl.Controls.Add(PrivacyUserControl);
            pnUserControl.Controls.Add(AboutUserControl);

            MainUserControl.BringToFront();
            #endregion

            #region Menu Button
            btnMenuChangeKey.Text = "        Change Master\n        Password";
            menuItemList = new List<Bunifu.Framework.UI.BunifuFlatButton>();
            menuItemList.Add(btnMenuMain);
            menuItemList.Add(btnMenuAccount);
            menuItemList.Add(btnMenuConfiguration);
            menuItemList.Add(btnMenuChangeKey);
            menuItemList.Add(btnMenuCustomize);
            menuItemList.Add(btnMenuPrivacy);
            menuItemList.Add(btnMenuAbout);

            //resetActiveMenu();
            ActiveIndex = 0;
            activeMenuItemByIndex();
            #endregion

            #region App Title
            lbAppName.Text = Common.ComponentDescriptionText.APPLICATION_NAME;
            #endregion

            #region Tray Icon
            this.ShowInTaskbar = true;
            this.notifyIcon.Visible = false;
            #endregion

            mpf = new LoginByMasterPassword(this);
            notifyIcon.Icon = Resources.AppIcon;
            lbVersionStatus.Text = "Version " + ComponentDescriptionText.CURRENT_APP_VERSION;

            this.Icon = Resources.AppIcon;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        #region Component Events

        private void btnExit_Click(object sender, EventArgs e)
        {
            CustomConfirmationBox ccb = new CustomConfirmationBox(
                    ComponentDescriptionText.QUIT_APPLICATION_MESSAGE,
                    ComponentDescriptionText.QUIT_APPLICATION_TITLE, AlertType.Quit);

            var result = ccb.ShowDialog();

            if (result == DialogResult.OK)
            {
                callCloseForm();
            }
            else
            {
                this.ShowInTaskbar = false;
                this.notifyIcon.Visible = true;
                this.Hide();
            }
        }

        private void callCloseForm()
        {
            CloseApplicationForm closeForm = new CloseApplicationForm();
            closeForm.ShowDialog();
        }

        private void resetActiveMenu()
        {
            foreach (var menuItem in menuItemList)
            {      
                menuItem.Enabled = true;         
            }
        }

        private void activeMenuItemByIndex()
        {
            resetActiveMenu();
            try
            {
                var menuItem = menuItemList[ActiveIndex];
                menuItem.Enabled = false;
            }
            catch
            {
                Console.WriteLine("Invalid menu index!");
            }
        }
        
        private void btnMenuMain_Click(object sender, EventArgs e)
        {
            MainUserControl.BringToFront();
            ActiveIndex = 0;
            activeMenuItemByIndex();
        }

        private void btnMenuAccount_Click(object sender, EventArgs e)
        {
            AccountUserControl.BringToFront();
            ActiveIndex = 1;
            activeMenuItemByIndex();
        }

        private void btnMenuConfiguration_Click(object sender, EventArgs e)
        {
            ConfigurationUserControl.BringToFront();
            ActiveIndex = 2;
            activeMenuItemByIndex();
        }

        private void btnMenuChangeKey_Click(object sender, EventArgs e)
        {
            ChangeKeyUserControl.BringToFront();
            ActiveIndex = 3;
            activeMenuItemByIndex();
        }

        private void btnMenuCustomize_Click(object sender, EventArgs e)
        {
            CustomizeUserControl.BringToFront();
            ActiveIndex = 4;
            activeMenuItemByIndex();
        }

        private void btnMenuPrivacy_Click(object sender, EventArgs e)
        {
            PrivacyUserControl.BringToFront();
            ActiveIndex = 5;
            activeMenuItemByIndex();
        }

        private void btnMenuAbout_Click(object sender, EventArgs e)
        {
            AboutUserControl.BringToFront();
            ActiveIndex = 6;
            activeMenuItemByIndex();
            
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        //private void btnExpandMenu_Click(object sender, EventArgs e)
        //{
        //    if (pnLeftMenu.Width == 200)
        //    {
        //        pnLeftPnUserProfile.Visible = false;
        //        btnExpandMenuL.Visible = true;
        //        pnLeftMenu.Width = 50;
        //        bunifuTransition1.ShowSync(pnLeftMenu);

        //        //Size old = this.Size;
        //        //old.Width += -150;
        //        //this.Size = old;
        //    }
        //    else
        //    {
        //        pnLeftMenu.Width = 200;
        //        pnLeftPnUserProfile.Visible = true;
        //        btnExpandMenuL.Visible = false;
        //        bunifuTransition1.ShowSync(pnLeftMenu);
        //        //Size old = this.Size;
        //        //old.Width += 150;
        //        //this.Size = old;
        //    }
        //}

        #endregion

        #region Config Prog
        private void updateConfig()
        {
            configHandle.readConfigurationFromFile();
            config = ConfigurationHandle.Config;
        }

        #endregion

        public void Logout()
        {
            btnLogout_Click(null, null);
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {

            CustomConfirmationBox ccb = new CustomConfirmationBox(
                 ComponentDescriptionText.LOGOUT_WARNING,
                 ComponentDescriptionText.LOGOUT_WARNING_TITLE);
            var result = ccb.ShowDialog();

            if (result == DialogResult.OK)
            {
                this.Visible = false;
                this.notifyIcon.Visible = false;
                try
                {
                    LoginForm.Visible = true;
                    MainUserControl.BringToFront();
                    ActiveIndex = 0;
                    activeMenuItemByIndex();
                }
                catch
                {
                    btnExit_Click(sender, e);
                }                
            }
        }

        public void setLoginForm(loginForm LoginForm)
        {
            this.LoginForm = LoginForm;
        }

        private void btnExit_MouseHover(object sender, EventArgs e)
        {
            btnExit.BackColor = ColorTranslator.FromHtml("#D83232");
        }

        private void btnMinimize_MouseHover(object sender, EventArgs e)
        {
            btnMinimize.BackColor = ColorTranslator.FromHtml("#787878");
        }

        private void btnReColored(object sender, EventArgs e)
        {
            btnMinimize.BackColor = ColorTranslator.FromHtml("#F0F0F0");
            btnExit.BackColor = ColorTranslator.FromHtml("#F0F0F0");
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (mpf == null)
            {
                mpf = new LoginByMasterPassword(this);
            }
            if (mpf.WindowState == FormWindowState.Minimized)
            {
                mpf.WindowState = FormWindowState.Normal;
            }
            mpf.Show();    
        }

        public void RevolkeForm()
        {   
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            this.notifyIcon.Visible = false;
            this.Show();
            WindowSnap.SetForegroundWindow(this.Handle);

            this.requestDeactivateAI();
        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void lbCheckForUpdate_Click(object sender, EventArgs e)
        {
            //Check connection
            if (!Common.Common.validConnection())
            {
                CustomNotificationBox cnb = new CustomNotificationBox(
                    ComponentDescriptionText.NETWORK_NOT_AVAILABLE_MESSAGE, 
                    AlertType.Info);
                cnb.Visible = true;
                return;
            }
            else
            {
                #region Check for update
                /*
                 * 0: No result > default 
                 * 1: Up to date
                 * 2: Out of date
                */
                appVersionResult versionResult = Common.Common.checkAppVersion();
                switch (versionResult.updateStatus.ToString().Trim()) {
                    case "1":
                        {
                            CustomNotificationBox cnb = new CustomNotificationBox(
                                ComponentDescriptionText.VERSION_UP_TO_DATE_NOTIFICATION,
                                AlertType.Info);
                            cnb.Visible = true;
                            break;
                        }
                    case "2":
                        {
                            CustomConfirmationBox ccb = new CustomConfirmationBox(
                                ComponentDescriptionText.VERSION_NEED_TO_UPDATE_NOTIFICATION, 
                                ComponentDescriptionText.VERSION_NEED_TO_UPDATE_NOTIFICATION_TITLE);
                            var result = ccb.ShowDialog();

                            // result = OK
                            if (result == DialogResult.OK)
                            {
                                Process.Start(versionResult.appLink);
                            }

                            break;
                        }
                    default:
                        {
                            CustomNotificationBox cnb = new CustomNotificationBox(Common.ComponentDescriptionText.CHECK_FOR_UPDATE_FAILED, AlertType.Info);
                            cnb.Visible = true;
                            break;
                        }
                }
                #endregion
            }

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notifyIcon_MouseDoubleClick(null, null);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            callCloseForm();
        }

        #region Activate - Deactivate AI
        public void requestActivateAI()
        {
            MainUserControl.requestActivateAI();
        }

        public void requestDeactivateAI()
        {
            MainUserControl.requestDeactivateAI();
        }
        #endregion
    }
}
