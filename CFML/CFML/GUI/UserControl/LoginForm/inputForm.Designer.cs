﻿namespace CFML.GUIDesign
{
    partial class inputForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnPasswordBg = new System.Windows.Forms.Panel();
            this.inpPassword = new System.Windows.Forms.TextBox();
            this.pnUsernameBg = new System.Windows.Forms.Panel();
            this.inpUserName = new System.Windows.Forms.TextBox();
            this.pnPasswordBg.SuspendLayout();
            this.pnUsernameBg.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnPasswordBg
            // 
            this.pnPasswordBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnPasswordBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnPasswordBg.Controls.Add(this.inpPassword);
            this.pnPasswordBg.Location = new System.Drawing.Point(34, 89);
            this.pnPasswordBg.Name = "pnPasswordBg";
            this.pnPasswordBg.Size = new System.Drawing.Size(250, 45);
            this.pnPasswordBg.TabIndex = 7;
            this.pnPasswordBg.Click += new System.EventHandler(this.pnPasswordBg_Click);
            // 
            // inpPassword
            // 
            this.inpPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpPassword.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpPassword.Location = new System.Drawing.Point(8, 14);
            this.inpPassword.Name = "inpPassword";
            this.inpPassword.PasswordChar = '•';
            this.inpPassword.Size = new System.Drawing.Size(231, 16);
            this.inpPassword.TabIndex = 1;
            this.inpPassword.Enter += new System.EventHandler(this.inpUserName_Enter);
            this.inpPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inpPassword_KeyDown);
            this.inpPassword.Leave += new System.EventHandler(this.inpUserName_Leave);
            // 
            // pnUsernameBg
            // 
            this.pnUsernameBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnUsernameBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnUsernameBg.Controls.Add(this.inpUserName);
            this.pnUsernameBg.Location = new System.Drawing.Point(34, 22);
            this.pnUsernameBg.Name = "pnUsernameBg";
            this.pnUsernameBg.Size = new System.Drawing.Size(250, 45);
            this.pnUsernameBg.TabIndex = 6;
            this.pnUsernameBg.Click += new System.EventHandler(this.pnUsernameBg_Click);
            // 
            // inpUserName
            // 
            this.inpUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpUserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpUserName.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpUserName.Location = new System.Drawing.Point(8, 12);
            this.inpUserName.Name = "inpUserName";
            this.inpUserName.Size = new System.Drawing.Size(231, 16);
            this.inpUserName.TabIndex = 1;
            this.inpUserName.Enter += new System.EventHandler(this.inpPassword_Enter);
            this.inpUserName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inpPassword_KeyDown);
            this.inpUserName.Leave += new System.EventHandler(this.inpPassword_Leave);
            // 
            // inputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnPasswordBg);
            this.Controls.Add(this.pnUsernameBg);
            this.Name = "inputForm";
            this.Size = new System.Drawing.Size(325, 160);
            this.pnPasswordBg.ResumeLayout(false);
            this.pnPasswordBg.PerformLayout();
            this.pnUsernameBg.ResumeLayout(false);
            this.pnUsernameBg.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox inpUserName;
        private System.Windows.Forms.Panel pnUsernameBg;
        private System.Windows.Forms.Panel pnPasswordBg;
        private System.Windows.Forms.TextBox inpPassword;
    }
}
