﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.Common;
using CFML.Properties;

namespace CFML.GUIDesign
{
    public partial class inputForm : UserControl
    {
        private loginForm parent;

        public inputForm()
        {
            InitializeComponent();
            Initiate();
        }

        private void Initiate()
        {
            clearLoginForm();
            inpUserName.Select();
        }

        public loginDetail getLoginDetail()
        {
            return new loginDetail(inpUserName.Text.Trim(), inpPassword.Text.Trim());
        }

        public void clearLoginForm()
        {
            inpPassword.Text = "";
            inpUserName.Text = "";
        }

        private void inpUserName_Enter(object sender, EventArgs e)
        {
            this.inpUserName.SelectAll();
            this.pnUsernameBg.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void inpUserName_Leave(object sender, EventArgs e)
        {
            
            this.pnUsernameBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void inpPassword_Enter(object sender, EventArgs e)
        {
            this.inpPassword.SelectAll();
            this.pnPasswordBg.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void inpPassword_Leave(object sender, EventArgs e)
        {
            this.pnPasswordBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void pnUsernameBg_Click(object sender, EventArgs e)
        {
            inpUserName.Focus();
        }

        private void pnPasswordBg_Click(object sender, EventArgs e)
        {
            inpPassword.Focus();
        }

        private void inpPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                parent.requestLogin();
            }
        }

        public void SetParent(loginForm loginForm)
        {
            this.parent = loginForm;
        }
    }
}
