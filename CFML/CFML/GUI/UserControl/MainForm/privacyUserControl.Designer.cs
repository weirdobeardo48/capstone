﻿namespace CFML.GUIDesign
{
    partial class privacyUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbAnonymousDataCollect = new System.Windows.Forms.Label();
            this.lblbAnonymousDataCollectDes = new System.Windows.Forms.Label();
            this.cbAnonymousDataCollect = new Bunifu.Framework.UI.BunifuCheckbox();
            this.pnUCTitle = new System.Windows.Forms.Panel();
            this.lbUCTitle = new System.Windows.Forms.Label();
            this.pbUCPic = new System.Windows.Forms.PictureBox();
            this.pnUpgradeVIPNote = new System.Windows.Forms.Panel();
            this.lbUpgradeLink = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnLoginNote = new System.Windows.Forms.Panel();
            this.lbLoginLink = new System.Windows.Forms.LinkLabel();
            this.lbLoginSuggestion = new System.Windows.Forms.Label();
            this.pnFunctionContainer = new System.Windows.Forms.Panel();
            this.pnUCTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).BeginInit();
            this.pnUpgradeVIPNote.SuspendLayout();
            this.pnLoginNote.SuspendLayout();
            this.pnFunctionContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbAnonymousDataCollect
            // 
            this.lbAnonymousDataCollect.AutoSize = true;
            this.lbAnonymousDataCollect.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnonymousDataCollect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbAnonymousDataCollect.Location = new System.Drawing.Point(41, 5);
            this.lbAnonymousDataCollect.Name = "lbAnonymousDataCollect";
            this.lbAnonymousDataCollect.Size = new System.Drawing.Size(267, 16);
            this.lbAnonymousDataCollect.TabIndex = 3;
            this.lbAnonymousDataCollect.Text = "Allow server to receive data from your device";
            // 
            // lblbAnonymousDataCollectDes
            // 
            this.lblbAnonymousDataCollectDes.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbAnonymousDataCollectDes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblbAnonymousDataCollectDes.Location = new System.Drawing.Point(10, 46);
            this.lblbAnonymousDataCollectDes.Name = "lblbAnonymousDataCollectDes";
            this.lblbAnonymousDataCollectDes.Size = new System.Drawing.Size(476, 207);
            this.lblbAnonymousDataCollectDes.TabIndex = 5;
            this.lblbAnonymousDataCollectDes.Text = "This function .xc.mxszxclmapsf";
            // 
            // cbAnonymousDataCollect
            // 
            this.cbAnonymousDataCollect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbAnonymousDataCollect.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.cbAnonymousDataCollect.Checked = true;
            this.cbAnonymousDataCollect.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbAnonymousDataCollect.ForeColor = System.Drawing.Color.White;
            this.cbAnonymousDataCollect.Location = new System.Drawing.Point(13, 3);
            this.cbAnonymousDataCollect.Name = "cbAnonymousDataCollect";
            this.cbAnonymousDataCollect.Size = new System.Drawing.Size(20, 20);
            this.cbAnonymousDataCollect.TabIndex = 6;
            this.cbAnonymousDataCollect.OnChange += new System.EventHandler(this.cbAnonymousDataCollect_OnChange);
            this.cbAnonymousDataCollect.EnabledChanged += new System.EventHandler(this.cbAnonymousDataCollect_EnabledChanged);
            this.cbAnonymousDataCollect.Click += new System.EventHandler(this.cbAnonymousDataCollect_Click);
            // 
            // pnUCTitle
            // 
            this.pnUCTitle.Controls.Add(this.lbUCTitle);
            this.pnUCTitle.Controls.Add(this.pbUCPic);
            this.pnUCTitle.Location = new System.Drawing.Point(50, 20);
            this.pnUCTitle.Name = "pnUCTitle";
            this.pnUCTitle.Size = new System.Drawing.Size(472, 54);
            this.pnUCTitle.TabIndex = 9;
            // 
            // lbUCTitle
            // 
            this.lbUCTitle.AutoSize = true;
            this.lbUCTitle.Font = new System.Drawing.Font("Lato Heavy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUCTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUCTitle.Location = new System.Drawing.Point(57, 16);
            this.lbUCTitle.Name = "lbUCTitle";
            this.lbUCTitle.Size = new System.Drawing.Size(63, 19);
            this.lbUCTitle.TabIndex = 1;
            this.lbUCTitle.Text = "Privacy";
            // 
            // pbUCPic
            // 
            this.pbUCPic.BackgroundImage = global::CFML.Properties.Resources.PrivacyIconColored;
            this.pbUCPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbUCPic.Location = new System.Drawing.Point(4, 4);
            this.pbUCPic.Name = "pbUCPic";
            this.pbUCPic.Size = new System.Drawing.Size(47, 47);
            this.pbUCPic.TabIndex = 0;
            this.pbUCPic.TabStop = false;
            // 
            // pnUpgradeVIPNote
            // 
            this.pnUpgradeVIPNote.Controls.Add(this.lbUpgradeLink);
            this.pnUpgradeVIPNote.Controls.Add(this.label1);
            this.pnUpgradeVIPNote.Location = new System.Drawing.Point(50, 96);
            this.pnUpgradeVIPNote.Name = "pnUpgradeVIPNote";
            this.pnUpgradeVIPNote.Size = new System.Drawing.Size(486, 30);
            this.pnUpgradeVIPNote.TabIndex = 21;
            // 
            // lbUpgradeLink
            // 
            this.lbUpgradeLink.AutoSize = true;
            this.lbUpgradeLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpgradeLink.Location = new System.Drawing.Point(392, 7);
            this.lbUpgradeLink.Name = "lbUpgradeLink";
            this.lbUpgradeLink.Size = new System.Drawing.Size(86, 16);
            this.lbUpgradeLink.TabIndex = 6;
            this.lbUpgradeLink.TabStop = true;
            this.lbUpgradeLink.Text = "Upgrade here";
            this.lbUpgradeLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbUpgradeLink_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(361, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Upgrade to VIP account to explore the amazing function below";
            // 
            // pnLoginNote
            // 
            this.pnLoginNote.Controls.Add(this.lbLoginLink);
            this.pnLoginNote.Controls.Add(this.lbLoginSuggestion);
            this.pnLoginNote.Location = new System.Drawing.Point(50, 96);
            this.pnLoginNote.Name = "pnLoginNote";
            this.pnLoginNote.Size = new System.Drawing.Size(486, 30);
            this.pnLoginNote.TabIndex = 20;
            // 
            // lbLoginLink
            // 
            this.lbLoginLink.AutoSize = true;
            this.lbLoginLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginLink.Location = new System.Drawing.Point(406, 8);
            this.lbLoginLink.Name = "lbLoginLink";
            this.lbLoginLink.Size = new System.Drawing.Size(71, 16);
            this.lbLoginLink.TabIndex = 6;
            this.lbLoginLink.TabStop = true;
            this.lbLoginLink.Text = "Log in here";
            this.lbLoginLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbLoginLink_LinkClicked);
            // 
            // lbLoginSuggestion
            // 
            this.lbLoginSuggestion.AutoSize = true;
            this.lbLoginSuggestion.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginSuggestion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbLoginSuggestion.Location = new System.Drawing.Point(3, 8);
            this.lbLoginSuggestion.Name = "lbLoginSuggestion";
            this.lbLoginSuggestion.Size = new System.Drawing.Size(242, 16);
            this.lbLoginSuggestion.TabIndex = 5;
            this.lbLoginSuggestion.Text = "Log in to explore these amazing functions";
            // 
            // pnFunctionContainer
            // 
            this.pnFunctionContainer.Controls.Add(this.cbAnonymousDataCollect);
            this.pnFunctionContainer.Controls.Add(this.lblbAnonymousDataCollectDes);
            this.pnFunctionContainer.Controls.Add(this.lbAnonymousDataCollect);
            this.pnFunctionContainer.Location = new System.Drawing.Point(50, 152);
            this.pnFunctionContainer.Name = "pnFunctionContainer";
            this.pnFunctionContainer.Size = new System.Drawing.Size(498, 264);
            this.pnFunctionContainer.TabIndex = 22;
            // 
            // privacyUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnUpgradeVIPNote);
            this.Controls.Add(this.pnLoginNote);
            this.Controls.Add(this.pnUCTitle);
            this.Controls.Add(this.pnFunctionContainer);
            this.Name = "privacyUserControl";
            this.Size = new System.Drawing.Size(612, 643);
            this.pnUCTitle.ResumeLayout(false);
            this.pnUCTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).EndInit();
            this.pnUpgradeVIPNote.ResumeLayout(false);
            this.pnUpgradeVIPNote.PerformLayout();
            this.pnLoginNote.ResumeLayout(false);
            this.pnLoginNote.PerformLayout();
            this.pnFunctionContainer.ResumeLayout(false);
            this.pnFunctionContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbAnonymousDataCollect;
        private System.Windows.Forms.Label lblbAnonymousDataCollectDes;
        private Bunifu.Framework.UI.BunifuCheckbox cbAnonymousDataCollect;
        private System.Windows.Forms.Panel pnUCTitle;
        private System.Windows.Forms.Label lbUCTitle;
        private System.Windows.Forms.PictureBox pbUCPic;
        private System.Windows.Forms.Panel pnUpgradeVIPNote;
        private System.Windows.Forms.LinkLabel lbUpgradeLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnLoginNote;
        private System.Windows.Forms.LinkLabel lbLoginLink;
        private System.Windows.Forms.Label lbLoginSuggestion;
        private System.Windows.Forms.Panel pnFunctionContainer;
    }
}
