﻿namespace CFML.GUIDesign
{
    partial class mainUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbChildrenModeStatus = new System.Windows.Forms.Label();
            this.lbSeparateLine = new System.Windows.Forms.Label();
            this.lbChildrenModeDes = new System.Windows.Forms.Label();
            this.cbChildrenMode = new Bunifu.Framework.UI.BunifuCheckbox();
            this.pbChildrenMode = new System.Windows.Forms.PictureBox();
            this.pnUCTitle = new System.Windows.Forms.Panel();
            this.lbUCTitle = new System.Windows.Forms.Label();
            this.pbUCPic = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbChildrenMode)).BeginInit();
            this.pnUCTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).BeginInit();
            this.SuspendLayout();
            // 
            // lbChildrenModeStatus
            // 
            this.lbChildrenModeStatus.Font = new System.Drawing.Font("Lato Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChildrenModeStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbChildrenModeStatus.Location = new System.Drawing.Point(46, 231);
            this.lbChildrenModeStatus.Name = "lbChildrenModeStatus";
            this.lbChildrenModeStatus.Size = new System.Drawing.Size(522, 20);
            this.lbChildrenModeStatus.TabIndex = 1;
            this.lbChildrenModeStatus.Text = "You are in children mode";
            this.lbChildrenModeStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSeparateLine
            // 
            this.lbSeparateLine.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbSeparateLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbSeparateLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbSeparateLine.Location = new System.Drawing.Point(46, 274);
            this.lbSeparateLine.Name = "lbSeparateLine";
            this.lbSeparateLine.Size = new System.Drawing.Size(522, 3);
            this.lbSeparateLine.TabIndex = 4;
            // 
            // lbChildrenModeDes
            // 
            this.lbChildrenModeDes.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChildrenModeDes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbChildrenModeDes.Location = new System.Drawing.Point(43, 310);
            this.lbChildrenModeDes.Name = "lbChildrenModeDes";
            this.lbChildrenModeDes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbChildrenModeDes.Size = new System.Drawing.Size(542, 116);
            this.lbChildrenModeDes.TabIndex = 5;
            this.lbChildrenModeDes.Text = "In the children mode,...";
            // 
            // cbChildrenMode
            // 
            this.cbChildrenMode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.cbChildrenMode.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.cbChildrenMode.Checked = true;
            this.cbChildrenMode.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.cbChildrenMode.ForeColor = System.Drawing.Color.White;
            this.cbChildrenMode.Location = new System.Drawing.Point(294, 179);
            this.cbChildrenMode.Name = "cbChildrenMode";
            this.cbChildrenMode.Size = new System.Drawing.Size(20, 20);
            this.cbChildrenMode.TabIndex = 6;
            this.cbChildrenMode.OnChange += new System.EventHandler(this.cbChildrenMode_OnChange);
            // 
            // pbChildrenMode
            // 
            this.pbChildrenMode.BackgroundImage = global::CFML.Properties.Resources.ToggleOn;
            this.pbChildrenMode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbChildrenMode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbChildrenMode.Location = new System.Drawing.Point(275, 161);
            this.pbChildrenMode.Name = "pbChildrenMode";
            this.pbChildrenMode.Size = new System.Drawing.Size(55, 51);
            this.pbChildrenMode.TabIndex = 7;
            this.pbChildrenMode.TabStop = false;
            this.pbChildrenMode.Click += new System.EventHandler(this.pbChildrenMode_Click);
            // 
            // pnUCTitle
            // 
            this.pnUCTitle.Controls.Add(this.lbUCTitle);
            this.pnUCTitle.Controls.Add(this.pbUCPic);
            this.pnUCTitle.Location = new System.Drawing.Point(46, 19);
            this.pnUCTitle.Name = "pnUCTitle";
            this.pnUCTitle.Size = new System.Drawing.Size(472, 54);
            this.pnUCTitle.TabIndex = 8;
            // 
            // lbUCTitle
            // 
            this.lbUCTitle.AutoSize = true;
            this.lbUCTitle.Font = new System.Drawing.Font("Lato Heavy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUCTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUCTitle.Location = new System.Drawing.Point(57, 16);
            this.lbUCTitle.Name = "lbUCTitle";
            this.lbUCTitle.Size = new System.Drawing.Size(90, 19);
            this.lbUCTitle.TabIndex = 1;
            this.lbUCTitle.Text = "Main menu";
            // 
            // pbUCPic
            // 
            this.pbUCPic.BackgroundImage = global::CFML.Properties.Resources.MainIconColored;
            this.pbUCPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbUCPic.Location = new System.Drawing.Point(4, 4);
            this.pbUCPic.Name = "pbUCPic";
            this.pbUCPic.Size = new System.Drawing.Size(47, 47);
            this.pbUCPic.TabIndex = 0;
            this.pbUCPic.TabStop = false;
            // 
            // mainUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.pnUCTitle);
            this.Controls.Add(this.pbChildrenMode);
            this.Controls.Add(this.cbChildrenMode);
            this.Controls.Add(this.lbChildrenModeDes);
            this.Controls.Add(this.lbSeparateLine);
            this.Controls.Add(this.lbChildrenModeStatus);
            this.Name = "mainUserControl";
            this.Size = new System.Drawing.Size(612, 643);
            ((System.ComponentModel.ISupportInitialize)(this.pbChildrenMode)).EndInit();
            this.pnUCTitle.ResumeLayout(false);
            this.pnUCTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbChildrenModeStatus;
        private System.Windows.Forms.Label lbSeparateLine;
        private System.Windows.Forms.Label lbChildrenModeDes;
        private Bunifu.Framework.UI.BunifuCheckbox cbChildrenMode;
        private System.Windows.Forms.PictureBox pbChildrenMode;
        private System.Windows.Forms.Panel pnUCTitle;
        private System.Windows.Forms.Label lbUCTitle;
        private System.Windows.Forms.PictureBox pbUCPic;
    }
}
