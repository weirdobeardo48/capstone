﻿namespace CFML.GUIDesign
{
    partial class configUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(configUserControl));
            this.pnCensorSensitivity = new System.Windows.Forms.Panel();
            this.pnCensorSensitivityDes = new System.Windows.Forms.Panel();
            this.lbTrackbarSensitivityLevel = new System.Windows.Forms.Label();
            this.lbCensorSensitivity = new System.Windows.Forms.Label();
            this.lbCensorObject = new System.Windows.Forms.Label();
            this.pnCensorBox = new System.Windows.Forms.Panel();
            this.pnCensorObjectDes = new System.Windows.Forms.Panel();
            this.lbCensorNumber = new System.Windows.Forms.Label();
            this.lbObjectTracking = new System.Windows.Forms.Label();
            this.pnObjectTracking = new System.Windows.Forms.Panel();
            this.pnObjectTrackingDes = new System.Windows.Forms.Panel();
            this.lbEnableObjectTrackingTitle = new System.Windows.Forms.Label();
            this.cbObjectTracking = new Bunifu.Framework.UI.BunifuCheckbox();
            this.lbSound = new System.Windows.Forms.Label();
            this.pnSound = new System.Windows.Forms.Panel();
            this.pnSoundDes = new System.Windows.Forms.Panel();
            this.lbSoundTitle = new System.Windows.Forms.Label();
            this.cbSound = new Bunifu.Framework.UI.BunifuCheckbox();
            this.lbInactiveApp = new System.Windows.Forms.Label();
            this.pnInactiveApp = new System.Windows.Forms.Panel();
            this.pnInactiveAppDes = new System.Windows.Forms.Panel();
            this.lbInactiveAppTitle = new System.Windows.Forms.Label();
            this.cbInactiveApp = new Bunifu.Framework.UI.BunifuCheckbox();
            this.lbRealTime = new System.Windows.Forms.Label();
            this.pnRealTime = new System.Windows.Forms.Panel();
            this.pnRealtimeDes = new System.Windows.Forms.Panel();
            this.lbRealtimeInv = new System.Windows.Forms.Label();
            this.inpRealtimeScanInterval = new System.Windows.Forms.TextBox();
            this.lbRealTimeTitile = new System.Windows.Forms.Label();
            this.cbRealTime = new Bunifu.Framework.UI.BunifuCheckbox();
            this.btnApply = new Bunifu.Framework.UI.BunifuFlatButton();
            this.ttDescription = new System.Windows.Forms.ToolTip(this.components);
            this.pnUCTitle = new System.Windows.Forms.Panel();
            this.lbUCTitle = new System.Windows.Forms.Label();
            this.pbUCPic = new System.Windows.Forms.PictureBox();
            this.pnFunctionContainer = new System.Windows.Forms.Panel();
            this.pnUpgradeVIPNote = new System.Windows.Forms.Panel();
            this.lbUpgradeLink = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnVIPFunctionOnly = new System.Windows.Forms.Panel();
            this.pnLoginNote = new System.Windows.Forms.Panel();
            this.lbLoginLink = new System.Windows.Forms.LinkLabel();
            this.lbLoginSuggestion = new System.Windows.Forms.Label();
            this.pnCensorSensitivity.SuspendLayout();
            this.pnCensorBox.SuspendLayout();
            this.pnObjectTracking.SuspendLayout();
            this.pnSound.SuspendLayout();
            this.pnInactiveApp.SuspendLayout();
            this.pnRealTime.SuspendLayout();
            this.pnUCTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).BeginInit();
            this.pnFunctionContainer.SuspendLayout();
            this.pnUpgradeVIPNote.SuspendLayout();
            this.pnVIPFunctionOnly.SuspendLayout();
            this.pnLoginNote.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnCensorSensitivity
            // 
            this.pnCensorSensitivity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnCensorSensitivity.Controls.Add(this.pnCensorSensitivityDes);
            this.pnCensorSensitivity.Controls.Add(this.lbTrackbarSensitivityLevel);
            this.pnCensorSensitivity.Location = new System.Drawing.Point(3, 22);
            this.pnCensorSensitivity.Name = "pnCensorSensitivity";
            this.pnCensorSensitivity.Size = new System.Drawing.Size(507, 77);
            this.pnCensorSensitivity.TabIndex = 0;
            // 
            // pnCensorSensitivityDes
            // 
            this.pnCensorSensitivityDes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnCensorSensitivityDes.BackgroundImage")));
            this.pnCensorSensitivityDes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnCensorSensitivityDes.Location = new System.Drawing.Point(488, 3);
            this.pnCensorSensitivityDes.Name = "pnCensorSensitivityDes";
            this.pnCensorSensitivityDes.Size = new System.Drawing.Size(12, 12);
            this.pnCensorSensitivityDes.TabIndex = 10;
            this.pnCensorSensitivityDes.MouseLeave += new System.EventHandler(this.Dismiss_Toolkit);
            this.pnCensorSensitivityDes.MouseHover += new System.EventHandler(this.pnCensorSensitivityDes_MouseHover);
            // 
            // lbTrackbarSensitivityLevel
            // 
            this.lbTrackbarSensitivityLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrackbarSensitivityLevel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbTrackbarSensitivityLevel.Location = new System.Drawing.Point(32, 42);
            this.lbTrackbarSensitivityLevel.Name = "lbTrackbarSensitivityLevel";
            this.lbTrackbarSensitivityLevel.Size = new System.Drawing.Size(422, 18);
            this.lbTrackbarSensitivityLevel.TabIndex = 6;
            this.lbTrackbarSensitivityLevel.Text = "1             2            3             4            5             6            " +
    "7             8             9           10";
            // 
            // lbCensorSensitivity
            // 
            this.lbCensorSensitivity.AutoSize = true;
            this.lbCensorSensitivity.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCensorSensitivity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbCensorSensitivity.Location = new System.Drawing.Point(16, 10);
            this.lbCensorSensitivity.Name = "lbCensorSensitivity";
            this.lbCensorSensitivity.Size = new System.Drawing.Size(111, 16);
            this.lbCensorSensitivity.TabIndex = 0;
            this.lbCensorSensitivity.Text = "Censor sensitivity";
            // 
            // lbCensorObject
            // 
            this.lbCensorObject.AutoSize = true;
            this.lbCensorObject.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCensorObject.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbCensorObject.Location = new System.Drawing.Point(16, 116);
            this.lbCensorObject.Name = "lbCensorObject";
            this.lbCensorObject.Size = new System.Drawing.Size(225, 16);
            this.lbCensorObject.TabIndex = 1;
            this.lbCensorObject.Text = "Maximum number of censored objects";
            // 
            // pnCensorBox
            // 
            this.pnCensorBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnCensorBox.Controls.Add(this.pnCensorObjectDes);
            this.pnCensorBox.Controls.Add(this.lbCensorNumber);
            this.pnCensorBox.Location = new System.Drawing.Point(3, 128);
            this.pnCensorBox.Name = "pnCensorBox";
            this.pnCensorBox.Size = new System.Drawing.Size(507, 77);
            this.pnCensorBox.TabIndex = 2;
            // 
            // pnCensorObjectDes
            // 
            this.pnCensorObjectDes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnCensorObjectDes.BackgroundImage")));
            this.pnCensorObjectDes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnCensorObjectDes.Location = new System.Drawing.Point(488, 3);
            this.pnCensorObjectDes.Name = "pnCensorObjectDes";
            this.pnCensorObjectDes.Size = new System.Drawing.Size(12, 12);
            this.pnCensorObjectDes.TabIndex = 11;
            this.pnCensorObjectDes.MouseLeave += new System.EventHandler(this.Dismiss_Toolkit);
            this.pnCensorObjectDes.MouseHover += new System.EventHandler(this.pnCensorObjectDes_MouseHover);
            // 
            // lbCensorNumber
            // 
            this.lbCensorNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCensorNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbCensorNumber.Location = new System.Drawing.Point(33, 41);
            this.lbCensorNumber.Name = "lbCensorNumber";
            this.lbCensorNumber.Size = new System.Drawing.Size(421, 18);
            this.lbCensorNumber.TabIndex = 5;
            this.lbCensorNumber.Text = "1                 2                 3                 4                 5        " +
    "        6                 7                 8";
            // 
            // lbObjectTracking
            // 
            this.lbObjectTracking.AutoSize = true;
            this.lbObjectTracking.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbObjectTracking.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbObjectTracking.Location = new System.Drawing.Point(25, 227);
            this.lbObjectTracking.Name = "lbObjectTracking";
            this.lbObjectTracking.Size = new System.Drawing.Size(95, 16);
            this.lbObjectTracking.TabIndex = 1;
            this.lbObjectTracking.Text = "Object tracking";
            // 
            // pnObjectTracking
            // 
            this.pnObjectTracking.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnObjectTracking.Controls.Add(this.pnObjectTrackingDes);
            this.pnObjectTracking.Controls.Add(this.lbEnableObjectTrackingTitle);
            this.pnObjectTracking.Controls.Add(this.cbObjectTracking);
            this.pnObjectTracking.Location = new System.Drawing.Point(3, 234);
            this.pnObjectTracking.Name = "pnObjectTracking";
            this.pnObjectTracking.Size = new System.Drawing.Size(245, 85);
            this.pnObjectTracking.TabIndex = 2;
            // 
            // pnObjectTrackingDes
            // 
            this.pnObjectTrackingDes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnObjectTrackingDes.BackgroundImage")));
            this.pnObjectTrackingDes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnObjectTrackingDes.Location = new System.Drawing.Point(225, 3);
            this.pnObjectTrackingDes.Name = "pnObjectTrackingDes";
            this.pnObjectTrackingDes.Size = new System.Drawing.Size(12, 12);
            this.pnObjectTrackingDes.TabIndex = 9;
            this.pnObjectTrackingDes.MouseLeave += new System.EventHandler(this.Dismiss_Toolkit);
            this.pnObjectTrackingDes.MouseHover += new System.EventHandler(this.pnObjectTrackingDes_MouseHover);
            // 
            // lbEnableObjectTrackingTitle
            // 
            this.lbEnableObjectTrackingTitle.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnableObjectTrackingTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbEnableObjectTrackingTitle.Location = new System.Drawing.Point(50, 35);
            this.lbEnableObjectTrackingTitle.Name = "lbEnableObjectTrackingTitle";
            this.lbEnableObjectTrackingTitle.Size = new System.Drawing.Size(159, 19);
            this.lbEnableObjectTrackingTitle.TabIndex = 5;
            this.lbEnableObjectTrackingTitle.Text = "Enable object tracking";
            // 
            // cbObjectTracking
            // 
            this.cbObjectTracking.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbObjectTracking.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.cbObjectTracking.Checked = true;
            this.cbObjectTracking.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbObjectTracking.ForeColor = System.Drawing.Color.White;
            this.cbObjectTracking.Location = new System.Drawing.Point(25, 34);
            this.cbObjectTracking.Name = "cbObjectTracking";
            this.cbObjectTracking.Size = new System.Drawing.Size(20, 20);
            this.cbObjectTracking.TabIndex = 4;
            this.cbObjectTracking.OnChange += new System.EventHandler(this.cbObjectTracking_OnChange);
            this.cbObjectTracking.EnabledChanged += new System.EventHandler(this.cbInactiveApp_EnabledChanged);
            // 
            // lbSound
            // 
            this.lbSound.AutoSize = true;
            this.lbSound.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSound.Location = new System.Drawing.Point(27, 9);
            this.lbSound.Name = "lbSound";
            this.lbSound.Size = new System.Drawing.Size(43, 16);
            this.lbSound.TabIndex = 1;
            this.lbSound.Text = "Sound";
            // 
            // pnSound
            // 
            this.pnSound.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnSound.Controls.Add(this.pnSoundDes);
            this.pnSound.Controls.Add(this.lbSoundTitle);
            this.pnSound.Controls.Add(this.cbSound);
            this.pnSound.Location = new System.Drawing.Point(3, 17);
            this.pnSound.Name = "pnSound";
            this.pnSound.Size = new System.Drawing.Size(245, 85);
            this.pnSound.TabIndex = 2;
            // 
            // pnSoundDes
            // 
            this.pnSoundDes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnSoundDes.BackgroundImage")));
            this.pnSoundDes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnSoundDes.Location = new System.Drawing.Point(225, 3);
            this.pnSoundDes.Name = "pnSoundDes";
            this.pnSoundDes.Size = new System.Drawing.Size(12, 12);
            this.pnSoundDes.TabIndex = 8;
            this.pnSoundDes.MouseLeave += new System.EventHandler(this.Dismiss_Toolkit);
            this.pnSoundDes.MouseHover += new System.EventHandler(this.pnSoundDes_MouseHover);
            // 
            // lbSoundTitle
            // 
            this.lbSoundTitle.AutoSize = true;
            this.lbSoundTitle.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoundTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbSoundTitle.Location = new System.Drawing.Point(57, 34);
            this.lbSoundTitle.Name = "lbSoundTitle";
            this.lbSoundTitle.Size = new System.Drawing.Size(39, 16);
            this.lbSoundTitle.TabIndex = 7;
            this.lbSoundTitle.Text = "Mute";
            // 
            // cbSound
            // 
            this.cbSound.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbSound.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.cbSound.Checked = true;
            this.cbSound.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbSound.ForeColor = System.Drawing.Color.White;
            this.cbSound.Location = new System.Drawing.Point(27, 32);
            this.cbSound.Name = "cbSound";
            this.cbSound.Size = new System.Drawing.Size(20, 20);
            this.cbSound.TabIndex = 6;
            this.cbSound.EnabledChanged += new System.EventHandler(this.cbInactiveApp_EnabledChanged);
            // 
            // lbInactiveApp
            // 
            this.lbInactiveApp.AutoSize = true;
            this.lbInactiveApp.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInactiveApp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbInactiveApp.Location = new System.Drawing.Point(283, 8);
            this.lbInactiveApp.Name = "lbInactiveApp";
            this.lbInactiveApp.Size = new System.Drawing.Size(122, 16);
            this.lbInactiveApp.TabIndex = 1;
            this.lbInactiveApp.Text = "Inactive applications";
            // 
            // pnInactiveApp
            // 
            this.pnInactiveApp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnInactiveApp.Controls.Add(this.pnInactiveAppDes);
            this.pnInactiveApp.Controls.Add(this.lbInactiveAppTitle);
            this.pnInactiveApp.Controls.Add(this.cbInactiveApp);
            this.pnInactiveApp.Location = new System.Drawing.Point(260, 17);
            this.pnInactiveApp.Name = "pnInactiveApp";
            this.pnInactiveApp.Size = new System.Drawing.Size(250, 85);
            this.pnInactiveApp.TabIndex = 2;
            // 
            // pnInactiveAppDes
            // 
            this.pnInactiveAppDes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnInactiveAppDes.BackgroundImage")));
            this.pnInactiveAppDes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnInactiveAppDes.Location = new System.Drawing.Point(231, 3);
            this.pnInactiveAppDes.Name = "pnInactiveAppDes";
            this.pnInactiveAppDes.Size = new System.Drawing.Size(12, 12);
            this.pnInactiveAppDes.TabIndex = 11;
            this.pnInactiveAppDes.MouseLeave += new System.EventHandler(this.Dismiss_Toolkit);
            this.pnInactiveAppDes.MouseHover += new System.EventHandler(this.pnInactiveAppDes_MouseHover);
            // 
            // lbInactiveAppTitle
            // 
            this.lbInactiveAppTitle.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInactiveAppTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbInactiveAppTitle.Location = new System.Drawing.Point(53, 31);
            this.lbInactiveAppTitle.Name = "lbInactiveAppTitle";
            this.lbInactiveAppTitle.Size = new System.Drawing.Size(186, 27);
            this.lbInactiveAppTitle.TabIndex = 9;
            this.lbInactiveAppTitle.Text = "Checking inactive applications";
            // 
            // cbInactiveApp
            // 
            this.cbInactiveApp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbInactiveApp.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.cbInactiveApp.Checked = true;
            this.cbInactiveApp.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbInactiveApp.ForeColor = System.Drawing.Color.White;
            this.cbInactiveApp.Location = new System.Drawing.Point(26, 29);
            this.cbInactiveApp.Name = "cbInactiveApp";
            this.cbInactiveApp.Size = new System.Drawing.Size(20, 20);
            this.cbInactiveApp.TabIndex = 8;
            this.cbInactiveApp.EnabledChanged += new System.EventHandler(this.cbInactiveApp_EnabledChanged);
            // 
            // lbRealTime
            // 
            this.lbRealTime.AutoSize = true;
            this.lbRealTime.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRealTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbRealTime.Location = new System.Drawing.Point(283, 226);
            this.lbRealTime.Name = "lbRealTime";
            this.lbRealTime.Size = new System.Drawing.Size(61, 16);
            this.lbRealTime.TabIndex = 3;
            this.lbRealTime.Text = "Real time";
            // 
            // pnRealTime
            // 
            this.pnRealTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnRealTime.Controls.Add(this.pnRealtimeDes);
            this.pnRealTime.Controls.Add(this.lbRealtimeInv);
            this.pnRealTime.Controls.Add(this.inpRealtimeScanInterval);
            this.pnRealTime.Controls.Add(this.lbRealTimeTitile);
            this.pnRealTime.Controls.Add(this.cbRealTime);
            this.pnRealTime.Location = new System.Drawing.Point(260, 234);
            this.pnRealTime.Name = "pnRealTime";
            this.pnRealTime.Size = new System.Drawing.Size(250, 85);
            this.pnRealTime.TabIndex = 4;
            // 
            // pnRealtimeDes
            // 
            this.pnRealtimeDes.BackgroundImage = global::CFML.Properties.Resources.HelpTooltipIcon;
            this.pnRealtimeDes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnRealtimeDes.Location = new System.Drawing.Point(231, 3);
            this.pnRealtimeDes.Name = "pnRealtimeDes";
            this.pnRealtimeDes.Size = new System.Drawing.Size(12, 12);
            this.pnRealtimeDes.TabIndex = 10;
            this.pnRealtimeDes.MouseLeave += new System.EventHandler(this.Dismiss_Toolkit);
            this.pnRealtimeDes.MouseHover += new System.EventHandler(this.pnRealtimeDes_MouseHover);
            // 
            // lbRealtimeInv
            // 
            this.lbRealtimeInv.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRealtimeInv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbRealtimeInv.Location = new System.Drawing.Point(75, 48);
            this.lbRealtimeInv.Name = "lbRealtimeInv";
            this.lbRealtimeInv.Size = new System.Drawing.Size(126, 21);
            this.lbRealtimeInv.TabIndex = 8;
            this.lbRealtimeInv.Text = "Scan Interval (sec)";
            // 
            // inpRealtimeScanInterval
            // 
            this.inpRealtimeScanInterval.BackColor = System.Drawing.SystemColors.Window;
            this.inpRealtimeScanInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpRealtimeScanInterval.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpRealtimeScanInterval.Location = new System.Drawing.Point(25, 48);
            this.inpRealtimeScanInterval.Multiline = true;
            this.inpRealtimeScanInterval.Name = "inpRealtimeScanInterval";
            this.inpRealtimeScanInterval.Size = new System.Drawing.Size(33, 22);
            this.inpRealtimeScanInterval.TabIndex = 7;
            this.inpRealtimeScanInterval.Text = "30";
            this.inpRealtimeScanInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.inpRealtimeScanInterval.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inpRealtimeScanInterval_KeyDown);
            this.inpRealtimeScanInterval.Validating += new System.ComponentModel.CancelEventHandler(this.validateScanInterValue_Validating);
            // 
            // lbRealTimeTitile
            // 
            this.lbRealTimeTitile.AutoSize = true;
            this.lbRealTimeTitile.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRealTimeTitile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbRealTimeTitile.Location = new System.Drawing.Point(50, 17);
            this.lbRealTimeTitile.Name = "lbRealTimeTitile";
            this.lbRealTimeTitile.Size = new System.Drawing.Size(105, 16);
            this.lbRealTimeTitile.TabIndex = 6;
            this.lbRealTimeTitile.Text = "Disable real-time";
            // 
            // cbRealTime
            // 
            this.cbRealTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbRealTime.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.cbRealTime.Checked = true;
            this.cbRealTime.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.cbRealTime.ForeColor = System.Drawing.Color.White;
            this.cbRealTime.Location = new System.Drawing.Point(25, 15);
            this.cbRealTime.Name = "cbRealTime";
            this.cbRealTime.Size = new System.Drawing.Size(20, 20);
            this.cbRealTime.TabIndex = 5;
            this.cbRealTime.OnChange += new System.EventHandler(this.cbRealTime_OnChange);
            this.cbRealTime.EnabledChanged += new System.EventHandler(this.cbInactiveApp_EnabledChanged);
            // 
            // btnApply
            // 
            this.btnApply.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnApply.BorderRadius = 0;
            this.btnApply.ButtonText = "Apply";
            this.btnApply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnApply.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnApply.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Iconcolor = System.Drawing.Color.Transparent;
            this.btnApply.Iconimage = null;
            this.btnApply.Iconimage_right = null;
            this.btnApply.Iconimage_right_Selected = null;
            this.btnApply.Iconimage_Selected = null;
            this.btnApply.IconMarginLeft = 0;
            this.btnApply.IconMarginRight = 0;
            this.btnApply.IconRightVisible = true;
            this.btnApply.IconRightZoom = 0D;
            this.btnApply.IconVisible = true;
            this.btnApply.IconZoom = 90D;
            this.btnApply.IsTab = false;
            this.btnApply.Location = new System.Drawing.Point(444, 583);
            this.btnApply.Name = "btnApply";
            this.btnApply.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnApply.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnApply.OnHoverTextColor = System.Drawing.Color.White;
            this.btnApply.selected = false;
            this.btnApply.Size = new System.Drawing.Size(116, 33);
            this.btnApply.TabIndex = 5;
            this.btnApply.Text = "Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnApply.Textcolor = System.Drawing.Color.White;
            this.btnApply.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // ttDescription
            // 
            this.ttDescription.ToolTipTitle = "Titile";
            // 
            // pnUCTitle
            // 
            this.pnUCTitle.Controls.Add(this.lbUCTitle);
            this.pnUCTitle.Controls.Add(this.pbUCPic);
            this.pnUCTitle.Location = new System.Drawing.Point(50, 16);
            this.pnUCTitle.Name = "pnUCTitle";
            this.pnUCTitle.Size = new System.Drawing.Size(472, 54);
            this.pnUCTitle.TabIndex = 16;
            // 
            // lbUCTitle
            // 
            this.lbUCTitle.AutoSize = true;
            this.lbUCTitle.Font = new System.Drawing.Font("Lato Heavy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUCTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUCTitle.Location = new System.Drawing.Point(57, 16);
            this.lbUCTitle.Name = "lbUCTitle";
            this.lbUCTitle.Size = new System.Drawing.Size(108, 19);
            this.lbUCTitle.TabIndex = 1;
            this.lbUCTitle.Text = "Configuration";
            // 
            // pbUCPic
            // 
            this.pbUCPic.BackgroundImage = global::CFML.Properties.Resources.ConfigIconColored;
            this.pbUCPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbUCPic.Location = new System.Drawing.Point(4, 4);
            this.pbUCPic.Name = "pbUCPic";
            this.pbUCPic.Size = new System.Drawing.Size(47, 47);
            this.pbUCPic.TabIndex = 0;
            this.pbUCPic.TabStop = false;
            // 
            // pnFunctionContainer
            // 
            this.pnFunctionContainer.Controls.Add(this.pnUpgradeVIPNote);
            this.pnFunctionContainer.Controls.Add(this.lbRealTime);
            this.pnFunctionContainer.Controls.Add(this.lbObjectTracking);
            this.pnFunctionContainer.Controls.Add(this.lbCensorSensitivity);
            this.pnFunctionContainer.Controls.Add(this.lbCensorObject);
            this.pnFunctionContainer.Controls.Add(this.pnCensorSensitivity);
            this.pnFunctionContainer.Controls.Add(this.pnCensorBox);
            this.pnFunctionContainer.Controls.Add(this.pnObjectTracking);
            this.pnFunctionContainer.Controls.Add(this.pnRealTime);
            this.pnFunctionContainer.Controls.Add(this.pnVIPFunctionOnly);
            this.pnFunctionContainer.Location = new System.Drawing.Point(50, 107);
            this.pnFunctionContainer.Name = "pnFunctionContainer";
            this.pnFunctionContainer.Size = new System.Drawing.Size(513, 472);
            this.pnFunctionContainer.TabIndex = 17;
            // 
            // pnUpgradeVIPNote
            // 
            this.pnUpgradeVIPNote.Controls.Add(this.lbUpgradeLink);
            this.pnUpgradeVIPNote.Controls.Add(this.label1);
            this.pnUpgradeVIPNote.Location = new System.Drawing.Point(3, 325);
            this.pnUpgradeVIPNote.Name = "pnUpgradeVIPNote";
            this.pnUpgradeVIPNote.Size = new System.Drawing.Size(506, 30);
            this.pnUpgradeVIPNote.TabIndex = 19;
            // 
            // lbUpgradeLink
            // 
            this.lbUpgradeLink.AutoSize = true;
            this.lbUpgradeLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpgradeLink.Location = new System.Drawing.Point(415, 7);
            this.lbUpgradeLink.Name = "lbUpgradeLink";
            this.lbUpgradeLink.Size = new System.Drawing.Size(86, 16);
            this.lbUpgradeLink.TabIndex = 6;
            this.lbUpgradeLink.TabStop = true;
            this.lbUpgradeLink.Text = "Upgrade here";
            this.lbUpgradeLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbUpgradeLink_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(361, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Upgrade to VIP account to explore the amazing function below";
            // 
            // pnVIPFunctionOnly
            // 
            this.pnVIPFunctionOnly.Controls.Add(this.lbInactiveApp);
            this.pnVIPFunctionOnly.Controls.Add(this.lbSound);
            this.pnVIPFunctionOnly.Controls.Add(this.pnSound);
            this.pnVIPFunctionOnly.Controls.Add(this.pnInactiveApp);
            this.pnVIPFunctionOnly.Location = new System.Drawing.Point(0, 365);
            this.pnVIPFunctionOnly.Name = "pnVIPFunctionOnly";
            this.pnVIPFunctionOnly.Size = new System.Drawing.Size(510, 105);
            this.pnVIPFunctionOnly.TabIndex = 19;
            // 
            // pnLoginNote
            // 
            this.pnLoginNote.Controls.Add(this.lbLoginLink);
            this.pnLoginNote.Controls.Add(this.lbLoginSuggestion);
            this.pnLoginNote.Location = new System.Drawing.Point(50, 76);
            this.pnLoginNote.Name = "pnLoginNote";
            this.pnLoginNote.Size = new System.Drawing.Size(513, 30);
            this.pnLoginNote.TabIndex = 18;
            // 
            // lbLoginLink
            // 
            this.lbLoginLink.AutoSize = true;
            this.lbLoginLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginLink.Location = new System.Drawing.Point(438, 8);
            this.lbLoginLink.Name = "lbLoginLink";
            this.lbLoginLink.Size = new System.Drawing.Size(71, 16);
            this.lbLoginLink.TabIndex = 6;
            this.lbLoginLink.TabStop = true;
            this.lbLoginLink.Text = "Log in here";
            this.lbLoginLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbLoginLink_LinkClicked);
            // 
            // lbLoginSuggestion
            // 
            this.lbLoginSuggestion.AutoSize = true;
            this.lbLoginSuggestion.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginSuggestion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbLoginSuggestion.Location = new System.Drawing.Point(3, 8);
            this.lbLoginSuggestion.Name = "lbLoginSuggestion";
            this.lbLoginSuggestion.Size = new System.Drawing.Size(242, 16);
            this.lbLoginSuggestion.TabIndex = 5;
            this.lbLoginSuggestion.Text = "Log in to explore these amazing functions";
            // 
            // configUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnLoginNote);
            this.Controls.Add(this.pnUCTitle);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.pnFunctionContainer);
            this.Name = "configUserControl";
            this.Size = new System.Drawing.Size(612, 643);
            this.pnCensorSensitivity.ResumeLayout(false);
            this.pnCensorBox.ResumeLayout(false);
            this.pnObjectTracking.ResumeLayout(false);
            this.pnSound.ResumeLayout(false);
            this.pnSound.PerformLayout();
            this.pnInactiveApp.ResumeLayout(false);
            this.pnRealTime.ResumeLayout(false);
            this.pnRealTime.PerformLayout();
            this.pnUCTitle.ResumeLayout(false);
            this.pnUCTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).EndInit();
            this.pnFunctionContainer.ResumeLayout(false);
            this.pnFunctionContainer.PerformLayout();
            this.pnUpgradeVIPNote.ResumeLayout(false);
            this.pnUpgradeVIPNote.PerformLayout();
            this.pnVIPFunctionOnly.ResumeLayout(false);
            this.pnVIPFunctionOnly.PerformLayout();
            this.pnLoginNote.ResumeLayout(false);
            this.pnLoginNote.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnCensorSensitivity;
        private System.Windows.Forms.Label lbCensorSensitivity;
        private System.Windows.Forms.Label lbCensorObject;
        private System.Windows.Forms.Panel pnCensorBox;
        private System.Windows.Forms.Label lbObjectTracking;
        private System.Windows.Forms.Panel pnObjectTracking;
        private System.Windows.Forms.Label lbSound;
        private System.Windows.Forms.Panel pnSound;
        private System.Windows.Forms.Label lbInactiveApp;
        private System.Windows.Forms.Panel pnInactiveApp;
        private System.Windows.Forms.Label lbRealTime;
        private System.Windows.Forms.Panel pnRealTime;
        private Bunifu.Framework.UI.BunifuCheckbox cbObjectTracking;
        private Bunifu.Framework.UI.BunifuCheckbox cbRealTime;
        private System.Windows.Forms.Label lbEnableObjectTrackingTitle;
        private System.Windows.Forms.Label lbRealTimeTitile;
        private System.Windows.Forms.Label lbSoundTitle;
        private Bunifu.Framework.UI.BunifuCheckbox cbSound;
        private System.Windows.Forms.Label lbInactiveAppTitle;
        private Bunifu.Framework.UI.BunifuCheckbox cbInactiveApp;
        private System.Windows.Forms.Label lbTrackbarSensitivityLevel;
        private Bunifu.Framework.UI.BunifuFlatButton btnApply;
        private System.Windows.Forms.Label lbCensorNumber;
        private System.Windows.Forms.Label lbRealtimeInv;
        private System.Windows.Forms.ToolTip ttDescription;
        private System.Windows.Forms.Panel pnUCTitle;
        private System.Windows.Forms.Label lbUCTitle;
        private System.Windows.Forms.PictureBox pbUCPic;
        private System.Windows.Forms.Panel pnSoundDes;
        private System.Windows.Forms.Panel pnObjectTrackingDes;
        private System.Windows.Forms.Panel pnInactiveAppDes;
        private System.Windows.Forms.Panel pnRealtimeDes;
        private System.Windows.Forms.Panel pnCensorSensitivityDes;
        private System.Windows.Forms.Panel pnCensorObjectDes;
        private System.Windows.Forms.Panel pnFunctionContainer;
        private System.Windows.Forms.Panel pnLoginNote;
        private System.Windows.Forms.LinkLabel lbLoginLink;
        private System.Windows.Forms.Label lbLoginSuggestion;
        private System.Windows.Forms.Panel pnVIPFunctionOnly;
        private System.Windows.Forms.Panel pnUpgradeVIPNote;
        private System.Windows.Forms.LinkLabel lbUpgradeLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox inpRealtimeScanInterval;
    }
}
