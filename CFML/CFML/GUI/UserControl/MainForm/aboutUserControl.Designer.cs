﻿namespace CFML.GUIDesign
{
    partial class aboutUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbDesContent = new System.Windows.Forms.Label();
            this.lbVersion = new System.Windows.Forms.Label();
            this.lbSendFeedbackLink = new System.Windows.Forms.LinkLabel();
            this.pnUCTitle = new System.Windows.Forms.Panel();
            this.lbUCTitle = new System.Windows.Forms.Label();
            this.pbUCPic = new System.Windows.Forms.PictureBox();
            this.pnUCTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).BeginInit();
            this.SuspendLayout();
            // 
            // lbDesContent
            // 
            this.lbDesContent.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDesContent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbDesContent.Location = new System.Drawing.Point(58, 110);
            this.lbDesContent.Name = "lbDesContent";
            this.lbDesContent.Size = new System.Drawing.Size(364, 110);
            this.lbDesContent.TabIndex = 0;
            this.lbDesContent.Text = "CFML is a software.....";
            // 
            // lbVersion
            // 
            this.lbVersion.AutoSize = true;
            this.lbVersion.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbVersion.Location = new System.Drawing.Point(58, 257);
            this.lbVersion.Name = "lbVersion";
            this.lbVersion.Size = new System.Drawing.Size(81, 16);
            this.lbVersion.TabIndex = 0;
            this.lbVersion.Text = "Version 1.10";
            // 
            // lbSendFeedbackLink
            // 
            this.lbSendFeedbackLink.AutoSize = true;
            this.lbSendFeedbackLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSendFeedbackLink.Location = new System.Drawing.Point(317, 257);
            this.lbSendFeedbackLink.Name = "lbSendFeedbackLink";
            this.lbSendFeedbackLink.Size = new System.Drawing.Size(92, 16);
            this.lbSendFeedbackLink.TabIndex = 1;
            this.lbSendFeedbackLink.TabStop = true;
            this.lbSendFeedbackLink.Text = "Send Feedback";
            this.lbSendFeedbackLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbSendFeedbackLink_LinkClicked);
            // 
            // pnUCTitle
            // 
            this.pnUCTitle.Controls.Add(this.lbUCTitle);
            this.pnUCTitle.Controls.Add(this.pbUCPic);
            this.pnUCTitle.Location = new System.Drawing.Point(57, 21);
            this.pnUCTitle.Name = "pnUCTitle";
            this.pnUCTitle.Size = new System.Drawing.Size(472, 54);
            this.pnUCTitle.TabIndex = 9;
            // 
            // lbUCTitle
            // 
            this.lbUCTitle.AutoSize = true;
            this.lbUCTitle.Font = new System.Drawing.Font("Lato Heavy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUCTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUCTitle.Location = new System.Drawing.Point(57, 16);
            this.lbUCTitle.Name = "lbUCTitle";
            this.lbUCTitle.Size = new System.Drawing.Size(53, 19);
            this.lbUCTitle.TabIndex = 1;
            this.lbUCTitle.Text = "About";
            // 
            // pbUCPic
            // 
            this.pbUCPic.BackgroundImage = global::CFML.Properties.Resources.AboutIconColored;
            this.pbUCPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbUCPic.Location = new System.Drawing.Point(4, 4);
            this.pbUCPic.Name = "pbUCPic";
            this.pbUCPic.Size = new System.Drawing.Size(47, 47);
            this.pbUCPic.TabIndex = 0;
            this.pbUCPic.TabStop = false;
            // 
            // aboutUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnUCTitle);
            this.Controls.Add(this.lbSendFeedbackLink);
            this.Controls.Add(this.lbVersion);
            this.Controls.Add(this.lbDesContent);
            this.Name = "aboutUserControl";
            this.Size = new System.Drawing.Size(612, 643);
            this.pnUCTitle.ResumeLayout(false);
            this.pnUCTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbDesContent;
        private System.Windows.Forms.Label lbVersion;
        private System.Windows.Forms.LinkLabel lbSendFeedbackLink;
        private System.Windows.Forms.Panel pnUCTitle;
        private System.Windows.Forms.Label lbUCTitle;
        private System.Windows.Forms.PictureBox pbUCPic;
    }
}
