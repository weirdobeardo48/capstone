﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.Common;
using System.Diagnostics;

namespace CFML.GUIDesign
{
    public partial class accountUserControl : UserControl
    {
        private userDetail user;
        private mainForm parent;

        public accountUserControl()
        {
            InitializeComponent();
            Initiate();
        }

        public accountUserControl(userDetail user)
        {
            InitializeComponent();
            Initiate();
            SetUser(user);
        }

        private void Initiate()
        {
            lbGuestWelcomeContent.Text = ComponentDescriptionText.UC_ACCOUNT_GUEST_WELCOME_NOTE;
            pnGuestWelcomeNote.Location = new Point(50, 113);
        }

        public void SetMainForm(mainForm parent)
        {
            this.parent = parent;
        }

        public void SetUser(userDetail user)
        {
            this.user = user;
            updateFormByUserType();
        }

        public void updateFormByUserType()
        {
            switch (user.UserType.ToString())
            {
                // Guest
                case "0":
                    {
                        pnUserInfo.Visible = false;
                        pnGuestWelcomeNote.Visible = true;
                        break;
                    }
                // Normal
                case "1":
                    {
                        lbUpgradeToVIPLink.Visible = true;
                        pnUserInfo.Visible = true;
                        pnGuestWelcomeNote.Visible = false;
                        lbExp.Visible = false;
                        lbAccountName.Text = user.OwnerName;
                        lbAccountEmail.Text = "Email: " + user.Email;
                        lbVIPStatus.Text = "Status: " + ComponentDescriptionText.UC_ACCOUNT_STATUS_VIP;
                        break;
                    }
                // VIP
                case "2":
                    {
                        lbUpgradeToVIPLink.Visible = false;
                        pnUserInfo.Visible = true;
                        pnGuestWelcomeNote.Visible = false;
                        lbExp.Visible = true;
                        lbAccountName.Text = user.OwnerName;
                        lbAccountEmail.Text = "Email: " + user.Email;
                        lbVIPStatus.Text = "Status: " + ComponentDescriptionText.UC_ACCOUNT_STATUS_VIP;
                        try 
                        {
                            lbExp.Text = "Expired Date: " + String.Format("{0:dd MMM yyyy}", user.ExpiredDate);
                        }
                        catch
                        {
                            Console.WriteLine("No Expired Date");
                            lbExp.Text = "Expired Date: Unknown";
                        }
                        
                        break;
                    }
            }
        }

        private void lbLoginLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            parent.Logout();
        }

        private void lbEditProfileLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_EDIT_PROFILE);
        }

        private void lbUpgradeToVIPLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_UPGRADE_ACCOUNT);
        }

        private void lbChangePasswordLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_CHANGE_PASSWORD);
        }

        private void lbForgetPasswordLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_FORGET_PASSWORD);
        }

        private void lbRegisterLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_REGISTER);
        }
    }
}
