﻿namespace CFML.GUIDesign
{
    partial class changeKeyUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(changeKeyUserControl));
            this.pnUCTitle = new System.Windows.Forms.Panel();
            this.lbUCTitle = new System.Windows.Forms.Label();
            this.pbUCPic = new System.Windows.Forms.PictureBox();
            this.pnInputForm = new System.Windows.Forms.Panel();
            this.lbLoginSuggestion = new System.Windows.Forms.Label();
            this.pnLoginNote = new System.Windows.Forms.Panel();
            this.lbLoginLink = new System.Windows.Forms.LinkLabel();
            this.btnChangeKey = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pnUCTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).BeginInit();
            this.pnLoginNote.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnUCTitle
            // 
            this.pnUCTitle.Controls.Add(this.lbUCTitle);
            this.pnUCTitle.Controls.Add(this.pbUCPic);
            this.pnUCTitle.Location = new System.Drawing.Point(50, 22);
            this.pnUCTitle.Name = "pnUCTitle";
            this.pnUCTitle.Size = new System.Drawing.Size(472, 54);
            this.pnUCTitle.TabIndex = 15;
            // 
            // lbUCTitle
            // 
            this.lbUCTitle.AutoSize = true;
            this.lbUCTitle.Font = new System.Drawing.Font("Lato Heavy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUCTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUCTitle.Location = new System.Drawing.Point(57, 16);
            this.lbUCTitle.Name = "lbUCTitle";
            this.lbUCTitle.Size = new System.Drawing.Size(192, 19);
            this.lbUCTitle.TabIndex = 1;
            this.lbUCTitle.Text = "Change Master Password";
            // 
            // pbUCPic
            // 
            this.pbUCPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbUCPic.BackgroundImage")));
            this.pbUCPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbUCPic.Location = new System.Drawing.Point(4, 4);
            this.pbUCPic.Name = "pbUCPic";
            this.pbUCPic.Size = new System.Drawing.Size(47, 47);
            this.pbUCPic.TabIndex = 0;
            this.pbUCPic.TabStop = false;
            // 
            // pnInputForm
            // 
            this.pnInputForm.Location = new System.Drawing.Point(50, 129);
            this.pnInputForm.Name = "pnInputForm";
            this.pnInputForm.Size = new System.Drawing.Size(319, 147);
            this.pnInputForm.TabIndex = 16;
            // 
            // lbLoginSuggestion
            // 
            this.lbLoginSuggestion.AutoSize = true;
            this.lbLoginSuggestion.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginSuggestion.Location = new System.Drawing.Point(3, 8);
            this.lbLoginSuggestion.Name = "lbLoginSuggestion";
            this.lbLoginSuggestion.Size = new System.Drawing.Size(242, 16);
            this.lbLoginSuggestion.TabIndex = 5;
            this.lbLoginSuggestion.Text = "Log in to explore these amazing functions";
            // 
            // pnLoginNote
            // 
            this.pnLoginNote.Controls.Add(this.lbLoginLink);
            this.pnLoginNote.Controls.Add(this.lbLoginSuggestion);
            this.pnLoginNote.Location = new System.Drawing.Point(46, 82);
            this.pnLoginNote.Name = "pnLoginNote";
            this.pnLoginNote.Size = new System.Drawing.Size(409, 30);
            this.pnLoginNote.TabIndex = 17;
            // 
            // lbLoginLink
            // 
            this.lbLoginLink.AutoSize = true;
            this.lbLoginLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginLink.Location = new System.Drawing.Point(330, 8);
            this.lbLoginLink.Name = "lbLoginLink";
            this.lbLoginLink.Size = new System.Drawing.Size(71, 16);
            this.lbLoginLink.TabIndex = 6;
            this.lbLoginLink.TabStop = true;
            this.lbLoginLink.Text = "Log in here";
            this.lbLoginLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbLoginLink_LinkClicked);
            // 
            // btnChangeKey
            // 
            this.btnChangeKey.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnChangeKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnChangeKey.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnChangeKey.BorderRadius = 0;
            this.btnChangeKey.ButtonText = "Change";
            this.btnChangeKey.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeKey.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnChangeKey.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeKey.Iconcolor = System.Drawing.Color.Transparent;
            this.btnChangeKey.Iconimage = null;
            this.btnChangeKey.Iconimage_right = null;
            this.btnChangeKey.Iconimage_right_Selected = null;
            this.btnChangeKey.Iconimage_Selected = null;
            this.btnChangeKey.IconMarginLeft = 0;
            this.btnChangeKey.IconMarginRight = 0;
            this.btnChangeKey.IconRightVisible = true;
            this.btnChangeKey.IconRightZoom = 0D;
            this.btnChangeKey.IconVisible = true;
            this.btnChangeKey.IconZoom = 90D;
            this.btnChangeKey.IsTab = false;
            this.btnChangeKey.Location = new System.Drawing.Point(50, 283);
            this.btnChangeKey.Margin = new System.Windows.Forms.Padding(4);
            this.btnChangeKey.Name = "btnChangeKey";
            this.btnChangeKey.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnChangeKey.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnChangeKey.OnHoverTextColor = System.Drawing.Color.White;
            this.btnChangeKey.selected = false;
            this.btnChangeKey.Size = new System.Drawing.Size(104, 29);
            this.btnChangeKey.TabIndex = 4;
            this.btnChangeKey.Text = "Change";
            this.btnChangeKey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnChangeKey.Textcolor = System.Drawing.Color.White;
            this.btnChangeKey.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeKey.Click += new System.EventHandler(this.btnChangeKey_Click);
            // 
            // changeKeyUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnChangeKey);
            this.Controls.Add(this.pnLoginNote);
            this.Controls.Add(this.pnInputForm);
            this.Controls.Add(this.pnUCTitle);
            this.Name = "changeKeyUserControl";
            this.Size = new System.Drawing.Size(612, 643);
            this.pnUCTitle.ResumeLayout(false);
            this.pnUCTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).EndInit();
            this.pnLoginNote.ResumeLayout(false);
            this.pnLoginNote.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnUCTitle;
        private System.Windows.Forms.Label lbUCTitle;
        private System.Windows.Forms.PictureBox pbUCPic;
        private System.Windows.Forms.Panel pnInputForm;
        private System.Windows.Forms.Label lbLoginSuggestion;
        private System.Windows.Forms.Panel pnLoginNote;
        private System.Windows.Forms.LinkLabel lbLoginLink;
        private Bunifu.Framework.UI.BunifuFlatButton btnChangeKey;
    }
}
