﻿namespace CFML.GUIDesign
{
    partial class customizeUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnPictureShowArea = new System.Windows.Forms.Panel();
            this.pnImgPreview = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnImagePreviewShow = new System.Windows.Forms.Panel();
            this.pnPictureOption = new System.Windows.Forms.Panel();
            this.optNotUseImage = new System.Windows.Forms.RadioButton();
            this.optUseImage = new System.Windows.Forms.RadioButton();
            this.flowLayoutBgColor = new System.Windows.Forms.FlowLayoutPanel();
            this.pnBackgroundColorArea = new System.Windows.Forms.Panel();
            this.inpColorHex = new System.Windows.Forms.Label();
            this.pnColorPlt = new System.Windows.Forms.Panel();
            this.pnVerticalColorPicker = new System.Windows.Forms.FlowLayoutPanel();
            this.pnColorPanel2D = new System.Windows.Forms.Panel();
            this.lbColorPreview = new System.Windows.Forms.Label();
            this.pnColorPreview = new System.Windows.Forms.Panel();
            this.pnOldBgColor = new System.Windows.Forms.Panel();
            this.lbPictureBox = new System.Windows.Forms.Label();
            this.lbBackgroundBox = new System.Windows.Forms.Label();
            this.lbUCTitle = new System.Windows.Forms.Label();
            this.pnUCTitle = new System.Windows.Forms.Panel();
            this.pbUCPic = new System.Windows.Forms.PictureBox();
            this.btnApply = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pnFunctionContainer = new System.Windows.Forms.Panel();
            this.pnUpgradeVIPNote = new System.Windows.Forms.Panel();
            this.lbUpgradeLink = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnLoginNote = new System.Windows.Forms.Panel();
            this.lbLoginLink = new System.Windows.Forms.LinkLabel();
            this.lbLoginSuggestion = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnPictureShowArea.SuspendLayout();
            this.pnImgPreview.SuspendLayout();
            this.pnPictureOption.SuspendLayout();
            this.flowLayoutBgColor.SuspendLayout();
            this.pnBackgroundColorArea.SuspendLayout();
            this.pnColorPlt.SuspendLayout();
            this.pnUCTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).BeginInit();
            this.pnFunctionContainer.SuspendLayout();
            this.pnUpgradeVIPNote.SuspendLayout();
            this.pnLoginNote.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.pnPictureShowArea);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(504, 201);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // pnPictureShowArea
            // 
            this.pnPictureShowArea.Controls.Add(this.pnImgPreview);
            this.pnPictureShowArea.Controls.Add(this.pnPictureOption);
            this.pnPictureShowArea.Location = new System.Drawing.Point(3, 3);
            this.pnPictureShowArea.Name = "pnPictureShowArea";
            this.pnPictureShowArea.Size = new System.Drawing.Size(458, 191);
            this.pnPictureShowArea.TabIndex = 0;
            // 
            // pnImgPreview
            // 
            this.pnImgPreview.Controls.Add(this.label1);
            this.pnImgPreview.Controls.Add(this.pnImagePreviewShow);
            this.pnImgPreview.Location = new System.Drawing.Point(273, 3);
            this.pnImgPreview.Name = "pnImgPreview";
            this.pnImgPreview.Size = new System.Drawing.Size(175, 183);
            this.pnImgPreview.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label1.Location = new System.Drawing.Point(8, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "Preview";
            // 
            // pnImagePreviewShow
            // 
            this.pnImagePreviewShow.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pnImagePreviewShow.BackgroundImage = global::CFML.Properties.Resources.CensorBg;
            this.pnImagePreviewShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnImagePreviewShow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnImagePreviewShow.Location = new System.Drawing.Point(7, 32);
            this.pnImagePreviewShow.Name = "pnImagePreviewShow";
            this.pnImagePreviewShow.Size = new System.Drawing.Size(160, 145);
            this.pnImagePreviewShow.TabIndex = 0;
            this.pnImagePreviewShow.MouseHover += new System.EventHandler(this.pnImagePreviewShow_Enter);
            // 
            // pnPictureOption
            // 
            this.pnPictureOption.Controls.Add(this.optNotUseImage);
            this.pnPictureOption.Controls.Add(this.optUseImage);
            this.pnPictureOption.Location = new System.Drawing.Point(49, 55);
            this.pnPictureOption.Name = "pnPictureOption";
            this.pnPictureOption.Size = new System.Drawing.Size(186, 91);
            this.pnPictureOption.TabIndex = 19;
            // 
            // optNotUseImage
            // 
            this.optNotUseImage.AutoSize = true;
            this.optNotUseImage.Checked = true;
            this.optNotUseImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.optNotUseImage.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optNotUseImage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.optNotUseImage.Location = new System.Drawing.Point(3, 13);
            this.optNotUseImage.Name = "optNotUseImage";
            this.optNotUseImage.Size = new System.Drawing.Size(124, 20);
            this.optNotUseImage.TabIndex = 0;
            this.optNotUseImage.TabStop = true;
            this.optNotUseImage.Text = "Do not use image";
            this.optNotUseImage.UseVisualStyleBackColor = true;
            this.optNotUseImage.CheckedChanged += new System.EventHandler(this.optCheckedChange);
            // 
            // optUseImage
            // 
            this.optUseImage.AutoSize = true;
            this.optUseImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.optUseImage.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optUseImage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.optUseImage.Location = new System.Drawing.Point(3, 52);
            this.optUseImage.Name = "optUseImage";
            this.optUseImage.Size = new System.Drawing.Size(167, 20);
            this.optUseImage.TabIndex = 18;
            this.optUseImage.Text = "Use this following image:";
            this.optUseImage.UseVisualStyleBackColor = true;
            this.optUseImage.CheckedChanged += new System.EventHandler(this.optCheckedChange);
            // 
            // flowLayoutBgColor
            // 
            this.flowLayoutBgColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutBgColor.Controls.Add(this.pnBackgroundColorArea);
            this.flowLayoutBgColor.Location = new System.Drawing.Point(3, 233);
            this.flowLayoutBgColor.Name = "flowLayoutBgColor";
            this.flowLayoutBgColor.Size = new System.Drawing.Size(504, 234);
            this.flowLayoutBgColor.TabIndex = 0;
            // 
            // pnBackgroundColorArea
            // 
            this.pnBackgroundColorArea.Controls.Add(this.inpColorHex);
            this.pnBackgroundColorArea.Controls.Add(this.pnColorPlt);
            this.pnBackgroundColorArea.Controls.Add(this.lbColorPreview);
            this.pnBackgroundColorArea.Controls.Add(this.pnColorPreview);
            this.pnBackgroundColorArea.Controls.Add(this.pnOldBgColor);
            this.pnBackgroundColorArea.Location = new System.Drawing.Point(3, 3);
            this.pnBackgroundColorArea.Name = "pnBackgroundColorArea";
            this.pnBackgroundColorArea.Size = new System.Drawing.Size(458, 227);
            this.pnBackgroundColorArea.TabIndex = 0;
            // 
            // inpColorHex
            // 
            this.inpColorHex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.inpColorHex.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpColorHex.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpColorHex.Location = new System.Drawing.Point(323, 185);
            this.inpColorHex.Name = "inpColorHex";
            this.inpColorHex.Size = new System.Drawing.Size(100, 28);
            this.inpColorHex.TabIndex = 7;
            this.inpColorHex.Text = "#";
            // 
            // pnColorPlt
            // 
            this.pnColorPlt.BackColor = System.Drawing.SystemColors.Control;
            this.pnColorPlt.Controls.Add(this.pnVerticalColorPicker);
            this.pnColorPlt.Controls.Add(this.pnColorPanel2D);
            this.pnColorPlt.Location = new System.Drawing.Point(228, 4);
            this.pnColorPlt.Name = "pnColorPlt";
            this.pnColorPlt.Size = new System.Drawing.Size(225, 169);
            this.pnColorPlt.TabIndex = 5;
            // 
            // pnVerticalColorPicker
            // 
            this.pnVerticalColorPicker.Location = new System.Drawing.Point(194, 3);
            this.pnVerticalColorPicker.Name = "pnVerticalColorPicker";
            this.pnVerticalColorPicker.Size = new System.Drawing.Size(25, 166);
            this.pnVerticalColorPicker.TabIndex = 1;
            // 
            // pnColorPanel2D
            // 
            this.pnColorPanel2D.Location = new System.Drawing.Point(43, 3);
            this.pnColorPanel2D.Name = "pnColorPanel2D";
            this.pnColorPanel2D.Size = new System.Drawing.Size(176, 166);
            this.pnColorPanel2D.TabIndex = 0;
            // 
            // lbColorPreview
            // 
            this.lbColorPreview.AutoSize = true;
            this.lbColorPreview.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbColorPreview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbColorPreview.Location = new System.Drawing.Point(55, 157);
            this.lbColorPreview.Name = "lbColorPreview";
            this.lbColorPreview.Size = new System.Drawing.Size(89, 16);
            this.lbColorPreview.TabIndex = 1;
            this.lbColorPreview.Text = "Color Preview";
            // 
            // pnColorPreview
            // 
            this.pnColorPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnColorPreview.Location = new System.Drawing.Point(56, 51);
            this.pnColorPreview.Name = "pnColorPreview";
            this.pnColorPreview.Size = new System.Drawing.Size(68, 69);
            this.pnColorPreview.TabIndex = 0;
            // 
            // pnOldBgColor
            // 
            this.pnOldBgColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnOldBgColor.Location = new System.Drawing.Point(98, 95);
            this.pnOldBgColor.Name = "pnOldBgColor";
            this.pnOldBgColor.Size = new System.Drawing.Size(47, 43);
            this.pnOldBgColor.TabIndex = 1;
            // 
            // lbPictureBox
            // 
            this.lbPictureBox.AutoSize = true;
            this.lbPictureBox.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPictureBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbPictureBox.Location = new System.Drawing.Point(29, 9);
            this.lbPictureBox.Name = "lbPictureBox";
            this.lbPictureBox.Size = new System.Drawing.Size(195, 16);
            this.lbPictureBox.TabIndex = 0;
            this.lbPictureBox.Text = "Displayed image on censor boxes";
            // 
            // lbBackgroundBox
            // 
            this.lbBackgroundBox.AutoSize = true;
            this.lbBackgroundBox.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBackgroundBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbBackgroundBox.Location = new System.Drawing.Point(29, 227);
            this.lbBackgroundBox.Name = "lbBackgroundBox";
            this.lbBackgroundBox.Size = new System.Drawing.Size(198, 16);
            this.lbBackgroundBox.TabIndex = 1;
            this.lbBackgroundBox.Text = "Background color of censor boxes";
            // 
            // lbUCTitle
            // 
            this.lbUCTitle.AutoSize = true;
            this.lbUCTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUCTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUCTitle.Location = new System.Drawing.Point(57, 16);
            this.lbUCTitle.Name = "lbUCTitle";
            this.lbUCTitle.Size = new System.Drawing.Size(94, 19);
            this.lbUCTitle.TabIndex = 1;
            this.lbUCTitle.Text = "Customize";
            // 
            // pnUCTitle
            // 
            this.pnUCTitle.Controls.Add(this.lbUCTitle);
            this.pnUCTitle.Controls.Add(this.pbUCPic);
            this.pnUCTitle.Location = new System.Drawing.Point(50, 20);
            this.pnUCTitle.Name = "pnUCTitle";
            this.pnUCTitle.Size = new System.Drawing.Size(472, 54);
            this.pnUCTitle.TabIndex = 17;
            // 
            // pbUCPic
            // 
            this.pbUCPic.BackgroundImage = global::CFML.Properties.Resources.CustomizeIconColored;
            this.pbUCPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbUCPic.Location = new System.Drawing.Point(4, 4);
            this.pbUCPic.Name = "pbUCPic";
            this.pbUCPic.Size = new System.Drawing.Size(47, 47);
            this.pbUCPic.TabIndex = 0;
            this.pbUCPic.TabStop = false;
            // 
            // btnApply
            // 
            this.btnApply.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnApply.BorderRadius = 0;
            this.btnApply.ButtonText = "Apply";
            this.btnApply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnApply.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnApply.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Iconcolor = System.Drawing.Color.Transparent;
            this.btnApply.Iconimage = null;
            this.btnApply.Iconimage_right = null;
            this.btnApply.Iconimage_right_Selected = null;
            this.btnApply.Iconimage_Selected = null;
            this.btnApply.IconMarginLeft = 0;
            this.btnApply.IconMarginRight = 0;
            this.btnApply.IconRightVisible = true;
            this.btnApply.IconRightZoom = 0D;
            this.btnApply.IconVisible = true;
            this.btnApply.IconZoom = 90D;
            this.btnApply.IsTab = false;
            this.btnApply.Location = new System.Drawing.Point(458, 596);
            this.btnApply.Name = "btnApply";
            this.btnApply.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnApply.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnApply.OnHoverTextColor = System.Drawing.Color.White;
            this.btnApply.selected = false;
            this.btnApply.Size = new System.Drawing.Size(99, 26);
            this.btnApply.TabIndex = 6;
            this.btnApply.Text = "Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnApply.Textcolor = System.Drawing.Color.White;
            this.btnApply.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // pnFunctionContainer
            // 
            this.pnFunctionContainer.Controls.Add(this.lbPictureBox);
            this.pnFunctionContainer.Controls.Add(this.lbBackgroundBox);
            this.pnFunctionContainer.Controls.Add(this.flowLayoutPanel1);
            this.pnFunctionContainer.Controls.Add(this.flowLayoutBgColor);
            this.pnFunctionContainer.Location = new System.Drawing.Point(50, 117);
            this.pnFunctionContainer.Name = "pnFunctionContainer";
            this.pnFunctionContainer.Size = new System.Drawing.Size(510, 473);
            this.pnFunctionContainer.TabIndex = 1;
            // 
            // pnUpgradeVIPNote
            // 
            this.pnUpgradeVIPNote.Controls.Add(this.lbUpgradeLink);
            this.pnUpgradeVIPNote.Controls.Add(this.label2);
            this.pnUpgradeVIPNote.Location = new System.Drawing.Point(50, 80);
            this.pnUpgradeVIPNote.Name = "pnUpgradeVIPNote";
            this.pnUpgradeVIPNote.Size = new System.Drawing.Size(486, 30);
            this.pnUpgradeVIPNote.TabIndex = 21;
            // 
            // lbUpgradeLink
            // 
            this.lbUpgradeLink.AutoSize = true;
            this.lbUpgradeLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpgradeLink.Location = new System.Drawing.Point(392, 7);
            this.lbUpgradeLink.Name = "lbUpgradeLink";
            this.lbUpgradeLink.Size = new System.Drawing.Size(86, 16);
            this.lbUpgradeLink.TabIndex = 6;
            this.lbUpgradeLink.TabStop = true;
            this.lbUpgradeLink.Text = "Upgrade here";
            this.lbUpgradeLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbUpgradeLink_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label2.Location = new System.Drawing.Point(3, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(361, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Upgrade to VIP account to explore the amazing function below";
            // 
            // pnLoginNote
            // 
            this.pnLoginNote.Controls.Add(this.lbLoginLink);
            this.pnLoginNote.Controls.Add(this.lbLoginSuggestion);
            this.pnLoginNote.Location = new System.Drawing.Point(50, 80);
            this.pnLoginNote.Name = "pnLoginNote";
            this.pnLoginNote.Size = new System.Drawing.Size(486, 30);
            this.pnLoginNote.TabIndex = 20;
            // 
            // lbLoginLink
            // 
            this.lbLoginLink.AutoSize = true;
            this.lbLoginLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginLink.Location = new System.Drawing.Point(406, 8);
            this.lbLoginLink.Name = "lbLoginLink";
            this.lbLoginLink.Size = new System.Drawing.Size(71, 16);
            this.lbLoginLink.TabIndex = 6;
            this.lbLoginLink.TabStop = true;
            this.lbLoginLink.Text = "Log in here";
            this.lbLoginLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbLoginLink_LinkClicked);
            // 
            // lbLoginSuggestion
            // 
            this.lbLoginSuggestion.AutoSize = true;
            this.lbLoginSuggestion.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginSuggestion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbLoginSuggestion.Location = new System.Drawing.Point(3, 8);
            this.lbLoginSuggestion.Name = "lbLoginSuggestion";
            this.lbLoginSuggestion.Size = new System.Drawing.Size(242, 16);
            this.lbLoginSuggestion.TabIndex = 5;
            this.lbLoginSuggestion.Text = "Log in to explore these amazing functions";
            // 
            // customizeUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.pnUpgradeVIPNote);
            this.Controls.Add(this.pnLoginNote);
            this.Controls.Add(this.pnUCTitle);
            this.Controls.Add(this.pnFunctionContainer);
            this.Name = "customizeUserControl";
            this.Size = new System.Drawing.Size(612, 643);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.pnPictureShowArea.ResumeLayout(false);
            this.pnImgPreview.ResumeLayout(false);
            this.pnImgPreview.PerformLayout();
            this.pnPictureOption.ResumeLayout(false);
            this.pnPictureOption.PerformLayout();
            this.flowLayoutBgColor.ResumeLayout(false);
            this.pnBackgroundColorArea.ResumeLayout(false);
            this.pnBackgroundColorArea.PerformLayout();
            this.pnColorPlt.ResumeLayout(false);
            this.pnUCTitle.ResumeLayout(false);
            this.pnUCTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).EndInit();
            this.pnFunctionContainer.ResumeLayout(false);
            this.pnFunctionContainer.PerformLayout();
            this.pnUpgradeVIPNote.ResumeLayout(false);
            this.pnUpgradeVIPNote.PerformLayout();
            this.pnLoginNote.ResumeLayout(false);
            this.pnLoginNote.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutBgColor;
        private System.Windows.Forms.Label lbPictureBox;
        private System.Windows.Forms.Label lbBackgroundBox;
        private System.Windows.Forms.Panel pnPictureShowArea;
        private System.Windows.Forms.Panel pnImagePreviewShow;
        private System.Windows.Forms.PictureBox pbUCPic;
        private System.Windows.Forms.Label lbUCTitle;
        private System.Windows.Forms.Panel pnUCTitle;
        private System.Windows.Forms.Panel pnBackgroundColorArea;
        private System.Windows.Forms.Panel pnColorPlt;
        private System.Windows.Forms.FlowLayoutPanel pnVerticalColorPicker;
        private System.Windows.Forms.Panel pnColorPanel2D;
        private System.Windows.Forms.Label lbColorPreview;
        private System.Windows.Forms.Panel pnColorPreview;
        private System.Windows.Forms.Panel pnOldBgColor;
        private System.Windows.Forms.RadioButton optUseImage;
        private System.Windows.Forms.RadioButton optNotUseImage;
        private System.Windows.Forms.Panel pnPictureOption;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnImgPreview;
        private System.Windows.Forms.Label inpColorHex;
        private Bunifu.Framework.UI.BunifuFlatButton btnApply;
        private System.Windows.Forms.Panel pnFunctionContainer;
        private System.Windows.Forms.Panel pnUpgradeVIPNote;
        private System.Windows.Forms.LinkLabel lbUpgradeLink;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnLoginNote;
        private System.Windows.Forms.LinkLabel lbLoginLink;
        private System.Windows.Forms.Label lbLoginSuggestion;
    }
}
