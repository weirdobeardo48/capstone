﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.GUIDesign.CustomizeClass;
using Microsoft.VisualBasic;
using CFML.Common;
using System.IO;
using CFML.GUIDesign.CustomizeComponent;

namespace CFML.GUIDesign
{
    public partial class changeKeyUserControl : UserControl
    {
        #region Variable
        private CustomizeTextBox tbOldKey;
        private CustomizeTextBox tbNewKey;
        private CustomizeTextBox tbNewKeyCf;
        private userDetail user;
        private mainForm parent;

        private CustomNotificationBox cnb;
        #endregion

        public changeKeyUserControl()
        {
            InitializeComponent();
            Initiate();
        }

        private void Initiate()
        {
            #region Init Text Box
            tbOldKey = new CustomizeTextBox(Common.ComponentDescriptionText.UC_CHANGE_KEY_OLD);
            tbOldKey.Size = new Size(217, 25);
            tbOldKey.Location = new Point(1, 1);
            tbOldKey.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbOldKey.Multiline = true;
            tbOldKey.Name = "tbOldKey";
            tbOldKey.TabIndex = 0;
            tbOldKey.PasswordChar = ControlChars.NullChar;
            tbOldKey.BorderStyle = BorderStyle.FixedSingle;

            tbNewKey = new CustomizeTextBox(Common.ComponentDescriptionText.UC_CHANGE_KEY_NEW);
            tbNewKey.Size = new Size(217, 25);
            tbNewKey.Location = new Point(1, 51);
            tbNewKey.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbNewKey.Multiline = true;
            tbNewKey.Name = "tbNewKey";
            tbNewKey.TabIndex = 0;
            tbNewKey.PasswordChar = ControlChars.NullChar;
            tbNewKey.BorderStyle = BorderStyle.FixedSingle;

            tbNewKeyCf = new CustomizeTextBox(Common.ComponentDescriptionText.UC_CHANGE_KEY_NEW_CF);
            tbNewKeyCf.Size = new Size(217, 25);
            tbNewKeyCf.Location = new Point(1, 101);
            tbNewKeyCf.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbNewKeyCf.Multiline = true;
            tbNewKeyCf.Name = "tbNewKeyCf";
            tbNewKeyCf.TabIndex = 0;
            tbNewKeyCf.PasswordChar = ControlChars.NullChar;
            tbNewKeyCf.BorderStyle = BorderStyle.FixedSingle;


            pnInputForm.Controls.Add(tbOldKey);
            pnInputForm.Controls.Add(tbNewKey);
            pnInputForm.Controls.Add(tbNewKeyCf);
            #endregion

        }

        private void Initiate(userDetail user)
        {
            #region Init Text Box
            tbOldKey = new CustomizeTextBox(Common.ComponentDescriptionText.UC_CHANGE_KEY_OLD);
            tbOldKey.Size = new Size(217, 30);
            tbOldKey.Location = new Point(1, 1);
            tbOldKey.Font = new Font("Tahoma", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            tbOldKey.Multiline = true;
            tbOldKey.Name = "tbOldKey";
            tbOldKey.TabIndex = 0;
            tbOldKey.PasswordChar = ControlChars.NullChar;

            tbNewKey = new CustomizeTextBox(Common.ComponentDescriptionText.UC_CHANGE_KEY_NEW);
            tbNewKey.Size = new Size(217, 30);
            tbNewKey.Location = new Point(1, 51);
            tbNewKey.Font = new Font("Tahoma", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            tbNewKey.Multiline = true;
            tbNewKey.Name = "tbNewKey";
            tbNewKey.TabIndex = 0;
            tbOldKey.PasswordChar = ControlChars.NullChar;

            tbNewKeyCf = new CustomizeTextBox(Common.ComponentDescriptionText.UC_CHANGE_KEY_NEW_CF);
            tbNewKeyCf.Size = new Size(217, 30);
            tbNewKeyCf.Location = new Point(1, 101);
            tbNewKeyCf.Font = new Font("Tahoma", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            tbNewKeyCf.Multiline = true;
            tbNewKeyCf.Name = "tbNewKeyCf";
            tbNewKeyCf.TabIndex = 0;
            tbNewKeyCf.PasswordChar = ControlChars.NullChar;


            pnInputForm.Controls.Add(tbOldKey);
            pnInputForm.Controls.Add(tbNewKey);
            pnInputForm.Controls.Add(tbNewKeyCf);
            #endregion

            SetUser(user);
        }

        private void btnChangeKey_Click(object sender, EventArgs e)
        {
            this.ActiveControl = null; 

            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat"))
            {
                cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_REQUEST_RESTART_MESSAGE, AlertType.Info);
                cnb.Visible = true;
                return;
            }
            else
            {
                if (Common.Common.readRegistryIfInitialKeyIsSet() == false)
                {
                    cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_REQUEST_RESTART_MESSAGE, AlertType.Info);
                    cnb.Visible = true;
                }
                else
                {
                    string oldKey = tbOldKey.TextBoxValue();
                    string newKey = tbNewKey.TextBoxValue();
                    string newKeyCf = tbNewKeyCf.TextBoxValue();
                    if (string.IsNullOrEmpty(oldKey) || string.IsNullOrEmpty(newKey) || string.IsNullOrEmpty(newKeyCf))
                    {
                        cnb = new CustomNotificationBox(ComponentDescriptionText.FILL_THE_FORM_MESSAGE, AlertType.Info);
                        cnb.Visible = true;
                        return;
                    }
                    else
                    {
                        if (!newKey.Equals(newKeyCf))
                        {
                            cnb = new CustomNotificationBox(ComponentDescriptionText.NEW_PASSWORD_DOES_NOT_MATCH_MESSAGE, AlertType.Info);
                            cnb.Visible = true;
                            return;
                        }
                        else
                        {
                            try
                            {
                                StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat");
                                string line = sr.ReadLine();
                                if (line != null)
                                {
                                    if (line.Equals(Common.Common.CreateMD5(oldKey + "5154bfd9725040c54f5f69c70c61bdc3")))
                                    {try
                                        {
                                            string encryptedMasterPassword = Common.Common.CreateMD5(newKey + "5154bfd9725040c54f5f69c70c61bdc3");
                                            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team"))
                                            {
                                                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team");
                                            }
                                            sr.Close();
                                            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat", encryptedMasterPassword);
                                            Common.Common.writeRegistrySetInitialKeyForTheFirstTime();

                                            // Loading Box
                                            PreLoaderDialog pld = new PreLoaderDialog();
                                            pld.ShowDialog();

                                            // Clear form
                                            this.clearForm();

                                            cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_CHANGE_MESSAGE, AlertType.Info);
                                            cnb.Visible = true;
                                        } catch(Exception ex)
                                        {
                                            Console.WriteLine(ex);
                                            cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_CHANGE_FAILED_MEMSSAGE, AlertType.Info);
                                            cnb.Visible = true;
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        cnb = new CustomNotificationBox(ComponentDescriptionText.OLD_PASSWORD_WRONG_MESSAGE, AlertType.Info);
                                        cnb.Visible = true;
                                        return;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }
                    }
                }
            }
        }

        public void SetUser(userDetail user)
        {
            this.user = user;
            updateFormByUserType();
        }

        private void updateFormByUserType()
        {
            switch (user.UserType.ToString())
            {
                // Guest
                case "0":
                    {
                        masterPasswordControl(0);
                        break;
                    }
                // Normal
                case "1":
                    {
                        masterPasswordControl(1);
                        break;
                    }
                // VIP
                case "2":
                    {
                        masterPasswordControl(1);
                        break;
                    }
            }
        }

        private void masterPasswordControl(int controlCode)
        {
            switch (controlCode.ToString())
            {
                // Disable
                case "0":
                    {
                        pnLoginNote.Visible = true;
                        foreach (Control ctrl in pnInputForm.Controls)
                        {
                            ctrl.Enabled = false;
                        }
                        btnChangeKey.Enabled = false;
                        break;
                    }
                // Enable
                case "1":
                    {
                        pnLoginNote.Visible = false;
                        foreach (Control ctrl in pnInputForm.Controls)
                        {
                            ctrl.Enabled = true;
                        }
                        btnChangeKey.Enabled = true;
                        break;
                    }
            }
        }

        public void SetMainForm(mainForm parent)
        {
            this.parent = parent;
        }

        private void lbLoginLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            parent.Logout();
        }
        
        private void clearForm()
        {
            tbOldKey.Text = String.Empty;
            tbNewKey.Text = String.Empty;
            tbNewKeyCf.Text = String.Empty;

            // Return place holder
            tbOldKey.Focus();
            tbNewKey.Focus();
            tbNewKeyCf.Focus();
            this.ActiveControl = null;
        }
    }
}
