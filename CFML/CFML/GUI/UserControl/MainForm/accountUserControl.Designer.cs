﻿namespace CFML.GUIDesign
{
    partial class accountUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbAccountName = new System.Windows.Forms.Label();
            this.lbAccountEmail = new System.Windows.Forms.Label();
            this.lbVIPStatus = new System.Windows.Forms.Label();
            this.lbExp = new System.Windows.Forms.Label();
            this.lbEditProfileLink = new System.Windows.Forms.LinkLabel();
            this.lbChangePasswordLink = new System.Windows.Forms.LinkLabel();
            this.pnUCTitle = new System.Windows.Forms.Panel();
            this.lbUCTitle = new System.Windows.Forms.Label();
            this.pbUCPic = new System.Windows.Forms.PictureBox();
            this.pnUserInfo = new System.Windows.Forms.Panel();
            this.lbUpgradeToVIPLink = new System.Windows.Forms.LinkLabel();
            this.pnGuestWelcomeNote = new System.Windows.Forms.Panel();
            this.lbForgetPasswordLink = new System.Windows.Forms.LinkLabel();
            this.lbRegisterLink = new System.Windows.Forms.LinkLabel();
            this.lbGuestWelcomeContent = new System.Windows.Forms.Label();
            this.lbLoginLink = new System.Windows.Forms.LinkLabel();
            this.lbWelcomeGuest = new System.Windows.Forms.Label();
            this.pnUCTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).BeginInit();
            this.pnUserInfo.SuspendLayout();
            this.pnGuestWelcomeNote.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbAccountName
            // 
            this.lbAccountName.AutoSize = true;
            this.lbAccountName.Font = new System.Drawing.Font("Lato Heavy", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAccountName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbAccountName.Location = new System.Drawing.Point(3, 0);
            this.lbAccountName.Name = "lbAccountName";
            this.lbAccountName.Size = new System.Drawing.Size(35, 16);
            this.lbAccountName.TabIndex = 8;
            this.lbAccountName.Text = "ABC";
            // 
            // lbAccountEmail
            // 
            this.lbAccountEmail.AutoSize = true;
            this.lbAccountEmail.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAccountEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbAccountEmail.Location = new System.Drawing.Point(3, 35);
            this.lbAccountEmail.Name = "lbAccountEmail";
            this.lbAccountEmail.Size = new System.Drawing.Size(164, 16);
            this.lbAccountEmail.TabIndex = 9;
            this.lbAccountEmail.Text = "Email: truongnx@gmail.com";
            // 
            // lbVIPStatus
            // 
            this.lbVIPStatus.AutoSize = true;
            this.lbVIPStatus.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVIPStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbVIPStatus.Location = new System.Drawing.Point(3, 73);
            this.lbVIPStatus.Name = "lbVIPStatus";
            this.lbVIPStatus.Size = new System.Drawing.Size(121, 16);
            this.lbVIPStatus.TabIndex = 10;
            this.lbVIPStatus.Text = "Status: VIP Account";
            // 
            // lbExp
            // 
            this.lbExp.AutoSize = true;
            this.lbExp.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbExp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbExp.Location = new System.Drawing.Point(3, 108);
            this.lbExp.Name = "lbExp";
            this.lbExp.Size = new System.Drawing.Size(163, 16);
            this.lbExp.TabIndex = 11;
            this.lbExp.Text = "Expired Date: 03 Apr 2018";
            // 
            // lbEditProfileLink
            // 
            this.lbEditProfileLink.AutoSize = true;
            this.lbEditProfileLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEditProfileLink.Location = new System.Drawing.Point(3, 159);
            this.lbEditProfileLink.Name = "lbEditProfileLink";
            this.lbEditProfileLink.Size = new System.Drawing.Size(71, 16);
            this.lbEditProfileLink.TabIndex = 12;
            this.lbEditProfileLink.TabStop = true;
            this.lbEditProfileLink.Text = "Edit Profile";
            this.lbEditProfileLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbEditProfileLink_LinkClicked);
            // 
            // lbChangePasswordLink
            // 
            this.lbChangePasswordLink.AutoSize = true;
            this.lbChangePasswordLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChangePasswordLink.Location = new System.Drawing.Point(370, 159);
            this.lbChangePasswordLink.Name = "lbChangePasswordLink";
            this.lbChangePasswordLink.Size = new System.Drawing.Size(109, 16);
            this.lbChangePasswordLink.TabIndex = 13;
            this.lbChangePasswordLink.TabStop = true;
            this.lbChangePasswordLink.Text = "Change Password";
            this.lbChangePasswordLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbChangePasswordLink_LinkClicked);
            // 
            // pnUCTitle
            // 
            this.pnUCTitle.Controls.Add(this.lbUCTitle);
            this.pnUCTitle.Controls.Add(this.pbUCPic);
            this.pnUCTitle.Location = new System.Drawing.Point(50, 20);
            this.pnUCTitle.Name = "pnUCTitle";
            this.pnUCTitle.Size = new System.Drawing.Size(472, 54);
            this.pnUCTitle.TabIndex = 14;
            // 
            // lbUCTitle
            // 
            this.lbUCTitle.AutoSize = true;
            this.lbUCTitle.Font = new System.Drawing.Font("Lato Heavy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUCTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUCTitle.Location = new System.Drawing.Point(57, 16);
            this.lbUCTitle.Name = "lbUCTitle";
            this.lbUCTitle.Size = new System.Drawing.Size(69, 19);
            this.lbUCTitle.TabIndex = 1;
            this.lbUCTitle.Text = "Account";
            // 
            // pbUCPic
            // 
            this.pbUCPic.BackgroundImage = global::CFML.Properties.Resources.AccountIconColored;
            this.pbUCPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbUCPic.Location = new System.Drawing.Point(4, 4);
            this.pbUCPic.Name = "pbUCPic";
            this.pbUCPic.Size = new System.Drawing.Size(47, 47);
            this.pbUCPic.TabIndex = 0;
            this.pbUCPic.TabStop = false;
            // 
            // pnUserInfo
            // 
            this.pnUserInfo.Controls.Add(this.lbUpgradeToVIPLink);
            this.pnUserInfo.Controls.Add(this.lbExp);
            this.pnUserInfo.Controls.Add(this.lbVIPStatus);
            this.pnUserInfo.Controls.Add(this.lbAccountEmail);
            this.pnUserInfo.Controls.Add(this.lbChangePasswordLink);
            this.pnUserInfo.Controls.Add(this.lbAccountName);
            this.pnUserInfo.Controls.Add(this.lbEditProfileLink);
            this.pnUserInfo.Location = new System.Drawing.Point(50, 113);
            this.pnUserInfo.Name = "pnUserInfo";
            this.pnUserInfo.Size = new System.Drawing.Size(511, 188);
            this.pnUserInfo.TabIndex = 15;
            // 
            // lbUpgradeToVIPLink
            // 
            this.lbUpgradeToVIPLink.AutoSize = true;
            this.lbUpgradeToVIPLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpgradeToVIPLink.Location = new System.Drawing.Point(178, 159);
            this.lbUpgradeToVIPLink.Name = "lbUpgradeToVIPLink";
            this.lbUpgradeToVIPLink.Size = new System.Drawing.Size(96, 16);
            this.lbUpgradeToVIPLink.TabIndex = 14;
            this.lbUpgradeToVIPLink.TabStop = true;
            this.lbUpgradeToVIPLink.Text = "Upgrade to VIP";
            this.lbUpgradeToVIPLink.Visible = false;
            this.lbUpgradeToVIPLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbUpgradeToVIPLink_LinkClicked);
            // 
            // pnGuestWelcomeNote
            // 
            this.pnGuestWelcomeNote.Controls.Add(this.lbForgetPasswordLink);
            this.pnGuestWelcomeNote.Controls.Add(this.lbRegisterLink);
            this.pnGuestWelcomeNote.Controls.Add(this.lbGuestWelcomeContent);
            this.pnGuestWelcomeNote.Controls.Add(this.lbLoginLink);
            this.pnGuestWelcomeNote.Controls.Add(this.lbWelcomeGuest);
            this.pnGuestWelcomeNote.Location = new System.Drawing.Point(50, 334);
            this.pnGuestWelcomeNote.Name = "pnGuestWelcomeNote";
            this.pnGuestWelcomeNote.Size = new System.Drawing.Size(511, 164);
            this.pnGuestWelcomeNote.TabIndex = 16;
            this.pnGuestWelcomeNote.Visible = false;
            // 
            // lbForgetPasswordLink
            // 
            this.lbForgetPasswordLink.AutoSize = true;
            this.lbForgetPasswordLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbForgetPasswordLink.Location = new System.Drawing.Point(376, 134);
            this.lbForgetPasswordLink.Name = "lbForgetPasswordLink";
            this.lbForgetPasswordLink.Size = new System.Drawing.Size(103, 16);
            this.lbForgetPasswordLink.TabIndex = 16;
            this.lbForgetPasswordLink.TabStop = true;
            this.lbForgetPasswordLink.Text = "Forget Password";
            this.lbForgetPasswordLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbForgetPasswordLink_LinkClicked);
            // 
            // lbRegisterLink
            // 
            this.lbRegisterLink.AutoSize = true;
            this.lbRegisterLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRegisterLink.Location = new System.Drawing.Point(188, 134);
            this.lbRegisterLink.Name = "lbRegisterLink";
            this.lbRegisterLink.Size = new System.Drawing.Size(56, 16);
            this.lbRegisterLink.TabIndex = 15;
            this.lbRegisterLink.TabStop = true;
            this.lbRegisterLink.Text = "Register";
            this.lbRegisterLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbRegisterLink_LinkClicked);
            // 
            // lbGuestWelcomeContent
            // 
            this.lbGuestWelcomeContent.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGuestWelcomeContent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbGuestWelcomeContent.Location = new System.Drawing.Point(3, 42);
            this.lbGuestWelcomeContent.Name = "lbGuestWelcomeContent";
            this.lbGuestWelcomeContent.Size = new System.Drawing.Size(477, 65);
            this.lbGuestWelcomeContent.TabIndex = 9;
            this.lbGuestWelcomeContent.Text = "Log in as a user help you to manage account and explire more amazing functions of" +
    " this application";
            // 
            // lbLoginLink
            // 
            this.lbLoginLink.AutoSize = true;
            this.lbLoginLink.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginLink.Location = new System.Drawing.Point(3, 134);
            this.lbLoginLink.Name = "lbLoginLink";
            this.lbLoginLink.Size = new System.Drawing.Size(42, 16);
            this.lbLoginLink.TabIndex = 14;
            this.lbLoginLink.TabStop = true;
            this.lbLoginLink.Text = "Log in";
            this.lbLoginLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbLoginLink_LinkClicked);
            // 
            // lbWelcomeGuest
            // 
            this.lbWelcomeGuest.AutoSize = true;
            this.lbWelcomeGuest.Font = new System.Drawing.Font("Lato Heavy", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWelcomeGuest.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbWelcomeGuest.Location = new System.Drawing.Point(3, 0);
            this.lbWelcomeGuest.Name = "lbWelcomeGuest";
            this.lbWelcomeGuest.Size = new System.Drawing.Size(100, 16);
            this.lbWelcomeGuest.TabIndex = 8;
            this.lbWelcomeGuest.Text = "Welcome Guest";
            // 
            // accountUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnUserInfo);
            this.Controls.Add(this.pnUCTitle);
            this.Controls.Add(this.pnGuestWelcomeNote);
            this.Name = "accountUserControl";
            this.Size = new System.Drawing.Size(612, 692);
            this.pnUCTitle.ResumeLayout(false);
            this.pnUCTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUCPic)).EndInit();
            this.pnUserInfo.ResumeLayout(false);
            this.pnUserInfo.PerformLayout();
            this.pnGuestWelcomeNote.ResumeLayout(false);
            this.pnGuestWelcomeNote.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbAccountName;
        private System.Windows.Forms.Label lbAccountEmail;
        private System.Windows.Forms.Label lbVIPStatus;
        private System.Windows.Forms.Label lbExp;
        private System.Windows.Forms.LinkLabel lbEditProfileLink;
        private System.Windows.Forms.LinkLabel lbChangePasswordLink;
        private System.Windows.Forms.Panel pnUCTitle;
        private System.Windows.Forms.Label lbUCTitle;
        private System.Windows.Forms.PictureBox pbUCPic;
        private System.Windows.Forms.Panel pnUserInfo;
        private System.Windows.Forms.Panel pnGuestWelcomeNote;
        private System.Windows.Forms.Label lbGuestWelcomeContent;
        private System.Windows.Forms.Label lbWelcomeGuest;
        private System.Windows.Forms.LinkLabel lbRegisterLink;
        private System.Windows.Forms.LinkLabel lbLoginLink;
        private System.Windows.Forms.LinkLabel lbForgetPasswordLink;
        private System.Windows.Forms.LinkLabel lbUpgradeToVIPLink;
    }
}
