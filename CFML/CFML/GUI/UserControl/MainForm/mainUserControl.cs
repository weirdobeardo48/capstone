﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.GUIDesign.CustomizeComponent;
using CFML.Common;
using CFML.Capture;
using System.Threading;
using TensorFlow;
using CMFL.CensorBox;
using Emgu.CV;
using System.Diagnostics;
using System.IO;
using Emgu.CV.Structure;
using CFML.PornDetector;
using CFML.CensorBox;
using Emgu.CV.Util;
using System.Net;

namespace CFML.GUIDesign
{
    public partial class mainUserControl : UserControl
    {
        #region Variable
        private static TFTensor[] tensor;
        private static Common.ConfigurationHandle config;
        List<WindowSnap> inactiveAppSnaps = new List<WindowSnap>();
        Thread pornDetectThreadRealTime;
        Thread pornDetectThread;
        Thread pornDetectInactiveAppThread;
        Thread uploadToFTPServerThread;
        Thread muteSoundThread;
        byte[] byteArrayOfSnapForRealTime;
        byte[] byteArrayOfSnapForInactiveThread;
        byte[] byteArrayOfSnapForThread;
        bool matchTemplate;
        Bitmap capturedBitmap;
        Bitmap capturedBitmapForScanIntervalApp;
        List<boxDetails> listBoxesForRealTimeApp;
        List<boxDetails> listBoxesForInactiveApp;
        List<boxDetails> listBoxesForScanIntervalApp;
        List<boxDetails> listBoxesToShow = new List<boxDetails>();
        censorBox cen = new censorBox();
        int count_match = 0;
        Image<Bgr, Byte> tempFrame;
        Rectangle tempRectangle = new Rectangle();
        List<Image<Bgr, Byte>> listTemplate = new List<Image<Bgr, byte>>();
        private ObjectTracking track = new ObjectTracking();
        private boxDetailsConvertToRect boxConvert = new boxDetailsConvertToRect();
        private static FormClose close = new FormClose();
        private userDetail user;
        private static int pId = 0;
        //static Image<Bgr, byte> tempImg;

        private mainForm parent;
        #endregion

        public mainUserControl()
        {
            config = new ConfigurationHandle();
            if (user == null)
            {
                config.readConfigurationFromFile();
            }
            else
            {
                config.readConfigurationFromFileByUserType(user.UserType);
            }
            InitializeComponent();
            Initiate();
            pornDetector.initGraph();
            Common.Common.InitCensorForm();
        }


        private void Initiate()
        {
            #region Button Children Mode
            //Init val
            this.cbChildrenMode.Checked = true;
            lbChildrenModeDes.Text = Common.ComponentDescriptionText.UC_MAIN_CHILDREN_MODE_DESCRIPTION;

            //Recolor
            cbChildrenMode_CheckedChanged();
            #endregion
        }

        private void cbChildrenMode_CheckedChanged()
        {
            if (this.cbChildrenMode.Checked)
            {
                pbChildrenMode.BackgroundImage = Properties.Resources.ToggleOff;
                this.cbChildrenMode.BackColor = Color.FromArgb(240, 240, 240);
                lbChildrenModeStatus.Text = "Children mode is off";
                cen.Hide();
                lbChildrenModeStatus.ForeColor = Color.FromArgb(40, 40, 40);
                lbChildrenModeDes.Text = Common.ComponentDescriptionText.UC_MAIN_PARENT_MODE_DESCRIPTION;
                if (parent != null)
                {
                    parent.updateAIRunningStatusToUserControl(false);
                }

            }
            else
            {
                pbChildrenMode.BackgroundImage = Properties.Resources.ToggleOn;
                this.cbChildrenMode.BackColor = Color.FromArgb(31, 93, 58);
                lbChildrenModeStatus.Text = "Children mode is on";
                lbChildrenModeStatus.ForeColor = Color.FromArgb(60, 201, 116);
                lbChildrenModeDes.Text = Common.ComponentDescriptionText.UC_MAIN_CHILDREN_MODE_DESCRIPTION;
                if (parent != null)
                {
                    parent.updateAIRunningStatusToUserControl(true);
                }
            }
        }

        public void SetUser(userDetail user)
        {
            this.user = user;

            config.readConfigurationFromFileByUserType(user.UserType);
            //Re-Init censor form
            pornDetector.initGraph();
            Common.Common.InitCensorForm(user.UserType);
        }

        public void SetMainForm(mainForm parent)
        {
            this.parent = parent;
        }

        private void cbChildrenMode_OnChange(object sender, EventArgs e)
        {
            if (user == null)
            {
                config.readConfigurationFromFileByUserType(0);
            }
            else
            {
                config.readConfigurationFromFileByUserType(user.UserType);
            }
            if (cbChildrenMode.Checked)
            {
                cbChildrenMode_CheckedChanged();
                if (pornDetectThreadRealTime != null)
                {
                    if (pornDetectThreadRealTime.IsAlive)
                    {
                        pornDetectThreadRealTime.Abort();
                        Console.WriteLine("Porn detector is now stopped");
                    }
                }
                
                if ( pornDetectInactiveAppThread != null)
                {
                    if (pornDetectInactiveAppThread.IsAlive)
                    {
                        pornDetectInactiveAppThread.Abort();
                        Console.WriteLine("Porn detector for inactive app is now stopped");
                    }
                }
                if (pornDetectThread != null)
                {
                    if (pornDetectThread.IsAlive)
                    {
                        pornDetectThread.Abort();
                        Console.WriteLine("Porn detector Scan is now stopped");
                    }
                }

                if (uploadToFTPServerThread != null)
                {
                    if (uploadToFTPServerThread.IsAlive)
                    {
                        uploadToFTPServerThread.Abort();
                        Console.WriteLine("Upload to FPT Server is now stopped");
                    }
                }
                return;
            }
            else
            {
                cbChildrenMode_CheckedChanged();


                // check privacy status and do it job
                if (ConfigurationHandle.Config.PrivacyCheck)
                {
                    if (uploadToFTPServerThread != null)
                    {
                        if (!uploadToFTPServerThread.IsAlive)
                        {
                            uploadToFTPServerThread = new Thread(() => uploadToFTPServer());
                            uploadToFTPServerThread.Start();
                            if (uploadToFTPServerThread.IsAlive)
                            {
                                Console.WriteLine("Doing uploading");
                            }
                            else
                            {
                                Console.WriteLine("There was some problem uploading to FTP Server");
                            }
                        }
                    }
                    else
                    {
                        uploadToFTPServerThread = new Thread(() => uploadToFTPServer());
                        uploadToFTPServerThread.Start();
                        if (uploadToFTPServerThread.IsAlive)
                        {
                            Console.WriteLine("Doing uploading");
                        }
                        else
                        {
                            Console.WriteLine("There was some problem uploading to FTP Server");
                        }
                    }
                }

                //pbChildrenMode.BackgroundImage = CFML.Properties.Resources.ToggleOff;
                if (ConfigurationHandle.Config.EnableRealTimeMode)
                {
                    if (pornDetectThreadRealTime != null)
                    {
                        if (!pornDetectThreadRealTime.IsAlive)
                        {
                            pornDetectThreadRealTime = new Thread(() => doDetector());
                            pornDetectThreadRealTime.Start();
                            if (pornDetectThreadRealTime.IsAlive)
                            {
                                Console.WriteLine("Porn detector is now running");
                            }
                            else
                            {
                                Console.WriteLine("There was some problem running porn detector");
                            }
                        }
                    }
                    else
                    {
                        pornDetectThreadRealTime = new Thread(() => doDetector());
                        pornDetectThreadRealTime.Start();
                        if (pornDetectThreadRealTime.IsAlive)
                        {
                            Console.WriteLine("Porn detector is now running");
                        }
                        else
                        {
                            Console.WriteLine("There was some problem running porn detector");
                        }
                    }
                }
                else
                {
                    pornDetectThread = new Thread(() => doDetectorScanInterval());
                    pornDetectThread.Start();
                    if (pornDetectThread.IsAlive)
                    {
                        Console.WriteLine("Porn Detector Scan Interval is now running");
                    }
                    else
                    {
                        Console.WriteLine("There was some problem running porn detector");
                    }
                }

                if (ConfigurationHandle.Config.CheckInactiveApplication)
                {
                    if (pornDetectInactiveAppThread != null)
                    {
                        if (!pornDetectInactiveAppThread.IsAlive)
                        {
                            pornDetectInactiveAppThread = new Thread(() => doDetectInactiveApp());
                            pornDetectInactiveAppThread.Start();
                            if (pornDetectInactiveAppThread.IsAlive)
                            {
                                Console.WriteLine("Porn Detect For Inactive App is now running");
                            }
                            else
                            {
                                Console.WriteLine("There was some problem running porn detector");
                            }
                        }
                    }
                    else
                    {
                        pornDetectInactiveAppThread = new Thread(() => doDetectInactiveApp());
                        pornDetectInactiveAppThread.Start();
                        if (pornDetectInactiveAppThread.IsAlive)
                        {
                            Console.WriteLine("Porn Detect For Inactive App is now running");
                        }
                        else
                        {
                            Console.WriteLine("There was some problem running porn detector");
                        }
                    }
                }

            }
        }

        //transfer from img file to byte array --> fetch to TFSensor
        //public static byte[] ImageToByte2(Image img)
        //{
        //    using (var stream = new MemoryStream())
        //    {
        //        img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        return stream.ToArray();
        //    }
        //}

        public static void saveImageToJpeg(Image img, string filePath)
        {
            try
            {
                using (var stream = new MemoryStream())
                {
                    img.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);
                }
            }
            catch
            {

            }


        }

        private void uploadToFTPServer()
        {
            while (true)
            {
                List<WindowSnap> listSnap = WindowSnap.GetListWindowSnap();
                List<string> filePathList = new List<string>();
                Bitmap activeImageToUpload = WindowSnap.GetActiveWindowImage();
                string fileName = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");
                if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\" + "savedImage"))
                {
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\" + "savedImage");
                }
                saveImageToJpeg(activeImageToUpload, Directory.GetCurrentDirectory() + "\\" + "savedImage\\" + fileName + "-active.jpg");
                filePathList.Add(Directory.GetCurrentDirectory() + "\\" + "savedImage\\" + fileName + "-active.jpg");
                for (int i = 0; i < listSnap.Count; i++)
                {
                    saveImageToJpeg(listSnap[i].Image, Directory.GetCurrentDirectory() + "\\" + "savedImage\\" + fileName + "-inactive-" + i + ".jpg");
                    filePathList.Add(Directory.GetCurrentDirectory() + "\\" + "savedImage\\" + fileName + "-inactive-" + i + ".jpg");
                }

                // Upload file to ftp server
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(Common.uploadToFTPServer.ftpUser, Common.uploadToFTPServer.ftpPassword);
                    foreach (var localFilePath in filePathList)
                    {
                        try
                        {
                            string path = localFilePath.Replace('\\', '/');
                            fileName = path.Split('/')[path.Split('/').Count() - 1];
                            client.UploadFile(Common.uploadToFTPServer.fptURL + "/" + fileName, "STOR", path);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }


                //Delete file after upload
                foreach (var localFilePath in filePathList)
                {
                    if (File.Exists(localFilePath))
                    {
                        try
                        {
                            File.Delete(localFilePath);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }
                Thread.Sleep(3600000);
            }
        }

        static Stopwatch w = new Stopwatch();
        public static int getSuggestSize(Bitmap image)
        {
            Mat imageMat = new Image<Bgr, byte>(image).Mat.Clone();
            //Mat finalMat = new Mat(imageMat.Rows, imageMat.Cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            //Mat tmp = new Mat(imageMat.Rows, imageMat.Cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            //Mat alpha = new Mat(imageMat.Rows, imageMat.Cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            CvInvoke.CvtColor(imageMat, imageMat, Emgu.CV.CvEnum.ColorConversion.Bgr2Hsv);
            //CvInvoke.Threshold(tmp, alpha, 100, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
            CvInvoke.InRange(imageMat, new ScalarArray(new MCvScalar(0, 10, 60)), new ScalarArray(new MCvScalar(20, 150, 255)), imageMat);
            Image<Gray, Byte> tmpImage = imageMat.ToImage<Gray, Byte>();
            int countWhitePixel = CvInvoke.CountNonZero(tmpImage);
            //for (int j = 0; j < image.Width; j++)
            //{
            //    for (int i = 0; i < image.Height; i++)
            //    {
            //        if (tmpImage.Data[i, j, 0] != 0 && tmpImage.Data[i, j, 1] != 0 && tmpImage.Data[i, j, 2] != 0)
            //        {
            //            //image.Data[i, j, 0] = 255;
            //            //image.Data[i, j, 1] = 255;
            //            //image.Data[i, j, 2] = 255;
            //            countWhitePixel++;
            //        }

            //    }
            //}
            if ((double)countWhitePixel / (image.Width * image.Height) < 0.1)
            {
                return -1;
            }
            if ((double)countWhitePixel / (image.Width * image.Height) < 0.2)
            {
               if(image.Width * image.Height < 800)
                {
                    return 8;
                } else
                {
                    return 3;
                }
            }
            else if ((double)countWhitePixel / (image.Width * image.Height) < 0.5 && (double)countWhitePixel / (image.Width * image.Height) > 0.2)
            {
                if (image.Width * image.Height < 800)
                {
                    return 4;
                }
                else
                {
                    return 3;
                }
            }
            else if ((double)countWhitePixel / (image.Width * image.Height) > 0.5 && (double)countWhitePixel / (image.Width * image.Height) < 0.8)
            {
                if (image.Width * image.Height < 800)
                {
                    return 3;
                }
                else
                {
                    return 2;
                }
            }
            else
            {
                return 2;
            }
            //VectorOfMat rgb = new VectorOfMat(3);

            //CvInvoke.Split(imageMat, rgb);
            //Mat[] rgba = { rgb[0], rgb[1], rgb[2], alpha };

            //VectorOfMat vector = new VectorOfMat(rgba);

            //CvInvoke.Merge(vector, finalMat);

        }


        static int scaleSize = -1;
        public static byte[] ImageToByte2(Bitmap img)
        {
            Image<Bgr, byte> outputImg;
            //tempImg = new Image<Bgr, byte>(img);
            scaleSize = getSuggestSize(img);
            if (scaleSize == -1)
            {
                return null;
            }
            //img = new Bitmap(img, img.Width * scaleSize, img.Height * scaleSize);

            outputImg = new Image<Bgr, byte>(img);
            //outputImg._EqualizeHist();
            outputImg._GammaCorrect(0.7);

            //outputImg = extractSkinColor(outputImg);
            //saveImageToJpeg(outputImg.Bitmap, "test.png");
            ImageConverter converter = new ImageConverter();
            byte[] returnByteArray = (byte[])converter.ConvertTo(outputImg.ToBitmap(), typeof(byte[]));
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return returnByteArray;

        }

        private static boxDetails getBoxPosition(boxDetails detectBox, boxDetails appBox)
        {
            boxDetails realBox = new boxDetails();

            realBox.Xmin = (((appBox.Xmax - appBox.Xmin) * detectBox.Xmin) + appBox.Xmin) / Screen.PrimaryScreen.Bounds.Width;
            realBox.Xmax = (((appBox.Xmax - appBox.Xmin) * detectBox.Xmax) + appBox.Xmin) / Screen.PrimaryScreen.Bounds.Width;
            realBox.Ymax = (((appBox.Ymax - appBox.Ymin) * detectBox.Ymax) + appBox.Ymin) / Screen.PrimaryScreen.Bounds.Height;
            realBox.Ymin = (((appBox.Ymax - appBox.Ymin) * detectBox.Ymin) + appBox.Ymin) / Screen.PrimaryScreen.Bounds.Height;
            return realBox;
        }


        //Scan inactive app
        private void doDetectInactiveApp()
        {
            if (user == null)
            {
                config.readConfigurationFromFileByUserType(0);
            }
            else
            {
                config.readConfigurationFromFileByUserType(user.UserType);
            }
            while (true)
            {
                var currentWindow = WindowSnap.GetForegroundWindow();
                try
                {
                    Console.WriteLine("--------------------Doing Detector For Inactive App--------------------");
                    inactiveAppSnaps = WindowSnap.GetListWindowSnap();
                    currentWindow = WindowSnap.GetForegroundWindow();
                    foreach (var snap in inactiveAppSnaps)
                    {
                        try
                        {
                            if (listBoxesForInactiveApp != null)
                            {
                                listBoxesForInactiveApp.Clear();
                            }
                            else
                            {
                                listBoxesForInactiveApp = new List<boxDetails>();
                            }
                            byteArrayOfSnapForInactiveThread = ImageToByte2(snap.Image);
                            tensor = pornDetector.detectPorn(byteArrayOfSnapForInactiveThread, pornDetector.Session);
                            listBoxesForInactiveApp = pornDetector.visualizeResult(tensor, ConfigurationHandle.Config.CensorSensitivity * 1.0 / 100);
                            if (listBoxesForInactiveApp.Count != 0)
                            {
                                WindowSnap.MinimizeWindow(snap);
                            }
                            Thread.Sleep(20);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                catch (Exception e)
                {
                    currentWindow = WindowSnap.GetForegroundWindow();
                    Console.WriteLine(e);
                }
                Thread.Sleep(5000);
                WindowSnap.SetForegroundWindow(currentWindow);
            }
        }

        //Scan interval detect and minimize windows that contain porn. 
        private void doDetectorScanInterval()
        {
            if (user == null)
            {
                config.readConfigurationFromFile();
            }
            else
            {
                config.readConfigurationFromFileByUserType(user.UserType);
            }
            while (true)
            {
                Console.WriteLine("-------------------Doing Detector Scan Interval-----------------");
                if (listBoxesForScanIntervalApp != null)
                {
                    listBoxesForScanIntervalApp.Clear();
                }
                else
                {
                    listBoxesForScanIntervalApp = new List<boxDetails>();
                }
                try
                {
                    capturedBitmapForScanIntervalApp = WindowSnap.GetActiveWindowImage();
                    byteArrayOfSnapForThread = ImageToByte2(capturedBitmapForScanIntervalApp);
                    tensor = pornDetector.detectPorn(byteArrayOfSnapForThread, pornDetector.Session);
                    listBoxesForScanIntervalApp = pornDetector.visualizeResult(tensor, ConfigurationHandle.Config.CensorSensitivity * 1.0 / 100);
                    if (listBoxesForScanIntervalApp.Count != 0)
                    {
                        WindowSnap.MinimizeWindow(WindowSnap.GetForegroundWindowPid() + "");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                try
                {
                    Thread.Sleep(ConfigurationHandle.Config.DisabledRealTimeScanInterval * 1000);
                }
                catch (Exception e) { Console.WriteLine(e); }
            }
        }

        //Real time detect 
        private void doDetector()
        {
            if (user == null)
            {
                config.readConfigurationFromFileByUserType(0);
            }
            else

            {
                config.readConfigurationFromFileByUserType(user.UserType);
            }
            matchTemplate = false;
            bool doDetect = true;
            while (true)
            {
                try
                {
                    capturedBitmap = WindowSnap.GetActiveWindowImage();
                    setpId(WindowSnap.GetForegroundWindowPid());
                    if (listBoxesForRealTimeApp != null)
                    {
                        listBoxesForRealTimeApp.Clear();
                    }
                    else
                    {
                        listBoxesForRealTimeApp = new List<boxDetails>();
                    }
                    listBoxesToShow.Clear();
                    //Console.WriteLine("Get PID active app after 2 second");
                    //Thread.Sleep(2000);
                    //boxDetails tempBox = Common.Common.getWindowBox(WindowSnap.GetForegroundWindowPid() + "");
                    boxDetails tempBox = Common.Common.getForegroundWindowBox();
                    if (capturedBitmap != null)
                    {
                        //saveImageToJpeg(capturedBitmap, "test.jpg");
                        if (doDetect)
                        {
                            if (capturedBitmap.Width < 400 || capturedBitmap.Height < 400)
                            {
                                capturedBitmap = new Bitmap(capturedBitmap, capturedBitmap.Width * 2, capturedBitmap.Height * 2);
                            }
                            else
                            {
                                if (capturedBitmap.Width < 800 || capturedBitmap.Height < 800)
                                {
                                    capturedBitmap = new Bitmap(capturedBitmap, capturedBitmap.Width * 2, capturedBitmap.Height * 2);
                                }
                                else
                                {
                                    capturedBitmap = new Bitmap(capturedBitmap, capturedBitmap.Width * 2, capturedBitmap.Height * 2);
                                }
                            }
                        }
                        byteArrayOfSnapForRealTime = ImageToByte2(capturedBitmap);
                        if (ConfigurationHandle.Config.EnableObjectTracking)
                        {
                            // Do match template
                            if (matchTemplate)
                            {
                                Console.WriteLine("Doing match template");
                                matchTemplate = false;
                                listBoxesForRealTimeApp.Clear();
                                foreach (var temp in listTemplate)
                                {
                                    var matchBox = track.matchTemplate(new Image<Bgr, byte>(capturedBitmap), temp);
                                    if (!(matchBox.X == 0 && matchBox.Y == 0 && matchBox.Width == 0 && matchBox.Height == 0))
                                    {
                                        boxDetails box = new boxDetails();
                                        box = boxConvert.RectToBoxDetails(matchBox, capturedBitmap);
                                        listBoxesForRealTimeApp.Add(box);
                                        matchTemplate = true;
                                        if (count_match > 3)
                                        {
                                            matchTemplate = false;
                                            doDetect = true;
                                        }
                                    }
                                }
                                foreach (var box in listBoxesForRealTimeApp)
                                {
                                    Console.WriteLine("Bounding box: " + box.Ymin + " " + box.Ymax + " " + box.Xmin + " " + box.Xmax);
                                    listBoxesToShow.Add(getBoxPosition(box, tempBox));
                                }
                                count_match++;
                            }
                            else
                            {
                                doDetect = true;
                            }
                        }
                        else
                        {
                            doDetect = true;
                        }

                        //Do detect by AI 
                        if (doDetect)
                        {
                            Console.WriteLine("Doing regconition");
                            listTemplate.Clear();
                            tensor = pornDetector.detectPorn(byteArrayOfSnapForRealTime, pornDetector.Session);
                            listBoxesForRealTimeApp = pornDetector.visualizeResult(tensor, ConfigurationHandle.Config.CensorSensitivity * 1.0 / 100);
                            for (int i = 0; i < tensor.Count(); i++)
                            {
                                tensor[i].Dispose();
                            }
                            foreach (var box in listBoxesForRealTimeApp)
                            {
                                matchTemplate = true;
                                tempRectangle = boxConvert.boxDetailsToRect(box, capturedBitmap);
                                tempFrame = new Image<Bgr, byte>(capturedBitmap);
                                tempFrame.Draw(tempRectangle, new Bgr(Color.Red), 3);
                                var trackingImage = tempFrame.Copy();
                                trackingImage.ROI = tempRectangle;
                                listTemplate.Add(trackingImage);
                                doDetect = false;

                            }

                            foreach (var box in listBoxesForRealTimeApp)
                            {
                                listBoxesToShow.Add(getBoxPosition(box, tempBox));
                            }
                            count_match = 0;
                        }

                        #region Decrease Volume and Show Box

                        if (listBoxesToShow.Count() != 0)
                        {
                            if (ConfigurationHandle.Config.EnableMuteSound)
                            {
                                if(muteSoundThread == null)
                                {
                                    muteSoundThread = new Thread(() => doMute());
                                    muteSoundThread.Start(); 
                                } else
                                {
                                    if (muteSoundThread.IsAlive)
                                    {
                                        muteSoundThread.Abort();
                                        muteSoundThread = new Thread(() => doMute());
                                        muteSoundThread.Start();
                                    }
                                }
                            }
                            if (listBoxesToShow.Count() > (ConfigurationHandle.Config.MaximumCensoredObject))
                            {
                                //boxDetails box = Common.Common.getWindowBox(pId.ToString());
                                boxDetails box = Common.Common.getForegroundWindowBox();
                                if (!Common.Common.getCheckFormClose())
                                {
                                    close.Hide();
                                    close.setPosition(box.Xmin, box.Ymin, box.Xmax, box.Ymax, pId);
                                    close.Show();
                                    Common.Common.setCheckFormClose();
                                }
                                cen.Hide();
                            }
                            else
                            {
                                censorBox.setList(listBoxesToShow);
                                cen.displayForm();
                                Application.DoEvents();
                            }
                            //Application.Run(censorBox);
                        }
                        else
                        {
                            cen.Hide();
                        }
                        #endregion
                    }
                    if (capturedBitmap != null)
                    {
                        capturedBitmap.Dispose();
                    }
                    //Thread.Sleep(150);

                }
                catch (Exception e)
                {
                    if (capturedBitmap != null)
                    {
                        capturedBitmap.Dispose();
                    }
                    if (listBoxesToShow.Count() != 0)
                    { }
                    else
                    { cen.Hide(); }
                    Thread.Sleep(150);
                    Console.WriteLine(e.Message);
                    matchTemplate = false;
                }
                finally
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
        }

        public void setpId(int id)
        {
            pId = id;
        }


        public void doMute()
        {
            try
            {
                VolumeMixer.SetVolumeToValue(0);
            }
            catch (Exception)
            {

            }
        }
        private void pbChildrenMode_Click(object sender, EventArgs e)
        {
            cbChildrenMode.Checked = !cbChildrenMode.Checked;
            cbChildrenMode_OnChange(sender, e);
        }

        public void setFormClose()
        {
            close = null;
        }

        public void requestActivateAI()
        {
            if (cbChildrenMode.Checked)
            {
                pbChildrenMode_Click(null, null);
            }
            //MessageBox.Show(cbChildrenMode.Checked.ToString());
        }

        public void requestDeactivateAI()
        {
            if (!cbChildrenMode.Checked)
            {
                pbChildrenMode_Click(null, null);
            }
            //MessageBox.Show(cbChildrenMode.Checked.ToString());
        }
    }
}
