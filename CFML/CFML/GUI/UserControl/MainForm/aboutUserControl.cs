﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.Common;
using System.Diagnostics;

namespace CFML.GUIDesign
{
    public partial class aboutUserControl : UserControl
    {
        public aboutUserControl()
        {
            InitializeComponent();
            Initiate();
        }

        private void Initiate()
        {
            lbDesContent.Text = Common.ComponentDescriptionText.UC_ABOUT_DESCRIPTON;
            lbVersion.Text = "Version " + ComponentDescriptionText.CURRENT_APP_VERSION;
        }

        private void lbSendFeedbackLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_FEEDBACK_EMAIL);
        }
    }
}
