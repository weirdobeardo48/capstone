﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.GUIDesign.CustomizeComponent;
using MechanikaDesign.WinForms.UI.ColorPicker;
using Emgu.CV;
using System.Drawing.Imaging;
using System.IO;
using CFML.Common;
using CFML.Properties;
using System.Reflection;
using System.Threading;
using System.Diagnostics;

namespace CFML.GUIDesign
{
    public partial class customizeUserControl : UserControl
    {
        #region Variables
        private ColorBox2D cb2d;
        private ColorSliderVertical csv;
        private bool lockUpdates;
        private HslColor colorHsl = HslColor.FromAhsl(0xff);
        private Color colorRgb = Color.Empty;

        private ExtendedPanel transpBtn;

        private static Configuration.Configuration config;
        private static ConfigurationHandle configHandle;

        private userDetail user;
        private mainForm parent;

        private bool isAIThreadRunning;
        #endregion

        public customizeUserControl()
        {
            InitializeComponent();
            Initiate();
        }

        private void Initiate()
        {
            #region Config
            config = new Configuration.Configuration();
            configHandle = new ConfigurationHandle(); 
            #endregion

            #region Color Picker
            cb2d = new ColorBox2D();
            csv = new ColorSliderVertical();
            lockUpdates = false;

            pnColorPanel2D.Controls.Add(cb2d);
            pnVerticalColorPicker.Controls.Add(csv);

            csv.ColorChanged += new ColorSliderVertical.ColorChangedEventHandler(this.colorSlider_ColorChanged);
            this.cb2d.ColorChanged += new ColorBox2D.ColorChangedEventHandler(this.colorBox2D_ColorChanged); 
            #endregion

            #region Color Picker Tooltip
            //Tooltip
            ToolTip currentColor = new ToolTip();
            ToolTip pickedColor = new ToolTip();

            pickedColor.IsBalloon = true;
            pickedColor.ShowAlways = true;

            currentColor.IsBalloon = true;
            currentColor.ShowAlways = true;

            currentColor.SetToolTip(pnColorPreview, "Selected Color from Color Picker");
            pickedColor.SetToolTip(pnOldBgColor, "Color that applied to be Censor Box Background"); 
            #endregion

            #region Select Image btn
            //Select Image Btn
            transpBtn = new ExtendedPanel();
            transpBtn.BackColor = ColorTranslator.FromHtml("#000000");
            transpBtn.Location = pnImagePreviewShow.Location;
            transpBtn.Name = "transpBtn";
            transpBtn.Opacity = 80;
            transpBtn.Size = pnImagePreviewShow.Size;
            transpBtn.TabIndex = 3;
            transpBtn.Text = "asdasd";
            pnImgPreview.Controls.Add(transpBtn);
            transpBtn.BringToFront();
            transpBtn.Visible = false;
            transpBtn.MouseLeave += new EventHandler(this.transpBtn_MouseLeave);
            transpBtn.Cursor = Cursors.Hand;

            //Label
            TransparentLabel tmplb = new TransparentLabel();
            tmplb.Text = "Change Image";
            tmplb.ForeColor = ColorTranslator.FromHtml("#FFFFFF");
            tmplb.Font = new Font("Tahoma", 12F, FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tmplb.Location = new Point(20, 60);
            tmplb.AutoSize = false;
            tmplb.Size = new Size(120, 30);
            transpBtn.Controls.Add(tmplb);
            transpBtn.MouseClick += new MouseEventHandler(this.transpBtn_MouseClick);
            tmplb.MouseClick += new MouseEventHandler(this.transpBtn_MouseClick);
            tmplb.Cursor = Cursors.Hand;
            #endregion

            updateConfigurationToUserControl();

            #region Disable Picture Box
            //foreach (Control ctrl in pnImgPreview.Controls)
            //{
            //    ctrl.Enabled = false;
            //}
            optCheckedChange(null, null);
            #endregion

            pnColorPreview.BackColor = ColorTranslator.FromHtml("#FFFFFF");
            inpColorHex.Text = ColorTranslator.ToHtml(pnColorPreview.BackColor);

            isAIThreadRunning = false;

        }

        #region Select Picture Button Event
        private void transpBtn_MouseClick(object sender, System.EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();

            // image filters  
            open.Filter = "Image Files(*.jpg; *.jpeg; *.png; *.bmp)|*.jpg; *.jpeg; *.png; *.bmp";

            if (open.ShowDialog() == DialogResult.OK)
            {
                using (Bitmap tmpBitmap = new Bitmap(open.FileName))
                {
                    pnImagePreviewShow.BackgroundImage = (Image)tmpBitmap.Clone();
                    tmpBitmap.Dispose();
                }    
            }
        }

        private void transpBtn_MouseLeave(object sender, System.EventArgs e)
        {
            transpBtn.Visible = false;
        } 
        #endregion

        #region Color Picker Event
        private void colorSlider_ColorChanged(object sender, ColorChangedEventArgs args)
        {
            if (!this.lockUpdates)
            {
                HslColor colorHSL = this.csv.ColorHSL;
                this.colorHsl = colorHSL;
                this.colorRgb = this.colorHsl.RgbValue;
                this.lockUpdates = true;
                this.cb2d.ColorHSL = this.colorHsl;
                this.lockUpdates = false;


                pnColorPreview.BackColor = this.colorRgb;
                inpColorHex.Text = ColorTranslator.ToHtml(this.colorRgb);
                UpdateColorFields();
            }
        }

        private void colorBox2D_ColorChanged(object sender, ColorChangedEventArgs args)
        {
            if (!this.lockUpdates)
            {
                HslColor colorHSL = this.cb2d.ColorHSL;
                this.colorHsl = colorHSL;
                this.colorRgb = this.colorHsl.RgbValue;
                this.lockUpdates = true;
                this.csv.ColorHSL = this.colorHsl;
                this.lockUpdates = false;


                pnColorPreview.BackColor = this.colorRgb;
                inpColorHex.Text = ColorTranslator.ToHtml(this.colorRgb);
                UpdateColorFields();
            }
        }

        private void UpdateColorFields()
        {
            this.lockUpdates = true;
            //Update Current Color
            this.lockUpdates = false;
        }
        #endregion

        #region Image Picker

        private void DeleteFile(string path)
        {
            if (!File.Exists(path))
            {
                return;
            }

            bool isDeleted = false;
            if (!isDeleted)
            {
                try
                {
                    File.Delete(path);
                    isDeleted = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                //Thread.Sleep(50);
            }
        }

        private void optCheckedChange(object sender, EventArgs e)
        {
            if (optNotUseImage.Checked)
            {
                foreach (Control ctrl in pnImgPreview.Controls)
                {
                    ctrl.Enabled = false;
                }
                pnImagePreviewShow.MouseHover -= new EventHandler(pnImagePreviewShow_Enter);
                return;
            }
            if (optUseImage.Checked)
            {
                foreach (Control ctrl in pnImgPreview.Controls)
                {
                    ctrl.Enabled = true;
                }
                pnImagePreviewShow.MouseHover += new EventHandler(pnImagePreviewShow_Enter);
                return;
            }
        }

        private void pnImagePreviewShow_Enter(object sender, EventArgs e)
        {
            if (!transpBtn.Visible)
            {
                transpBtn.Visible = true;
                transpBtn.BringToFront();
            }

        } 
        #endregion

        #region Config

        private void readConfigurationFromFile()
        {
            configHandle.readConfigurationFromFile();
            config = ConfigurationHandle.Config;
        }

        private void configurationToUI()
        {
            if (config.CensorBgImageEnable)
            {
                optNotUseImage.Checked = false;
                optUseImage.Checked = true;
            }
            else
            {
                optNotUseImage.Checked = true;
                optUseImage.Checked = false;
            }
            optCheckedChange(null, null);

            if (config.CensorBgImageName == null || config.CensorBgImageName == "")
            {
                //load default bg Image
                try
                {
                    pnImagePreviewShow.BackgroundImage = Resources.CensorBgDefault;
                }
                catch
                {
                    Console.WriteLine("Can not load Default Background Image");
                }
            }
            else
            {
                string dir = Directory.GetCurrentDirectory() + @"\img\";
                string imgName = config.CensorBgImageName;
                string imgDir = dir.Trim() + imgName.Trim();
                try
                {
                    Image img;
                    using (var bmpTemp = new Bitmap(imgDir))
                    {
                        img = (Image) new Bitmap(bmpTemp).Clone();
                    }

                    pnImagePreviewShow.BackgroundImage = (Image) img.Clone();
 
                    img.Dispose();
                }
                catch
                {
                    Console.WriteLine("Can not load image: " + dir.Trim() + imgName.Trim());
                    try
                    {
                        pnImagePreviewShow.BackgroundImage = Resources.CensorBgDefault;
                    }
                    catch
                    {
                        Console.WriteLine("Can not load Default Background Image");
                    }
                }
            }

            if (config.CensorBgImageName == null || config.CensorBgImageName == "")
            {
                //Default Color is Black
                pnOldBgColor.BackColor = ColorTranslator.FromHtml("#000000");
            }
            else
            {
                string colorHex = config.CensorBgColor;
                try
                {
                    Color color = ColorTranslator.FromHtml(colorHex);
                    pnOldBgColor.BackColor = color;
                }
                catch
                {
                    Console.WriteLine("Wrong color config or can not use this color: " + colorHex);
                    pnOldBgColor.BackColor = ColorTranslator.FromHtml("#000000");
                }
            }
        }

        private void updateComponentValue()
        {
            try
            {
                configurationToUI();
            }
            catch
            {
                Console.WriteLine("First Attempt: Wrong Configuration Value!");
                configHandle.setDefaultConfiguration();
                config = ConfigurationHandle.Config;
                updateComponentValueByDefault();
            }

        }

        private void updateComponentValueByDefault()
        {
            try
            {
                configurationToUI();
            }
            catch
            {
                Console.WriteLine("Second Attempt: Failed to Config by Default!");
            }

        }

        public void updateConfigurationToUserControl()
        {
            readConfigurationFromFile();
            updateComponentValue();
        }

        private void getNewConfigurationStage()
        {
            Color bgColor = pnOldBgColor.BackColor;
            if (bgColor == null)
            {
                //default color if bg color not available
                bgColor = ColorTranslator.FromHtml("#000000");
            }
            config.CensorBgColor = ColorTranslator.ToHtml(bgColor);

            if (optUseImage.Checked)
            {
                if (!optNotUseImage.Checked)
                {
                    config.CensorBgImageEnable = true;
                }
                else
                {
                    config.CensorBgImageEnable = false;
                }   
            }
            else
            {
                //false by default
                config.CensorBgImageEnable = false;
            }

            string dir = Directory.GetCurrentDirectory() + @"\img\";
            string imgName = "CensorBg.png";
            string imgDir = dir + imgName;

            if (!File.Exists(imgDir))
            {
                //Empty Img Name to load Default Image
                config.CensorBgImageName = "";
            }
            else
            {
                config.CensorBgImageName = imgName;
            }
        }

        private void saveNewConfigurationStageToFile()
        {
            ConfigurationHandle.writeConfigurationToFileByUserType(config, user.UserType);
        }
        #endregion

        public void SetUser(userDetail user)
        {
            this.user = user;
            updateFormByUserType();
            updateConfigurationToUserControl(user.UserType);
        }
        public void updateConfigurationToUserControl(int userType)
        {
            readConfigurationFromFile(userType);
            updateComponentValue();
        }

        private void readConfigurationFromFile(int userType)
        {
            configHandle.readConfigurationFromFileByUserType(userType);
            config = ConfigurationHandle.Config;
        }

        private void updateFormByUserType()
        {
            switch (user.UserType.ToString())
            {
                // Guest
                case "0":
                    {
                        pnLoginNote.Visible = true;
                        pnUpgradeVIPNote.Visible = false;
                        customizeControl(0);
                        break;
                    }
                // Normal
                case "1":
                    {
                        pnLoginNote.Visible = false;
                        pnUpgradeVIPNote.Visible = true;
                        customizeControl(0);
                        break;
                    }
                // VIP
                case "2":
                    {
                        customizeControl(1);
                        break;
                    }
            }
        }

        private void customizeControl(int controlCode)
        {
            switch (controlCode.ToString())
            {
                // Disable
                case "0":
                    {
                        foreach (Control ctrl in pnFunctionContainer.Controls)
                        {
                            ctrl.Enabled = false;
                        }
                        pnFunctionContainer.Location = new Point(50, 116);
                        btnApply.Location = new Point(458, 603);
                        btnApply.Enabled = false;
                        break;
                    }
                // Enable
                case "1":
                    {
                        pnLoginNote.Visible = false;
                        pnUpgradeVIPNote.Visible = false;
                        foreach (Control ctrl in pnFunctionContainer.Controls)
                        {
                            ctrl.Enabled = true;
                        }
                        pnFunctionContainer.Location = new Point(50, 80);
                        btnApply.Location = new Point(458, 567);
                        btnApply.Enabled = true;
                        break;
                    }
            }
        }
        public void SetMainForm(mainForm parent)
        {
            this.parent = parent;
        }

        private void lbLoginLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            parent.Logout();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            bool lastAIStage = isAIThreadRunning;
            if (lastAIStage)
            {
                parent.requestDeactivateAI();
            }
            //if (!isAIThreadRunning) { 
            CustomConfirmationBox ccb = new CustomConfirmationBox(ComponentDescriptionText.SAVE_CONFIG_WARNING, ComponentDescriptionText.SAVE_CONFIG_WARNING_TITLE);
            var result = ccb.ShowDialog();

            if (result == DialogResult.OK)
            {

                //dir: Directory.GetCurrentDirectory() + @"\img" + img_name
                //Save tis pic to dir: pnImagePreviewShow.BackgroundImage
                string dir = Directory.GetCurrentDirectory() + ComponentDescriptionText.DEFAULT_CENSOR_IMAGE_FOLDER;
                string imgName = ComponentDescriptionText.DEFAULT_CENSOR_IMAGE_FILE_NAME;
                string imgDir = dir + imgName;
                Bitmap bmp = (Bitmap)pnImagePreviewShow.BackgroundImage.Clone();

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                else
                {
                    try
                    {
                        DeleteFile(imgDir);
                        bmp.Save(imgDir, ImageFormat.Png);
                    }
                    catch (Exception exp)
                    {
                        Console.WriteLine(exp.Message);
                        CustomNotificationBox cnbx = new CustomNotificationBox("Can not delete image because it is currently in use", AlertType.Info);
                        cnbx.Visible = true;
                        return;
                    }
                }
                //Save picked Color to current box
                pnOldBgColor.BackColor = pnColorPreview.BackColor;

                //Update config
                readConfigurationFromFile();

                getNewConfigurationStage();

                saveNewConfigurationStageToFile();

                readConfigurationFromFile();

                this.ActiveControl = null;

                Common.Common.InitCensorForm(user.UserType);

                // Loading Box
                PreLoaderDialog pld = new PreLoaderDialog();
                pld.ShowDialog();

                CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.SAVE_CONFIG_SUCCESS_MESSAGE, AlertType.Info);
                cnb.Visible = true;
            }
            else
            {
                // Do Nothin
            }
            //}
            //else
            //{
            //    CustomNotificationBox cnb = new CustomNotificationBox(
            //        ComponentDescriptionText.SAVE_CONFIG_REQUIRE_STOP_AI_MESSAGE, AlertType.Info);
            //    cnb.Visible = true;
            //}

            if (lastAIStage)
            {
                parent.requestActivateAI();
            }

        }

        private void lbUpgradeLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_UPGRADE_ACCOUNT);
        }

        public void setAIThreadRunningStatus(bool currentStatus)
        {
            this.isAIThreadRunning = currentStatus;
        }
    }
}
