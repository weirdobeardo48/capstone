﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.Common;
using CFML.GUIDesign.CustomizeComponent;
using System.Diagnostics;

namespace CFML.GUIDesign
{
    public partial class configUserControl : UserControl
    {
        #region Variable
        private Configuration.Configuration config;
        private ConfigurationHandle configHandle;

        private userDetail user;
        private mainForm parent;
        private ColorSlider trackBarSensitivity;
        private ColorSlider trackbarCensorBox;
        
        private bool isAIThreadRunning;
        #endregion

        public configUserControl()
        {
            InitializeComponent();
            Initiate();
        }

        private void Initiate()
        {
            config = new Configuration.Configuration();
            configHandle = new ConfigurationHandle();

            #region Trackbar
            trackBarSensitivity = new ColorSlider();
            this.trackBarSensitivity.Location = new Point(26, 12);
            this.trackBarSensitivity.Size = new System.Drawing.Size(428, 20);
            this.trackBarSensitivity.TabIndex = 3;
            this.trackBarSensitivity.Value = 2;
            this.trackBarSensitivity.BorderRoundRectSize = new Size(8, 8);
            this.trackBarSensitivity.MouseEffects = false;
            this.trackBarSensitivity.SmallChange = ((uint)(1u));
            this.trackBarSensitivity.LargeChange = ((uint)(5u));
            this.trackBarSensitivity.Minimum = 1;
            this.trackBarSensitivity.Maximum = 10;
            this.trackBarSensitivity.Name = "trackBarSensitivity";
            this.trackBarSensitivity.ThumbRoundRectSize = new System.Drawing.Size(2, 2);
            this.trackBarSensitivity.ThumbSize = 10;
            this.trackBarSensitivity.BarInnerColor = ColorTranslator.FromHtml("#282828");
            this.trackBarSensitivity.BarOuterColor = ColorTranslator.FromHtml("#f0f0f0");
            this.trackBarSensitivity.BarPenColor = ColorTranslator.FromHtml("#f0f0f0");
            this.trackBarSensitivity.ElapsedInnerColor = ColorTranslator.FromHtml("#282828");
            this.trackBarSensitivity.ElapsedOuterColor = ColorTranslator.FromHtml("#282828");
            this.trackBarSensitivity.ThumbInnerColor = ColorTranslator.FromHtml("#676767");
            this.trackBarSensitivity.ThumbOuterColor = ColorTranslator.FromHtml("#676767");
            this.trackBarSensitivity.ThumbPenColor = ColorTranslator.FromHtml("#676767");
            this.trackBarSensitivity.BackColor = ColorTranslator.FromHtml("#f0f0f0");
            this.trackBarSensitivity.ForeColor = ColorTranslator.FromHtml("#f0f0f0");
            this.trackBarSensitivity.EnabledChanged += new System.EventHandler(this.trackBar_EnabledChanged);
            this.pnCensorSensitivity.Controls.Add(this.trackBarSensitivity);
            

            trackbarCensorBox = new ColorSlider(); 
            this.trackbarCensorBox.Location = new System.Drawing.Point(27, 11);
            this.trackbarCensorBox.BorderRoundRectSize = new Size(8, 8);
            this.trackbarCensorBox.MouseEffects = false;
            this.trackbarCensorBox.SmallChange = ((uint)(1u));
            this.trackbarCensorBox.LargeChange = ((uint)(5u));
            this.trackbarCensorBox.Maximum = 8;
            this.trackbarCensorBox.Minimum = 1;
            this.trackbarCensorBox.Name = "trackbarCensorBox";
            this.trackbarCensorBox.Size = new System.Drawing.Size(427, 20);
            this.trackbarCensorBox.TabIndex = 4;            
            this.trackbarCensorBox.Value = 3;
            this.trackbarCensorBox.ThumbSize = 10;
            this.trackbarCensorBox.ThumbRoundRectSize = new System.Drawing.Size(2, 2);
            this.trackbarCensorBox.BarInnerColor = ColorTranslator.FromHtml("#282828");
            this.trackbarCensorBox.BarOuterColor = ColorTranslator.FromHtml("#f0f0f0");
            this.trackbarCensorBox.BarPenColor = ColorTranslator.FromHtml("#f0f0f0");
            this.trackbarCensorBox.ElapsedInnerColor = ColorTranslator.FromHtml("#282828");
            this.trackbarCensorBox.ElapsedOuterColor = ColorTranslator.FromHtml("#282828");
            this.trackbarCensorBox.ThumbInnerColor = ColorTranslator.FromHtml("#676767");
            this.trackbarCensorBox.ThumbOuterColor = ColorTranslator.FromHtml("#676767");
            this.trackbarCensorBox.ThumbPenColor = ColorTranslator.FromHtml("#676767");
            this.trackbarCensorBox.BackColor = ColorTranslator.FromHtml("#f0f0f0");
            this.trackbarCensorBox.ForeColor = ColorTranslator.FromHtml("#f0f0f0");
            this.trackbarCensorBox.EnabledChanged += new System.EventHandler(this.trackBar_EnabledChanged);
            this.pnCensorBox.Controls.Add(this.trackbarCensorBox);
            #endregion

            isAIThreadRunning = false;
        }

        public void SetUser(userDetail user)
        {
            this.user = user;
            updateConfigurationToUserControl(user.UserType);
            updateFormByUserType();
        }

        public void setAIThreadRunningStatus(bool currentStatus)
        {
            this.isAIThreadRunning = currentStatus;
        }

        private void updateFormByUserType()
        {
            switch (user.UserType.ToString())
            {
                // Guest
                case "0":
                    {
                        pnLoginNote.Visible = true;
                        foreach (Control ctrl in pnFunctionContainer.Controls)
                        {
                            ctrl.Enabled = false;
                        }
                        btnApply.Enabled = false;
                        pnUpgradeVIPNote.Visible = false;
                        pnVIPFunctionOnly.Location = new Point(0, 329);
                        pnFunctionContainer.Location = new Point(50, 117);
                        break;
                    }
                // Normal
                case "1":
                    {
                        pnLoginNote.Visible = false;
                        foreach (Control ctrl in pnFunctionContainer.Controls)
                        {
                            ctrl.Enabled = true;
                        }

                        foreach (Control ctrl in pnVIPFunctionOnly.Controls)
                        {
                            ctrl.Enabled = false;
                        }
                        btnApply.Enabled = true;
                        pnUpgradeVIPNote.Visible = true;
                        pnUpgradeVIPNote.Location = new Point(3, 330);
                        pnVIPFunctionOnly.Location = new Point(0, 365);
                        pnFunctionContainer.Location = new Point(50, 95);
                        break;
                    }
                // VIP
                case "2":
                    {
                        pnLoginNote.Visible = false;
                        foreach (Control ctrl in pnFunctionContainer.Controls)
                        {
                            ctrl.Enabled = true;
                        }

                        foreach (Control ctrl in pnVIPFunctionOnly.Controls)
                        {
                            ctrl.Enabled = true;
                        }
                        btnApply.Enabled = true;
                        pnUpgradeVIPNote.Visible = false;
                        pnVIPFunctionOnly.Location = new Point(0, 325);
                        pnFunctionContainer.Location = new Point(50, 95);
                        break;
                    }
            }

            // Update Realtime Checkbox
            if (cbRealTime.Enabled)
            {
                if (cbRealTime.Checked)
                {
                    foreach (Control ctrl in pnObjectTracking.Controls)
                    {
                        ctrl.Enabled = false;
                    }
                    inpRealtimeScanInterval.Enabled = true;
                }
                else
                {
                    foreach (Control ctrl in pnObjectTracking.Controls)
                    {
                        ctrl.Enabled = true;
                    }
                    inpRealtimeScanInterval.Enabled = false;
                }
            }
        }

        #region ToolTip Event
        private void Dismiss_Toolkit(object sender, EventArgs e)
        {
            ttDescription.Hide(this);
            Panel pn = (Panel)sender;
            pn.BackColor = ColorTranslator.FromHtml("#f0f0f0");
        }

        private void pnSoundDes_MouseHover(object sender, EventArgs e)
        {

            var location = pnFunctionContainer.Location;
            location.Y += pnSound.Location.Y + pnVIPFunctionOnly.Location.Y;
            var size = pnSound.Size;

            ttDescription.ToolTipTitle = Common.ComponentDescriptionText.UC_CONFIGURATION_SOUND_TITLE;
            ttDescription.Show(Common.ComponentDescriptionText.UC_CONFIGURATION_SOUND_DESCRIPTION,
                                this,
                                location.X + pnSound.Width - 20,
                                location.Y - 30);
            pnSoundDes.BackColor = ColorTranslator.FromHtml("#676767");
        }
        
        private void pnObjectTrackingDes_MouseHover(object sender, EventArgs e)
        {
            var location = pnFunctionContainer.Location;
            location.Y += pnObjectTracking.Location.Y;
            var size = pnObjectTracking.Size;

            ttDescription.ToolTipTitle = Common.ComponentDescriptionText.UC_CONFIGURATION_OBJECT_TRACKING_TITLE;
            ttDescription.Show(Common.ComponentDescriptionText.UC_CONFIGURATION_OBJECT_TRACKING_DESCRIPTION,
                                this,
                                location.X + size.Width - 20,
                                location.Y - 30);
            pnObjectTrackingDes.BackColor = ColorTranslator.FromHtml("#676767");
        }

        private void pnInactiveAppDes_MouseHover(object sender, EventArgs e)
        {
            var location = pnFunctionContainer.Location;
            location.Y += pnInactiveApp.Location.Y + pnVIPFunctionOnly.Location.Y;
            var size = pnInactiveApp.Size;

            ttDescription.ToolTipTitle = Common.ComponentDescriptionText.UC_CONFIGURATION_INACTIVE_TITLE;
            ttDescription.Show(Common.ComponentDescriptionText.UC_CONFIGURATION_INACTIVE_DESCRIPTION,
                                this,
                                location.X + size.Width - 20,
                                location.Y - 50);
            pnInactiveAppDes.BackColor = ColorTranslator.FromHtml("#676767");
        }

        private void pnRealtimeDes_MouseHover(object sender, EventArgs e)
        {
            var location = pnFunctionContainer.Location;
            location.Y += pnRealTime.Location.Y;
            var size = pnRealTime.Size;

            ttDescription.ToolTipTitle = Common.ComponentDescriptionText.UC_CONFIGURATION_REALTIME_TITLE;
            ttDescription.Show(Common.ComponentDescriptionText.UC_CONFIGURATION_REALTIME_DESCRIPTION,
                                this,
                                location.X + size.Width,
                                location.Y - 60);
            pnRealtimeDes.BackColor = ColorTranslator.FromHtml("#676767");
        }

        private void pnCensorObjectDes_MouseHover(object sender, EventArgs e)
        {
            var location = pnFunctionContainer.Location;
            location.Y += pnCensorBox.Location.Y;
            var size = pnCensorBox.Size;

            ttDescription.ToolTipTitle = Common.ComponentDescriptionText.UC_CONFIGURATION_CENSOR_OBJECT_TITLE;
            ttDescription.Show(Common.ComponentDescriptionText.UC_CONFIGURATION_CENSOR_OBJECT_DESCRIPTION,
                                this,
                                location.X + size.Width - 50 - pnRealTime.Location.X,
                                location.Y - 80);
            pnCensorObjectDes.BackColor = ColorTranslator.FromHtml("#676767");
        }

        private void pnCensorSensitivityDes_MouseHover(object sender, EventArgs e)
        {
            var location = pnFunctionContainer.Location;
            location.Y += pnCensorSensitivity.Location.Y;
            var size = pnCensorSensitivity.Size;

            ttDescription.ToolTipTitle = Common.ComponentDescriptionText.UC_CONFIGURATION_CENSOR_SENSITIVITY_TITLE;
            ttDescription.Show(Common.ComponentDescriptionText.UC_CONFIGURATION_CENSOR_SENSITIVITY_DESCRIPTION,
                                this,
                                location.X + size.Width - 50 - pnRealTime.Location.X,
                                location.Y - 50);
            pnCensorSensitivityDes.BackColor = ColorTranslator.FromHtml("#676767");
        }
        #endregion

        #region Config

        private void readConfigurationFromFile()
        {
            configHandle.readConfigurationFromFile();
            config = ConfigurationHandle.Config;
        }

        private void readConfigurationFromFile(int userType)
        {
            configHandle.readConfigurationFromFileByUserType(userType);
            config = ConfigurationHandle.Config;
        }

        private void configurationToUI()
        {
            trackBarSensitivity.Value = config.CensorSensitivity / 10;

            trackbarCensorBox.Value = config.MaximumCensoredObject;
            cbObjectTracking.Checked = config.EnableObjectTracking;
            cbRealTime.Checked = !config.EnableRealTimeMode;
            inpRealtimeScanInterval.Text = config.DisabledRealTimeScanInterval.ToString();
            cbInactiveApp.Checked = config.CheckInactiveApplication;
            cbSound.Checked = config.EnableMuteSound;

            if (cbRealTime.Checked)
            {
                foreach (Control ctrl in pnObjectTracking.Controls)
                {
                    ctrl.Enabled = false;
                }
            }
        }

        private void updateComponentValue()
        {
            try
            {
                configurationToUI();
            }
            catch 
            {
                Console.WriteLine("First Attempt: Wrong Configuration Value!");
                configHandle.setDefaultConfiguration();
                config = ConfigurationHandle.Config;
                updateComponentValueByDefault();
            }

        }

        private void updateComponentValueByDefault()
        {
            try
            {
                configurationToUI();
            }
            catch
            {
                Console.WriteLine("Second Attempt: Failed to Config by Default!");
            }

        }

        public void updateConfigurationToUserControl()
        {
            readConfigurationFromFile();
            updateComponentValue();
        }

        public void updateConfigurationToUserControl(int userType)
        {
            readConfigurationFromFile(userType);
            updateComponentValue();
        }

        private void getNewConfigurationStage()
        {
            config.CensorSensitivity = trackBarSensitivity.Value * 10; //%??

            config.MaximumCensoredObject = trackbarCensorBox.Value;
            config.EnableObjectTracking = cbObjectTracking.Checked;
            config.EnableRealTimeMode = !cbRealTime.Checked;
            config.DisabledRealTimeScanInterval = Int32.Parse(inpRealtimeScanInterval.Text);
            config.CheckInactiveApplication = cbInactiveApp.Checked;
            config.EnableMuteSound = cbSound.Checked;
        }

        private void saveNewConfigurationStageToFile()
        {
            ConfigurationHandle.writeConfigurationToFileByUserType(config, user.UserType);
        }
        #endregion

        private void btnApply_Click(object sender, EventArgs e)
        {

            bool lastAIStage = isAIThreadRunning;
            if (lastAIStage)
            {
                parent.requestDeactivateAI();
            }
            //if (!isAIThreadRunning)
            //{
                CustomConfirmationBox ccb = new CustomConfirmationBox(ComponentDescriptionText.SAVE_CONFIG_WARNING, ComponentDescriptionText.SAVE_CONFIG_WARNING_TITLE);
                var result = ccb.ShowDialog();

                if (result == DialogResult.OK) { 
                    if (!validateScanInterValue())
                    {
                        return;
                    }

                    readConfigurationFromFile();

                    getNewConfigurationStage();
            
                    saveNewConfigurationStageToFile();

                    Common.Common.InitCensorForm(user.UserType);

                    // Loading Box
                    PreLoaderDialog pld = new PreLoaderDialog();
                    pld.ShowDialog();
                    this.ActiveControl = null;
                
                    CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.SAVE_CONFIG_SUCCESS_MESSAGE, AlertType.Info);
                    cnb.Visible = true;
                }
            //}
            //else
            //{
            //    CustomNotificationBox cnb = new CustomNotificationBox(
            //        ComponentDescriptionText.SAVE_CONFIG_REQUIRE_STOP_AI_MESSAGE, AlertType.Info);
            //    cnb.Visible = true;
            //}

            if (lastAIStage)
            {
                parent.requestActivateAI();
            }
        }

        private void cbRealTime_OnChange(object sender, EventArgs e)
        {
            // Not allowed Guest User to use this function
            if (user.UserType == 0)
            {
                // Log
                Console.WriteLine("Block Guest user to use Realtime Function");
                // Revert Change
                cbRealTime.Checked = !cbRealTime.Checked;
                // Disable Function
                updateFormByUserType();
                return;
            }
            if (!cbRealTime.Checked) {
                // Enable Object Tracking
                try
                {
                    if (!validateScanInterValue())
                    {
                        cbRealTime.Checked = !cbRealTime.Checked;
                        return;
                    }
                }
                catch
                {
                    cbRealTime.Checked = !cbRealTime.Checked;
                    return;
                }

                foreach (Control ctrl in pnObjectTracking.Controls)
                {
                    ctrl.Enabled = true;
                }
                inpRealtimeScanInterval.Enabled = false;
            }
            else
            {
                CustomConfirmationBox ccb = new CustomConfirmationBox(ComponentDescriptionText.UC_CONFIGURATION_ENABLE_REALTIME_WARINING_TITLE,
                    ComponentDescriptionText.UC_CONFIGURATION_ENABLE_REALTIME_WARINING);
                var result = ccb.ShowDialog();

                if (result == DialogResult.OK)
                {
                    // Disable Function Object Tracking
                    foreach (Control ctrl in pnObjectTracking.Controls)
                    {
                        ctrl.Enabled = false;
                    }
                    inpRealtimeScanInterval.Enabled = true;
                }
                else
                {
                    // Enable Object Tracking
                    foreach (Control ctrl in pnObjectTracking.Controls)
                    {
                        ctrl.Enabled = true;
                    }
                    inpRealtimeScanInterval.Enabled = false;
                    // Revert Change
                    cbRealTime.Checked = !cbRealTime.Checked;
                }
            }
        }

        private void cbObjectTracking_OnChange(object sender, EventArgs e)
        {
            // Not allowed Guest User to use this function
            if (user.UserType == 0)
            {
                // Log
                Console.WriteLine("Block Guest user to use this function");
                // Revert Change
                cbRealTime.Checked = !cbRealTime.Checked;
                // Disable Function
                updateFormByUserType();
                return;
            }

            if (cbObjectTracking.Checked) {
                CustomConfirmationBox ccb = new CustomConfirmationBox(ComponentDescriptionText.UC_CONFIGURATION_ENABLE_OBJECT_TRACKING_WARINING_TITLE,
                    ComponentDescriptionText.UC_CONFIGURATION_ENABLE_OBJECT_TRACKING_WARINING);
                var result = ccb.ShowDialog();
                
                if (result == DialogResult.OK)
                {
                    // Agree to use Object Tracking
                    // Do nothing
                } else
                {
                    // Revert Change
                    cbObjectTracking.Checked = !cbObjectTracking.Checked;
                }
            }
            else
            {
                // Do nothing
            }
        }

        private bool validateScanInterValue()
        {
            var value = 0;
            try
            {
                value = int.Parse(inpRealtimeScanInterval.Text);   
            }
            catch
            {
                CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.SCAN_INTERVAL_VALIDATE_MESSAGE, AlertType.Info);
                cnb.Visible = true;

                inpRealtimeScanInterval.Focus();
                inpRealtimeScanInterval.SelectAll();
                return false;
            }
            return validateScanInterValue(value);
        }

        private bool validateScanInterValue(int? value)
        {
            if (value == null)
            {
                return false;
            }

            try
            {
                if (value < 6 || value > 30)
                {
                    CustomNotificationBox cnbox = new CustomNotificationBox(ComponentDescriptionText.SCAN_INTERVAL_VALIDATE_MESSAGE, AlertType.Info);
                    cnbox.Visible = true;

                    inpRealtimeScanInterval.Focus();
                    inpRealtimeScanInterval.SelectAll();
                    return false;
                }
                return true;
            }
            catch
            {
            }

            CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.SCAN_INTERVAL_VALIDATE_MESSAGE, AlertType.Info);
            cnb.Visible = true;

            inpRealtimeScanInterval.Focus();
            inpRealtimeScanInterval.SelectAll();
            return false;
        }

        private void validateScanInterValue_Validating(object sender, CancelEventArgs e)
        {
            validateScanInterValue();
        }

        public void SetMainForm(mainForm parent)
        {
            this.parent = parent;
        }

        private void lbLoginLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            parent.Logout();
        }

        private void cbInactiveApp_EnabledChanged(object sender, EventArgs e)
        {
            Bunifu.Framework.UI.BunifuCheckbox senderCheckbox = (Bunifu.Framework.UI.BunifuCheckbox)sender;

            if(senderCheckbox.Enabled)
            {
                senderCheckbox.BackColor = Color.FromArgb(40, 40, 40);
                senderCheckbox.CheckedOnColor = Color.FromArgb(40, 40, 40);
                senderCheckbox.ChechedOffColor = Color.FromArgb(56, 56, 56);
            }
            else
            {
                senderCheckbox.BackColor = Color.FromArgb(192, 192, 192);
                senderCheckbox.CheckedOnColor = Color.FromArgb(192, 192, 192);
                senderCheckbox.ChechedOffColor = Color.FromArgb(192, 192, 192);
            }
            
        }
        private void trackBar_EnabledChanged(object sender, EventArgs e)
        {
            ColorSlider senderSlider = (ColorSlider)sender;
            if (senderSlider.Enabled)
            {
                senderSlider.BarInnerColor = ColorTranslator.FromHtml("#282828");
                senderSlider.BarOuterColor = ColorTranslator.FromHtml("#f0f0f0");
                senderSlider.BarPenColor = ColorTranslator.FromHtml("#f0f0f0");
                senderSlider.ElapsedInnerColor = ColorTranslator.FromHtml("#282828");
                senderSlider.ElapsedOuterColor = ColorTranslator.FromHtml("#282828");
                senderSlider.ThumbInnerColor = ColorTranslator.FromHtml("#676767");
                senderSlider.ThumbOuterColor = ColorTranslator.FromHtml("#676767");
                senderSlider.ThumbPenColor = ColorTranslator.FromHtml("#676767");

            }
            else
            {
                senderSlider.BarInnerColor = Color.FromArgb(192, 192, 192);
                senderSlider.BarOuterColor = Color.FromArgb(192, 192, 192);
                senderSlider.BarPenColor = Color.FromArgb(192, 192, 192);
                senderSlider.ElapsedInnerColor = Color.FromArgb(192, 192, 192);
                senderSlider.ElapsedOuterColor = Color.FromArgb(192, 192, 192);
                senderSlider.ThumbInnerColor = Color.FromArgb(192, 192, 192);
                senderSlider.ThumbOuterColor = Color.FromArgb(192, 192, 192);
                senderSlider.ThumbPenColor = Color.FromArgb(192, 192, 192);
            
            }
        }

        private void lbUpgradeLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_UPGRADE_ACCOUNT);
        }

        private void inpRealtimeScanInterval_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.validateScanInterValue();
            }
        }
    }
}
