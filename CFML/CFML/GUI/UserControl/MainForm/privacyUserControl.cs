﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.Common;
using System.Diagnostics;
using CFML.GUIDesign.CustomizeComponent;

namespace CFML.GUIDesign
{
    public partial class privacyUserControl : UserControl
    {

        private userDetail user;
        private mainForm parent;
        private Configuration.Configuration config;
        private ConfigurationHandle configHandle;
        private bool isAIThreadRunning;

        public privacyUserControl()
        {
            InitializeComponent();
            Initiate();
        }

        private void Initiate()
        {
            lblbAnonymousDataCollectDes.Text = Common.ComponentDescriptionText.UC_PRIVACY_ANONYMOUS_COLLECT_DATA_DESCRIPTON;
            cbAnonymousDataCollect.Checked = true;

            config = new Configuration.Configuration();
            configHandle = new ConfigurationHandle();

            isAIThreadRunning = false;
        }

        public void SetUser(userDetail user)
        {
            this.user = user;
            updateFormByUserType();
        }

        private void updateFormByUserType()
        {
            updateConfigurationToUserControl(user.UserType);
            switch (user.UserType.ToString())
            {
                // Guest
                case "0":
                    {
                        pnLoginNote.Visible = true;
                        pnUpgradeVIPNote.Visible = false;
                        customizeControl(0);
                        break;
                    }
                // Normal
                case "1":
                    {
                        pnLoginNote.Visible = false;
                        pnUpgradeVIPNote.Visible = true;
                        customizeControl(0);
                        break;
                    }
                // VIP
                case "2":
                    {
                        pnLoginNote.Visible = false;
                        pnUpgradeVIPNote.Visible = false;
                        customizeControl(1);
                        break;
                    }
            }
            
        }

        #region Config
        public void updateConfigurationToUserControl(int userType)
        {
            readConfigurationFromFile(userType);
            updateComponentValue();
        }

        private void readConfigurationFromFile(int userType)
        {
            configHandle.readConfigurationFromFileByUserType(userType);
            config = ConfigurationHandle.Config;
        }

        private void updateComponentValue()
        {
            try
            {
                configurationToUI();
            }
            catch
            {
                Console.WriteLine("First Attempt: Wrong Configuration Value!");
                configHandle.setDefaultConfiguration();
                config = ConfigurationHandle.Config;
                updateComponentValueByDefault();
            }

        }

        private void configurationToUI()
        {
            cbAnonymousDataCollect.Checked = config.PrivacyCheck;
        }

        private void updateComponentValueByDefault()
        {
            cbAnonymousDataCollect.Checked = true;
        }

        private void readConfigurationFromFile()
        {
            configHandle.readConfigurationFromFile();
            config = ConfigurationHandle.Config;
        }

        private void getNewConfigurationStage()
        {
            config.PrivacyCheck = cbAnonymousDataCollect.Checked;
        }

        private void saveNewConfigurationStageToFile()
        {
            ConfigurationHandle.writeConfigurationToFileByUserType(config, user.UserType);
        }
        #endregion

        private void customizeControl(int controlCode)
        {
            switch (controlCode.ToString())
            {
                // Disable
                case "0":
                    {
                        foreach (Control ctrl in pnFunctionContainer.Controls)
                        {
                            ctrl.Enabled = false;
                        }
                        pnFunctionContainer.Location = new Point(50, 154);
                        break;
                    }
                // Enable
                case "1":
                    {
                        pnLoginNote.Visible = false;
                        pnUpgradeVIPNote.Visible = false;
                        foreach (Control ctrl in pnFunctionContainer.Controls)
                        {
                            ctrl.Enabled = true;
                        }
                        pnFunctionContainer.Location = new Point(50, 100);
                        break;
                    }
            }
        }

        public void SetMainForm(mainForm parent)
        {
            this.parent = parent;
        }

        private void lbLoginLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            parent.Logout();
        }

        private void cbAnonymousDataCollect_EnabledChanged(object sender, EventArgs e)
        {
            if (cbAnonymousDataCollect.Enabled)
            {
                cbAnonymousDataCollect.BackColor = Color.FromArgb(40, 40, 40);
            }
            else
            {
                cbAnonymousDataCollect.BackColor = Color.FromArgb(192, 192, 192);
            } 
            
        }

        private void lbUpgradeLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_UPGRADE_ACCOUNT);
        }

        private void cbAnonymousDataCollect_Click(object sender, EventArgs e)
        {
            
        }

        private void cbAnonymousDataCollect_OnChange(object sender, EventArgs e)
        {
            bool lastAIStage = isAIThreadRunning;
            if (lastAIStage)
            {
                parent.requestDeactivateAI();
            }

            CustomConfirmationBox ccb = new CustomConfirmationBox(ComponentDescriptionText.SAVE_CONFIG_WARNING, ComponentDescriptionText.SAVE_CONFIG_WARNING_TITLE);
            var result = ccb.ShowDialog();

            if (result == DialogResult.OK)
            {

                readConfigurationFromFile();

                getNewConfigurationStage();

                saveNewConfigurationStageToFile();

                // Loading Box
                PreLoaderDialog pld = new PreLoaderDialog();
                pld.ShowDialog();

                this.ActiveControl = null;
                CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.SAVE_CONFIG_SUCCESS_MESSAGE, AlertType.Info);
                cnb.Visible = true;

            }
            else
            {
                cbAnonymousDataCollect.Checked = !cbAnonymousDataCollect.Checked;
            }

            if (lastAIStage)
            {
                parent.requestActivateAI();
            }

        }

        public void setAIThreadRunningStatus(bool currentStatus)
        {
            this.isAIThreadRunning = currentStatus;
        }
    }
}
