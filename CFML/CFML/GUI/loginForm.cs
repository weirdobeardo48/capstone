﻿using CFML.Capture;
using CFML.Common;
using CFML.GUI.CustomizeComponent;
using CFML.GUIDesign.CustomizeComponent;
using CFML.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CFML.GUIDesign
{
    public partial class loginForm : Form
    {

        #region Variable

        #region User Control
        private inputForm InputForm;
        private mainForm MainForm;
        #endregion

        #region User Detail
        private userDetail user;
        #endregion

        LoginByMasterPassword mpf;
        #endregion

        public bool runFromStartup = false;

        public loginForm()
        {
            InitializeComponent();
            Initiate();
        }

        public loginForm(bool runFromStartup)
        {
            this.runFromStartup = runFromStartup;
            InitializeComponent();
            Initiate();
            if (runFromStartup)
            {
                hideFormToTray();
                this.MainForm.SetUser(new userDetail());
                this.requestActivateAI();
            }
            
        }

        private void Initiate()
        {
            InputForm = new inputForm();
            MainForm = new mainForm();

            InputForm.SetParent(this);

            pnLoginContainer.Controls.Add(InputForm);

            optionChange(0);

            #region Tray Icon
            this.ShowInTaskbar = true;
            this.notifyIcon.Visible = false;
            //this.Show();
            #endregion

            mpf = new LoginByMasterPassword(this);
            notifyIcon.Icon = Resources.AppIcon;
            this.ShowInTaskbar = true;

            this.Icon = Resources.AppIcon;
        }

        public void requestActivateAI()
        {
            MainForm.requestActivateAI();
        }

        public void requestDeactivateAI()
        {
            MainForm.requestDeactivateAI();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }


        public void optionChange(int index)
        {
            switch (index.ToString())
            {
                case "0":
                    {

                        lbForgotPasswordLink.Visible = false;
                        lbRegisterLink.Visible = true;
                        pnLoginOption.BringToFront();
                        btnBackToLoginOption.Visible = false;
                        btnLoginBtn.Visible = false;
                        break;
                    }
                case "1":
                    {

                        lbForgotPasswordLink.Visible = true;
                        lbRegisterLink.Visible = false;
                        pnLoginContainer.BringToFront();
                        btnBackToLoginOption.Visible = true;
                        btnLoginBtn.Visible = true;
                        break;
                    }
            }
        }

        private void btnLoginAsMember_Click(object sender, EventArgs e)
        {
            optionChange(1);
        }

        private void btnLoginAsGuest_Click(object sender, EventArgs e)
        {
            if (MainForm == null)
            {
                MainForm = new mainForm();
            }

            // Update User Info <Guest>
            user = new userDetail();

            MainForm.SetUser(user);

            MainForm.Visible = true;
            MainForm.setLoginForm(this);
            MainForm.RevolkeForm();
            this.Hide();

            // Set login form to default Stage
            optionChange(0);

            this.notifyIcon.Visible = false;

            MainForm.CheckForUpdate();
        }

        private void btnBackToLoginOption_Click(object sender, EventArgs e)
        {
            optionChange(0);
        }

        public void requestLogin()
        {
            btnLoginBtn_Click(null, null);
        }

        private void btnLoginBtn_Click(object sender, EventArgs e)
        {
            CustomNotificationBox cnb;
            // Get Form's Login Info
            loginDetail login = InputForm.getLoginDetail();
            if (login.Username == null || login.Username.Equals(String.Empty)
                || login.Password == null || login.Password.Equals(String.Empty)
                )
            {
                cnb = new CustomNotificationBox(ComponentDescriptionText.FILL_THE_FORM_MESSAGE, AlertType.Info);
                cnb.Visible = true;
                return;
            }

            // Check Login Info <login>
            loginResult loginSuccess = null;

            if (Common.Common.validConnection())
            {
                loginSuccess = Common.Common.checkLoginInfo(login);
                user = new userDetail(loginSuccess.user);
                Console.WriteLine("Login Success: " + loginSuccess.result);
                Common.Common.writeLastLoggedInUserToRegistry(user.OwnerName, 1);
            }
            else
            {
                userModel userM = Common.Common.checkOfflineLoginFromFile(login);
                if (userM == null)
                {
                    cnb = new CustomNotificationBox(ComponentDescriptionText.WRONG_USERNAME_PASSWORD_MESSAGE, AlertType.Info);
                    cnb.Visible = true;
                    return;
                }
                else
                {
                    int tokenLive = Common.Common.checkValidOfflineLogin(userM.userName);
                    if (tokenLive == -1)
                    {
                        cnb = new CustomNotificationBox(ComponentDescriptionText.LOGIN_SESSION_EXPIRED_MESSAGE, AlertType.Info);
                        cnb.Visible = true;
                        return;
                    }
                    else
                    {
                        Common.Common.writeLastLoggedInUserToRegistry(userM.userName, tokenLive + 1);
                        loginSuccess = new loginResult(userM, true);
                        user = new userDetail(loginSuccess.user);
                    }
                }
            }

            if (loginSuccess.result)
            {
                InputForm.clearLoginForm();

                #region Mocking Data
                //// Mock  user data

                //Guest User
                //user = new userDetail();

                //Normal User
                //user = new userDetail("Nguyen Xuan Truong", "truongnx@gmail.com", 1, null);

                ////VIP User
                //DateTime expDate = new DateTime(2018, 4, 30);
                //user = new userDetail("Nguyen Xuan Truong", "truongnx@gmail.com", 2, expDate);
                #endregion

                if (MainForm == null)
                {
                    MainForm = new mainForm();
                }

                MainForm.SetUser(user);

                Common.Common.writeLoggedInUserToFile(login, user);

                MainForm.Visible = true;
                MainForm.setLoginForm(this);
                MainForm.RevolkeForm();
                this.Hide();
                
                // Set login form to default Stage
                optionChange(0);
                this.notifyIcon.Visible = false;

                MainForm.CheckForUpdate();

            }
            else
            {
                CustomConfirmationBox ccb = new CustomConfirmationBox(
                    ComponentDescriptionText.LOGIN_FAILED_MESSAGE, 
                    ComponentDescriptionText.LOGIN_FAILED_MESSAGE_TITLE);
                
                var result = ccb.ShowDialog();

                if (result == DialogResult.OK)
                {
                    // Login By Guest
                    btnLoginAsGuest_Click(null, null);
                }
                else
                {
                    // Do Nothin
                }
                
            }
        }

        public void RevolkeForm()
        {
            
            this.Show();
            this.ShowInTaskbar = true;
            this.notifyIcon.Visible = false;

            WindowSnap.SetForegroundWindow(this.Handle);

            this.requestDeactivateAI();
            this.runFromStartup = false;
        }

        private void loginForm_Resize(object sender, EventArgs e)
        {
            if (runFromStartup)
                return;
            this.ShowInTaskbar = true;
            this.notifyIcon.Visible = false;
            this.Show();
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (mpf == null)
            {
                mpf = new LoginByMasterPassword(this);
            }
            if (mpf.WindowState == FormWindowState.Minimized)
            {
                mpf.WindowState = FormWindowState.Normal;
            }
            mpf.Show();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            CustomConfirmationBox ccb = new CustomConfirmationBox(
                    ComponentDescriptionText.QUIT_APPLICATION_MESSAGE,
                    ComponentDescriptionText.QUIT_APPLICATION_TITLE, AlertType.Quit);

            var result = ccb.ShowDialog();

            if (result == DialogResult.OK)
            {
                callCloseForm();
            }
            else
            {
                //this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;
                this.notifyIcon.Visible = true;
                this.Hide();
            }
        }

        public void hideFormToTray()
        {
            //this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            this.notifyIcon.Visible = true;
            this.Hide();
        }

        private void loginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void lbRegisterLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_REGISTER);
        }

        private void lbForgotPasswordLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ComponentDescriptionText.URL_FORGET_PASSWORD);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notifyIcon_MouseDoubleClick(null, null);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            callCloseForm();
        }

        private void callCloseForm()
        {
            CloseApplicationForm closeForm = new CloseApplicationForm();
            closeForm.ShowDialog();
        }
    }
}
