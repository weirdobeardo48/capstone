﻿namespace CFML.GUIDesign
{
    partial class setInitialKeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(setInitialKeyForm));
            this.bunifuElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btnQuit = new System.Windows.Forms.PictureBox();
            this.lbCfKey = new System.Windows.Forms.Label();
            this.lbSetNewKey = new System.Windows.Forms.Label();
            this.btnConfirm = new Bunifu.Framework.UI.BunifuThinButton2();
            this.pnNewKeyBg = new System.Windows.Forms.Panel();
            this.inpNewKey = new System.Windows.Forms.TextBox();
            this.pnCfKeyBg = new System.Windows.Forms.Panel();
            this.inpCfKey = new System.Windows.Forms.TextBox();
            this.lbSetMasterPassword = new Bunifu.Framework.UI.BunifuCustomLabel();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).BeginInit();
            this.pnNewKeyBg.SuspendLayout();
            this.pnCfKeyBg.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse
            // 
            this.bunifuElipse.ElipseRadius = 5;
            this.bunifuElipse.TargetControl = this;
            // 
            // btnQuit
            // 
            this.btnQuit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuit.BackgroundImage")));
            this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQuit.Location = new System.Drawing.Point(541, 12);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(56, 25);
            this.btnQuit.TabIndex = 7;
            this.btnQuit.TabStop = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // lbCfKey
            // 
            this.lbCfKey.AutoSize = true;
            this.lbCfKey.Font = new System.Drawing.Font("Lato", 9.75F);
            this.lbCfKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbCfKey.Location = new System.Drawing.Point(99, 163);
            this.lbCfKey.Name = "lbCfKey";
            this.lbCfKey.Size = new System.Drawing.Size(150, 16);
            this.lbCfKey.TabIndex = 9;
            this.lbCfKey.Text = "Confirm Master Pasword";
            // 
            // lbSetNewKey
            // 
            this.lbSetNewKey.AutoSize = true;
            this.lbSetNewKey.Font = new System.Drawing.Font("Lato", 9.75F);
            this.lbSetNewKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbSetNewKey.Location = new System.Drawing.Point(90, 112);
            this.lbSetNewKey.Name = "lbSetNewKey";
            this.lbSetNewKey.Size = new System.Drawing.Size(159, 16);
            this.lbSetNewKey.TabIndex = 8;
            this.lbSetNewKey.Text = "Set New Master Password";
            // 
            // btnConfirm
            // 
            this.btnConfirm.ActiveBorderThickness = 1;
            this.btnConfirm.ActiveCornerRadius = 20;
            this.btnConfirm.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnConfirm.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnConfirm.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnConfirm.BackColor = System.Drawing.SystemColors.Control;
            this.btnConfirm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConfirm.BackgroundImage")));
            this.btnConfirm.ButtonText = "OK";
            this.btnConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfirm.Font = new System.Drawing.Font("Lato", 12F);
            this.btnConfirm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnConfirm.IdleBorderThickness = 1;
            this.btnConfirm.IdleCornerRadius = 20;
            this.btnConfirm.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnConfirm.IdleForecolor = System.Drawing.Color.White;
            this.btnConfirm.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnConfirm.Location = new System.Drawing.Point(231, 224);
            this.btnConfirm.Margin = new System.Windows.Forms.Padding(5);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(123, 45);
            this.btnConfirm.TabIndex = 12;
            this.btnConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // pnNewKeyBg
            // 
            this.pnNewKeyBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnNewKeyBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnNewKeyBg.Controls.Add(this.inpNewKey);
            this.pnNewKeyBg.Location = new System.Drawing.Point(263, 97);
            this.pnNewKeyBg.Name = "pnNewKeyBg";
            this.pnNewKeyBg.Size = new System.Drawing.Size(250, 45);
            this.pnNewKeyBg.TabIndex = 13;
            this.pnNewKeyBg.Click += new System.EventHandler(this.pnNewKeyBg_Click);
            // 
            // inpNewKey
            // 
            this.inpNewKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpNewKey.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpNewKey.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpNewKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpNewKey.Location = new System.Drawing.Point(8, 12);
            this.inpNewKey.Name = "inpNewKey";
            this.inpNewKey.PasswordChar = '•';
            this.inpNewKey.Size = new System.Drawing.Size(231, 16);
            this.inpNewKey.TabIndex = 1;
            this.inpNewKey.Enter += new System.EventHandler(this.inpNewKey_Enter);
            this.inpNewKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inpNewKey_KeyDown);
            this.inpNewKey.Leave += new System.EventHandler(this.inpNewKey_Leave);
            // 
            // pnCfKeyBg
            // 
            this.pnCfKeyBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnCfKeyBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnCfKeyBg.Controls.Add(this.inpCfKey);
            this.pnCfKeyBg.Location = new System.Drawing.Point(263, 148);
            this.pnCfKeyBg.Name = "pnCfKeyBg";
            this.pnCfKeyBg.Size = new System.Drawing.Size(250, 45);
            this.pnCfKeyBg.TabIndex = 14;
            this.pnCfKeyBg.Click += new System.EventHandler(this.pnCfKeyBg_Click);
            // 
            // inpCfKey
            // 
            this.inpCfKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpCfKey.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpCfKey.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpCfKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpCfKey.Location = new System.Drawing.Point(8, 12);
            this.inpCfKey.Name = "inpCfKey";
            this.inpCfKey.PasswordChar = '•';
            this.inpCfKey.Size = new System.Drawing.Size(231, 16);
            this.inpCfKey.TabIndex = 1;
            this.inpCfKey.Enter += new System.EventHandler(this.inpCfKey_Enter);
            this.inpCfKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inpNewKey_KeyDown);
            this.inpCfKey.Leave += new System.EventHandler(this.inpCfKey_Leave);
            // 
            // lbSetMasterPassword
            // 
            this.lbSetMasterPassword.AutoSize = true;
            this.lbSetMasterPassword.Font = new System.Drawing.Font("Lato", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbSetMasterPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbSetMasterPassword.Location = new System.Drawing.Point(211, 53);
            this.lbSetMasterPassword.Name = "lbSetMasterPassword";
            this.lbSetMasterPassword.Size = new System.Drawing.Size(166, 18);
            this.lbSetMasterPassword.TabIndex = 15;
            this.lbSetMasterPassword.Text = "Set Master Password";
            // 
            // setInitialKeyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 294);
            this.Controls.Add(this.lbSetMasterPassword);
            this.Controls.Add(this.pnCfKeyBg);
            this.Controls.Add(this.pnNewKeyBg);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.lbCfKey);
            this.Controls.Add(this.lbSetNewKey);
            this.Controls.Add(this.btnQuit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "setInitialKeyForm";
            this.Text = "Set Master Password";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.setInitialKeyForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).EndInit();
            this.pnNewKeyBg.ResumeLayout(false);
            this.pnNewKeyBg.PerformLayout();
            this.pnCfKeyBg.ResumeLayout(false);
            this.pnCfKeyBg.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse;
        private System.Windows.Forms.PictureBox btnQuit;
        private System.Windows.Forms.Label lbCfKey;
        private System.Windows.Forms.Label lbSetNewKey;
        private Bunifu.Framework.UI.BunifuThinButton2 btnConfirm;
        private System.Windows.Forms.Panel pnCfKeyBg;
        private System.Windows.Forms.TextBox inpCfKey;
        private System.Windows.Forms.Panel pnNewKeyBg;
        private System.Windows.Forms.TextBox inpNewKey;
        private Bunifu.Framework.UI.BunifuCustomLabel lbSetMasterPassword;
    }
}