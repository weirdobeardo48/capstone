﻿using Bunifu.Framework.UI;

namespace CFML.GUIDesign
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation3 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.bunifuElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pnTop = new System.Windows.Forms.Panel();
            this.btnExit = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnMinimize = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.lbCheckForUpdate = new System.Windows.Forms.Label();
            this.lbVersionStatus = new System.Windows.Forms.Label();
            this.bnfDragControlTitleBar = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuTransition = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.pnUserControl = new System.Windows.Forms.Panel();
            this.pnLeftPnUserProfile = new System.Windows.Forms.Panel();
            this.btnAccountPic = new System.Windows.Forms.Button();
            this.btnLogout = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lbAppName = new System.Windows.Forms.Label();
            this.lbWelcomeNote = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lbVIPStatus = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pnMenuContainer = new System.Windows.Forms.Panel();
            this.btnMenuAbout = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMenuMain = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMenuAccount = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMenuPrivacy = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMenuConfiguration = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMenuCustomize = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMenuChangeKey = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pnLeftMenu = new System.Windows.Forms.Panel();
            this.bnfDragControlAppName = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bnfDragControlAppLogo = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).BeginInit();
            this.pnBottom.SuspendLayout();
            this.pnLeftPnUserProfile.SuspendLayout();
            this.pnMenuContainer.SuspendLayout();
            this.pnLeftMenu.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse
            // 
            this.bunifuElipse.ElipseRadius = 5;
            this.bunifuElipse.TargetControl = this;
            // 
            // pnTop
            // 
            this.pnTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.pnTop.Controls.Add(this.btnExit);
            this.pnTop.Controls.Add(this.btnMinimize);
            this.bunifuTransition.SetDecoration(this.pnTop, BunifuAnimatorNS.DecorationType.None);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(200, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(612, 26);
            this.pnTop.TabIndex = 1;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.bunifuTransition.SetDecoration(this.btnExit, BunifuAnimatorNS.DecorationType.None);
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageActive = null;
            this.btnExit.Location = new System.Drawing.Point(558, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(53, 26);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExit.TabIndex = 3;
            this.btnExit.TabStop = false;
            this.btnExit.Zoom = 10;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnReColored);
            this.btnExit.MouseHover += new System.EventHandler(this.btnExit_MouseHover);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.bunifuTransition.SetDecoration(this.btnMinimize, BunifuAnimatorNS.DecorationType.None);
            this.btnMinimize.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimize.Image")));
            this.btnMinimize.ImageActive = null;
            this.btnMinimize.Location = new System.Drawing.Point(513, 0);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(45, 26);
            this.btnMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimize.TabIndex = 2;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.Zoom = 10;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            this.btnMinimize.MouseLeave += new System.EventHandler(this.btnReColored);
            this.btnMinimize.MouseHover += new System.EventHandler(this.btnMinimize_MouseHover);
            // 
            // pnBottom
            // 
            this.pnBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.pnBottom.Controls.Add(this.lbCheckForUpdate);
            this.pnBottom.Controls.Add(this.lbVersionStatus);
            this.bunifuTransition.SetDecoration(this.pnBottom, BunifuAnimatorNS.DecorationType.None);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(200, 669);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(612, 27);
            this.pnBottom.TabIndex = 3;
            // 
            // lbCheckForUpdate
            // 
            this.lbCheckForUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCheckForUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.lbCheckForUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition.SetDecoration(this.lbCheckForUpdate, BunifuAnimatorNS.DecorationType.None);
            this.lbCheckForUpdate.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCheckForUpdate.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lbCheckForUpdate.Location = new System.Drawing.Point(486, 2);
            this.lbCheckForUpdate.Name = "lbCheckForUpdate";
            this.lbCheckForUpdate.Size = new System.Drawing.Size(118, 23);
            this.lbCheckForUpdate.TabIndex = 7;
            this.lbCheckForUpdate.Text = "Check for updates";
            this.lbCheckForUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbCheckForUpdate.Click += new System.EventHandler(this.lbCheckForUpdate_Click);
            // 
            // lbVersionStatus
            // 
            this.lbVersionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbVersionStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.bunifuTransition.SetDecoration(this.lbVersionStatus, BunifuAnimatorNS.DecorationType.None);
            this.lbVersionStatus.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVersionStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbVersionStatus.Location = new System.Drawing.Point(0, 2);
            this.lbVersionStatus.Name = "lbVersionStatus";
            this.lbVersionStatus.Size = new System.Drawing.Size(480, 23);
            this.lbVersionStatus.TabIndex = 6;
            this.lbVersionStatus.Text = "Version 1.0";
            this.lbVersionStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bnfDragControlTitleBar
            // 
            this.bnfDragControlTitleBar.Fixed = true;
            this.bnfDragControlTitleBar.Horizontal = true;
            this.bnfDragControlTitleBar.TargetControl = this.pnTop;
            this.bnfDragControlTitleBar.Vertical = true;
            // 
            // bunifuTransition
            // 
            this.bunifuTransition.AnimationType = BunifuAnimatorNS.AnimationType.Leaf;
            this.bunifuTransition.Cursor = null;
            animation3.AnimateOnlyDifferences = true;
            animation3.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.BlindCoeff")));
            animation3.LeafCoeff = 1F;
            animation3.MaxTime = 1F;
            animation3.MinTime = 0F;
            animation3.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicCoeff")));
            animation3.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicShift")));
            animation3.MosaicSize = 0;
            animation3.Padding = new System.Windows.Forms.Padding(0);
            animation3.RotateCoeff = 0F;
            animation3.RotateLimit = 0F;
            animation3.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.ScaleCoeff")));
            animation3.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.SlideCoeff")));
            animation3.TimeCoeff = 0F;
            animation3.TransparencyCoeff = 0F;
            this.bunifuTransition.DefaultAnimation = animation3;
            // 
            // pnUserControl
            // 
            this.pnUserControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.bunifuTransition.SetDecoration(this.pnUserControl, BunifuAnimatorNS.DecorationType.None);
            this.pnUserControl.Location = new System.Drawing.Point(200, 26);
            this.pnUserControl.Name = "pnUserControl";
            this.pnUserControl.Size = new System.Drawing.Size(612, 643);
            this.pnUserControl.TabIndex = 4;
            // 
            // pnLeftPnUserProfile
            // 
            this.pnLeftPnUserProfile.Controls.Add(this.btnAccountPic);
            this.pnLeftPnUserProfile.Controls.Add(this.btnLogout);
            this.pnLeftPnUserProfile.Controls.Add(this.lbAppName);
            this.pnLeftPnUserProfile.Controls.Add(this.lbWelcomeNote);
            this.pnLeftPnUserProfile.Controls.Add(this.lbVIPStatus);
            this.bunifuTransition.SetDecoration(this.pnLeftPnUserProfile, BunifuAnimatorNS.DecorationType.None);
            this.pnLeftPnUserProfile.Location = new System.Drawing.Point(5, 7);
            this.pnLeftPnUserProfile.Name = "pnLeftPnUserProfile";
            this.pnLeftPnUserProfile.Size = new System.Drawing.Size(188, 222);
            this.pnLeftPnUserProfile.TabIndex = 6;
            // 
            // btnAccountPic
            // 
            this.btnAccountPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAccountPic.BackgroundImage")));
            this.btnAccountPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuTransition.SetDecoration(this.btnAccountPic, BunifuAnimatorNS.DecorationType.None);
            this.btnAccountPic.Location = new System.Drawing.Point(13, 81);
            this.btnAccountPic.Name = "btnAccountPic";
            this.btnAccountPic.Size = new System.Drawing.Size(59, 58);
            this.btnAccountPic.TabIndex = 6;
            this.btnAccountPic.UseVisualStyleBackColor = true;
            this.btnAccountPic.Click += new System.EventHandler(this.btnMenuAccount_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogout.BorderRadius = 0;
            this.btnLogout.ButtonText = "Logout";
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition.SetDecoration(this.btnLogout, BunifuAnimatorNS.DecorationType.None);
            this.btnLogout.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnLogout.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnLogout.Iconcolor = System.Drawing.Color.Transparent;
            this.btnLogout.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnLogout.Iconimage")));
            this.btnLogout.Iconimage_right = null;
            this.btnLogout.Iconimage_right_Selected = null;
            this.btnLogout.Iconimage_Selected = null;
            this.btnLogout.IconMarginLeft = 1;
            this.btnLogout.IconMarginRight = 0;
            this.btnLogout.IconRightVisible = true;
            this.btnLogout.IconRightZoom = 0D;
            this.btnLogout.IconVisible = true;
            this.btnLogout.IconZoom = 90D;
            this.btnLogout.IsTab = false;
            this.btnLogout.Location = new System.Drawing.Point(3, 168);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnLogout.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnLogout.OnHoverTextColor = System.Drawing.Color.White;
            this.btnLogout.selected = false;
            this.btnLogout.Size = new System.Drawing.Size(121, 33);
            this.btnLogout.TabIndex = 0;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Textcolor = System.Drawing.Color.White;
            this.btnLogout.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // lbAppName
            // 
            this.lbAppName.AutoSize = true;
            this.bunifuTransition.SetDecoration(this.lbAppName, BunifuAnimatorNS.DecorationType.None);
            this.lbAppName.Font = new System.Drawing.Font("Somatic Rounded", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAppName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.lbAppName.Location = new System.Drawing.Point(7, 19);
            this.lbAppName.Name = "lbAppName";
            this.lbAppName.Size = new System.Drawing.Size(91, 32);
            this.lbAppName.TabIndex = 1;
            this.lbAppName.Text = "CFML ";
            // 
            // lbWelcomeNote
            // 
            this.bunifuTransition.SetDecoration(this.lbWelcomeNote, BunifuAnimatorNS.DecorationType.None);
            this.lbWelcomeNote.Font = new System.Drawing.Font("Lato Heavy", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWelcomeNote.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.lbWelcomeNote.Location = new System.Drawing.Point(72, 80);
            this.lbWelcomeNote.Name = "lbWelcomeNote";
            this.lbWelcomeNote.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbWelcomeNote.Size = new System.Drawing.Size(113, 29);
            this.lbWelcomeNote.TabIndex = 3;
            this.lbWelcomeNote.Text = "Welcome, ABC";
            this.lbWelcomeNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbVIPStatus
            // 
            this.bunifuTransition.SetDecoration(this.lbVIPStatus, BunifuAnimatorNS.DecorationType.None);
            this.lbVIPStatus.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVIPStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.lbVIPStatus.Location = new System.Drawing.Point(72, 116);
            this.lbVIPStatus.Name = "lbVIPStatus";
            this.lbVIPStatus.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbVIPStatus.Size = new System.Drawing.Size(110, 23);
            this.lbVIPStatus.TabIndex = 4;
            this.lbVIPStatus.Text = "VIP User";
            this.lbVIPStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnMenuContainer
            // 
            this.pnMenuContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.pnMenuContainer.Controls.Add(this.btnMenuAbout);
            this.pnMenuContainer.Controls.Add(this.btnMenuMain);
            this.pnMenuContainer.Controls.Add(this.btnMenuAccount);
            this.pnMenuContainer.Controls.Add(this.btnMenuPrivacy);
            this.pnMenuContainer.Controls.Add(this.btnMenuConfiguration);
            this.pnMenuContainer.Controls.Add(this.btnMenuCustomize);
            this.pnMenuContainer.Controls.Add(this.btnMenuChangeKey);
            this.bunifuTransition.SetDecoration(this.pnMenuContainer, BunifuAnimatorNS.DecorationType.None);
            this.pnMenuContainer.Location = new System.Drawing.Point(0, 240);
            this.pnMenuContainer.Name = "pnMenuContainer";
            this.pnMenuContainer.Size = new System.Drawing.Size(200, 339);
            this.pnMenuContainer.TabIndex = 12;
            // 
            // btnMenuAbout
            // 
            this.btnMenuAbout.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuAbout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMenuAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuAbout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMenuAbout.BorderRadius = 0;
            this.btnMenuAbout.ButtonText = "        About Us";
            this.btnMenuAbout.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bunifuTransition.SetDecoration(this.btnMenuAbout, BunifuAnimatorNS.DecorationType.None);
            this.btnMenuAbout.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuAbout.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuAbout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnMenuAbout.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMenuAbout.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMenuAbout.Iconimage")));
            this.btnMenuAbout.Iconimage_right = null;
            this.btnMenuAbout.Iconimage_right_Selected = null;
            this.btnMenuAbout.Iconimage_Selected = null;
            this.btnMenuAbout.IconMarginLeft = 20;
            this.btnMenuAbout.IconMarginRight = 0;
            this.btnMenuAbout.IconRightVisible = true;
            this.btnMenuAbout.IconRightZoom = 0D;
            this.btnMenuAbout.IconVisible = true;
            this.btnMenuAbout.IconZoom = 50D;
            this.btnMenuAbout.IsTab = false;
            this.btnMenuAbout.Location = new System.Drawing.Point(0, 291);
            this.btnMenuAbout.Name = "btnMenuAbout";
            this.btnMenuAbout.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuAbout.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(79)))), ((int)(((byte)(79)))));
            this.btnMenuAbout.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMenuAbout.selected = false;
            this.btnMenuAbout.Size = new System.Drawing.Size(200, 48);
            this.btnMenuAbout.TabIndex = 11;
            this.btnMenuAbout.Text = "        About Us";
            this.btnMenuAbout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuAbout.Textcolor = System.Drawing.Color.White;
            this.btnMenuAbout.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuAbout.Click += new System.EventHandler(this.btnMenuAbout_Click);
            // 
            // btnMenuMain
            // 
            this.btnMenuMain.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMenuMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMenuMain.BorderRadius = 0;
            this.btnMenuMain.ButtonText = "        Main";
            this.btnMenuMain.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bunifuTransition.SetDecoration(this.btnMenuMain, BunifuAnimatorNS.DecorationType.None);
            this.btnMenuMain.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuMain.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuMain.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnMenuMain.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMenuMain.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMenuMain.Iconimage")));
            this.btnMenuMain.Iconimage_right = null;
            this.btnMenuMain.Iconimage_right_Selected = null;
            this.btnMenuMain.Iconimage_Selected = null;
            this.btnMenuMain.IconMarginLeft = 20;
            this.btnMenuMain.IconMarginRight = 0;
            this.btnMenuMain.IconRightVisible = true;
            this.btnMenuMain.IconRightZoom = 0D;
            this.btnMenuMain.IconVisible = true;
            this.btnMenuMain.IconZoom = 50D;
            this.btnMenuMain.IsTab = false;
            this.btnMenuMain.Location = new System.Drawing.Point(0, 3);
            this.btnMenuMain.Name = "btnMenuMain";
            this.btnMenuMain.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuMain.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnMenuMain.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMenuMain.selected = true;
            this.btnMenuMain.Size = new System.Drawing.Size(201, 48);
            this.btnMenuMain.TabIndex = 4;
            this.btnMenuMain.Text = "        Main";
            this.btnMenuMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuMain.Textcolor = System.Drawing.Color.White;
            this.btnMenuMain.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuMain.Click += new System.EventHandler(this.btnMenuMain_Click);
            // 
            // btnMenuAccount
            // 
            this.btnMenuAccount.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuAccount.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMenuAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMenuAccount.BorderRadius = 0;
            this.btnMenuAccount.ButtonText = "        Account";
            this.btnMenuAccount.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bunifuTransition.SetDecoration(this.btnMenuAccount, BunifuAnimatorNS.DecorationType.None);
            this.btnMenuAccount.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuAccount.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuAccount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnMenuAccount.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMenuAccount.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMenuAccount.Iconimage")));
            this.btnMenuAccount.Iconimage_right = null;
            this.btnMenuAccount.Iconimage_right_Selected = null;
            this.btnMenuAccount.Iconimage_Selected = null;
            this.btnMenuAccount.IconMarginLeft = 20;
            this.btnMenuAccount.IconMarginRight = 0;
            this.btnMenuAccount.IconRightVisible = true;
            this.btnMenuAccount.IconRightZoom = 0D;
            this.btnMenuAccount.IconVisible = true;
            this.btnMenuAccount.IconZoom = 50D;
            this.btnMenuAccount.IsTab = false;
            this.btnMenuAccount.Location = new System.Drawing.Point(0, 51);
            this.btnMenuAccount.Name = "btnMenuAccount";
            this.btnMenuAccount.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuAccount.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnMenuAccount.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMenuAccount.selected = false;
            this.btnMenuAccount.Size = new System.Drawing.Size(201, 48);
            this.btnMenuAccount.TabIndex = 6;
            this.btnMenuAccount.Text = "        Account";
            this.btnMenuAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuAccount.Textcolor = System.Drawing.Color.White;
            this.btnMenuAccount.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuAccount.Click += new System.EventHandler(this.btnMenuAccount_Click);
            // 
            // btnMenuPrivacy
            // 
            this.btnMenuPrivacy.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuPrivacy.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMenuPrivacy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuPrivacy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMenuPrivacy.BorderRadius = 0;
            this.btnMenuPrivacy.ButtonText = "        Privacy";
            this.btnMenuPrivacy.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bunifuTransition.SetDecoration(this.btnMenuPrivacy, BunifuAnimatorNS.DecorationType.None);
            this.btnMenuPrivacy.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuPrivacy.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuPrivacy.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMenuPrivacy.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMenuPrivacy.Iconimage")));
            this.btnMenuPrivacy.Iconimage_right = null;
            this.btnMenuPrivacy.Iconimage_right_Selected = null;
            this.btnMenuPrivacy.Iconimage_Selected = null;
            this.btnMenuPrivacy.IconMarginLeft = 20;
            this.btnMenuPrivacy.IconMarginRight = 0;
            this.btnMenuPrivacy.IconRightVisible = true;
            this.btnMenuPrivacy.IconRightZoom = 0D;
            this.btnMenuPrivacy.IconVisible = true;
            this.btnMenuPrivacy.IconZoom = 50D;
            this.btnMenuPrivacy.IsTab = false;
            this.btnMenuPrivacy.Location = new System.Drawing.Point(0, 243);
            this.btnMenuPrivacy.Name = "btnMenuPrivacy";
            this.btnMenuPrivacy.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuPrivacy.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(79)))), ((int)(((byte)(79)))));
            this.btnMenuPrivacy.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMenuPrivacy.selected = false;
            this.btnMenuPrivacy.Size = new System.Drawing.Size(200, 48);
            this.btnMenuPrivacy.TabIndex = 10;
            this.btnMenuPrivacy.Text = "        Privacy";
            this.btnMenuPrivacy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuPrivacy.Textcolor = System.Drawing.Color.White;
            this.btnMenuPrivacy.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuPrivacy.Click += new System.EventHandler(this.btnMenuPrivacy_Click);
            // 
            // btnMenuConfiguration
            // 
            this.btnMenuConfiguration.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuConfiguration.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMenuConfiguration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuConfiguration.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMenuConfiguration.BorderRadius = 0;
            this.btnMenuConfiguration.ButtonText = "        Configuration";
            this.btnMenuConfiguration.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bunifuTransition.SetDecoration(this.btnMenuConfiguration, BunifuAnimatorNS.DecorationType.None);
            this.btnMenuConfiguration.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuConfiguration.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuConfiguration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnMenuConfiguration.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMenuConfiguration.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMenuConfiguration.Iconimage")));
            this.btnMenuConfiguration.Iconimage_right = null;
            this.btnMenuConfiguration.Iconimage_right_Selected = null;
            this.btnMenuConfiguration.Iconimage_Selected = null;
            this.btnMenuConfiguration.IconMarginLeft = 20;
            this.btnMenuConfiguration.IconMarginRight = 0;
            this.btnMenuConfiguration.IconRightVisible = true;
            this.btnMenuConfiguration.IconRightZoom = 0D;
            this.btnMenuConfiguration.IconVisible = true;
            this.btnMenuConfiguration.IconZoom = 50D;
            this.btnMenuConfiguration.IsTab = false;
            this.btnMenuConfiguration.Location = new System.Drawing.Point(0, 99);
            this.btnMenuConfiguration.Name = "btnMenuConfiguration";
            this.btnMenuConfiguration.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuConfiguration.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(79)))), ((int)(((byte)(79)))));
            this.btnMenuConfiguration.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMenuConfiguration.selected = false;
            this.btnMenuConfiguration.Size = new System.Drawing.Size(200, 48);
            this.btnMenuConfiguration.TabIndex = 7;
            this.btnMenuConfiguration.Text = "        Configuration";
            this.btnMenuConfiguration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuConfiguration.Textcolor = System.Drawing.Color.White;
            this.btnMenuConfiguration.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuConfiguration.Click += new System.EventHandler(this.btnMenuConfiguration_Click);
            // 
            // btnMenuCustomize
            // 
            this.btnMenuCustomize.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuCustomize.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMenuCustomize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuCustomize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMenuCustomize.BorderRadius = 0;
            this.btnMenuCustomize.ButtonText = "        Customize";
            this.btnMenuCustomize.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bunifuTransition.SetDecoration(this.btnMenuCustomize, BunifuAnimatorNS.DecorationType.None);
            this.btnMenuCustomize.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuCustomize.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuCustomize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnMenuCustomize.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMenuCustomize.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMenuCustomize.Iconimage")));
            this.btnMenuCustomize.Iconimage_right = null;
            this.btnMenuCustomize.Iconimage_right_Selected = null;
            this.btnMenuCustomize.Iconimage_Selected = null;
            this.btnMenuCustomize.IconMarginLeft = 20;
            this.btnMenuCustomize.IconMarginRight = 0;
            this.btnMenuCustomize.IconRightVisible = true;
            this.btnMenuCustomize.IconRightZoom = 0D;
            this.btnMenuCustomize.IconVisible = true;
            this.btnMenuCustomize.IconZoom = 50D;
            this.btnMenuCustomize.IsTab = false;
            this.btnMenuCustomize.Location = new System.Drawing.Point(0, 195);
            this.btnMenuCustomize.Name = "btnMenuCustomize";
            this.btnMenuCustomize.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuCustomize.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(79)))), ((int)(((byte)(79)))));
            this.btnMenuCustomize.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMenuCustomize.selected = false;
            this.btnMenuCustomize.Size = new System.Drawing.Size(200, 48);
            this.btnMenuCustomize.TabIndex = 9;
            this.btnMenuCustomize.Text = "        Customize";
            this.btnMenuCustomize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuCustomize.Textcolor = System.Drawing.Color.White;
            this.btnMenuCustomize.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuCustomize.Click += new System.EventHandler(this.btnMenuCustomize_Click);
            // 
            // btnMenuChangeKey
            // 
            this.btnMenuChangeKey.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuChangeKey.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMenuChangeKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuChangeKey.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMenuChangeKey.BorderRadius = 0;
            this.btnMenuChangeKey.ButtonText = "        Change Master                Password";
            this.btnMenuChangeKey.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bunifuTransition.SetDecoration(this.btnMenuChangeKey, BunifuAnimatorNS.DecorationType.None);
            this.btnMenuChangeKey.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
            this.btnMenuChangeKey.Font = new System.Drawing.Font("Lato", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuChangeKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnMenuChangeKey.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMenuChangeKey.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMenuChangeKey.Iconimage")));
            this.btnMenuChangeKey.Iconimage_right = null;
            this.btnMenuChangeKey.Iconimage_right_Selected = null;
            this.btnMenuChangeKey.Iconimage_Selected = null;
            this.btnMenuChangeKey.IconMarginLeft = 20;
            this.btnMenuChangeKey.IconMarginRight = 0;
            this.btnMenuChangeKey.IconRightVisible = true;
            this.btnMenuChangeKey.IconRightZoom = 0D;
            this.btnMenuChangeKey.IconVisible = true;
            this.btnMenuChangeKey.IconZoom = 50D;
            this.btnMenuChangeKey.IsTab = false;
            this.btnMenuChangeKey.Location = new System.Drawing.Point(0, 147);
            this.btnMenuChangeKey.Name = "btnMenuChangeKey";
            this.btnMenuChangeKey.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMenuChangeKey.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(79)))), ((int)(((byte)(79)))));
            this.btnMenuChangeKey.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMenuChangeKey.selected = false;
            this.btnMenuChangeKey.Size = new System.Drawing.Size(200, 48);
            this.btnMenuChangeKey.TabIndex = 8;
            this.btnMenuChangeKey.Text = "        Change Master                Password";
            this.btnMenuChangeKey.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenuChangeKey.Textcolor = System.Drawing.Color.White;
            this.btnMenuChangeKey.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuChangeKey.Click += new System.EventHandler(this.btnMenuChangeKey_Click);
            // 
            // pnLeftMenu
            // 
            this.pnLeftMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.pnLeftMenu.Controls.Add(this.pnLeftPnUserProfile);
            this.pnLeftMenu.Controls.Add(this.pnMenuContainer);
            this.bunifuTransition.SetDecoration(this.pnLeftMenu, BunifuAnimatorNS.DecorationType.None);
            this.pnLeftMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnLeftMenu.Location = new System.Drawing.Point(0, 0);
            this.pnLeftMenu.Name = "pnLeftMenu";
            this.pnLeftMenu.Size = new System.Drawing.Size(200, 696);
            this.pnLeftMenu.TabIndex = 0;
            // 
            // bnfDragControlAppName
            // 
            this.bnfDragControlAppName.Fixed = true;
            this.bnfDragControlAppName.Horizontal = true;
            this.bnfDragControlAppName.TargetControl = this.lbAppName;
            this.bnfDragControlAppName.Vertical = true;
            // 
            // bnfDragControlAppLogo
            // 
            this.bnfDragControlAppLogo.Fixed = true;
            this.bnfDragControlAppLogo.Horizontal = true;
            this.bnfDragControlAppLogo.TargetControl = null;
            this.bnfDragControlAppLogo.Vertical = true;
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStrip;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "CFML - Main";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // contextMenuStrip
            // 
            this.bunifuTransition.SetDecoration(this.contextMenuStrip, BunifuAnimatorNS.DecorationType.None);
            this.contextMenuStrip.Font = new System.Drawing.Font("Lato", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(153, 70);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Font = new System.Drawing.Font("Lato Heavy", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Lato", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 696);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.Controls.Add(this.pnLeftMenu);
            this.Controls.Add(this.pnUserControl);
            this.bunifuTransition.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CFML";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.pnTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).EndInit();
            this.pnBottom.ResumeLayout(false);
            this.pnLeftPnUserProfile.ResumeLayout(false);
            this.pnLeftPnUserProfile.PerformLayout();
            this.pnMenuContainer.ResumeLayout(false);
            this.pnLeftMenu.ResumeLayout(false);
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BunifuElipse bunifuElipse;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel pnTop;
        private System.Windows.Forms.Label lbVersionStatus;
        private System.Windows.Forms.Label lbCheckForUpdate;
        private BunifuDragControl bnfDragControlTitleBar;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition;
        private BunifuDragControl bnfDragControlAppName;
        private BunifuDragControl bnfDragControlAppLogo;
        private BunifuImageButton btnMinimize;
        private BunifuImageButton btnExit;
        private System.Windows.Forms.Panel pnUserControl;
        private System.Windows.Forms.Panel pnLeftMenu;
        private System.Windows.Forms.Panel pnLeftPnUserProfile;
        private System.Windows.Forms.Button btnAccountPic;
        private BunifuFlatButton btnLogout;
        private System.Windows.Forms.Label lbAppName;
        private BunifuCustomLabel lbWelcomeNote;
        private BunifuCustomLabel lbVIPStatus;
        private System.Windows.Forms.Panel pnMenuContainer;
        private BunifuFlatButton btnMenuAbout;
        private BunifuFlatButton btnMenuMain;
        private BunifuFlatButton btnMenuAccount;
        private BunifuFlatButton btnMenuPrivacy;
        private BunifuFlatButton btnMenuConfiguration;
        private BunifuFlatButton btnMenuCustomize;
        private BunifuFlatButton btnMenuChangeKey;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}