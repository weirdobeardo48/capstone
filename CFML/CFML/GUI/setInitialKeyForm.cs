﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using CFML.Properties;
using CFML.GUIDesign.CustomizeComponent;
using CFML.PornDetector;
using CFML.Capture;
using TensorFlow;
using System.Diagnostics;
using CFML.Common;

namespace CFML.GUIDesign
{
    public partial class setInitialKeyForm : Form
    {
        private static TFTensor[] tensor;
        Stopwatch stopwatch = new Stopwatch();
        public setInitialKeyForm()
        {
            InitializeComponent();
            inpNewKey.Select();

            this.Icon = Resources.AppIcon;
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private byte[] ImageToByte2(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                return stream.ToArray();
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(inpNewKey.Text) && !string.IsNullOrEmpty(inpCfKey.Text))
            {
                if (inpCfKey.Text.Equals(inpNewKey.Text))
                {
                    try
                    {
                        string encryptedMasterPassword = Common.Common.CreateMD5(inpCfKey.Text + Common.Const.APPEND_STRING);
                        if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team"))
                        {
                            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team");
                        }
                        File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat", encryptedMasterPassword);
                        Common.Common.writeRegistrySetInitialKeyForTheFirstTime();
                        this.Visible = false;
                        CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.SAVE_PASSWORD_AND_BENCHMARKING_MESSAGE, AlertType.Info);
                        cnb.Visible = true;

                        Bitmap bit = null;
                        pornDetector.initGraph();
                        int count = 0;
                        double FPSmeasure = 0;
                        while (count < 10)
                        {
                            bit = WindowSnap.GetActiveWindowImage();
                            stopwatch.Reset();
                            stopwatch.Start();
                            tensor = pornDetector.detectPorn(ImageToByte2(bit), pornDetector.Session);
                            stopwatch.Stop();
                            Console.WriteLine("Time To Detect: {0}", stopwatch.Elapsed.Duration());
                            string timeToDetect = stopwatch.Elapsed.Duration().ToString();
                            double timeDiff = (double)stopwatch.ElapsedMilliseconds / 1000;
                            Console.WriteLine("FPS           : {0}", 1 / timeDiff);
                            FPSmeasure = 1 / timeDiff;
                            count++;
                        }

                        if(FPSmeasure < 3)
                        {
                            writeConfigForLowEndPC();
                        } else  if(3 <= FPSmeasure && FPSmeasure <= 8)
                        {
                            writeConfigForMidEndPC();
                        } else
                        {
                            writeConfigForHighEndPC();
                        }
                        cnb.Visible = false;
                        cnb = new CustomNotificationBox(ComponentDescriptionText.BENCHMARKING_FINISHED_MESSAGE, AlertType.Info);
                        cnb.Visible = true;
                        loginForm LoginForm = new loginForm();
                        LoginForm.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
                else
                {
                    CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.WRONG_PASSWORD_MESSAGE, AlertType.Info);
                    cnb.Visible = true;
                    return;
                }
            }
            else
            {
                CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.FILL_THE_FORM_MESSAGE, AlertType.Info);
                cnb.Visible = true;
                return;
            }
        }

        private void writeConfigForLowEndPC()
        {
            /* Config.CensorSensitivity = 30;
            Config.CheckInactiveApplication = true;
            Config.DisabledRealTimeScanInterval = 5;
            Config.EnableMuteSound = false;
            Config.EnableObjectTracking = false;
            Config.EnableRealTimeMode = true;
            
            Config.CensorBgImageEnable = false;
            Config.CensorBgImageName = "";
            Config.CensorBgColor = "#000000";*/
            Configuration.Configuration cfg = new Configuration.Configuration();
            cfg.EnableRealTimeMode = false;
            cfg.EnableObjectTracking = false;
            cfg.CensorSensitivity = 30;
            cfg.DisabledRealTimeScanInterval = 10;
            cfg.CheckInactiveApplication = false;
            cfg.EnableMuteSound = false;
            cfg.CensorBgImageEnable = false;
            cfg.CensorBgImageName = "";
            cfg.CensorBgColor = "#000000";
            Common.ConfigurationHandle.writeConfigurationToFile(cfg);
        }

        private void writeConfigForMidEndPC()
        {
            Configuration.Configuration cfg = new Configuration.Configuration();
            cfg.EnableRealTimeMode = true;
            cfg.EnableObjectTracking = true;
            cfg.CensorSensitivity = 30;
            cfg.DisabledRealTimeScanInterval = 10;
            cfg.CheckInactiveApplication = false;
            cfg.EnableMuteSound = false;
            cfg.CensorBgImageEnable = false;
            cfg.CensorBgImageName = "";
            cfg.CensorBgColor = "#000000";
            Common.ConfigurationHandle.writeConfigurationToFile(cfg);
        }

        private void writeConfigForHighEndPC()
        {
            Configuration.Configuration cfg = new Configuration.Configuration();
            cfg.EnableRealTimeMode = true;
            cfg.EnableObjectTracking = false;
            cfg.CensorSensitivity = 30;
            cfg.DisabledRealTimeScanInterval = 10;
            cfg.CheckInactiveApplication = true;
            cfg.EnableMuteSound = false;
            cfg.CensorBgImageEnable = false;
            cfg.CensorBgImageName = "";
            cfg.CensorBgColor = "#000000";
            Common.ConfigurationHandle.writeConfigurationToFile(cfg);
        }

        private void inpCfKey_Enter(object sender, EventArgs e)
        {
            this.pnCfKeyBg.BackgroundImage = Resources.TextBoxBg_Active;
            this.inpCfKey.SelectAll();
        }

        private void inpCfKey_Leave(object sender, EventArgs e)
        {
            this.pnCfKeyBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void inpNewKey_Enter(object sender, EventArgs e)
        {
            this.pnCfKeyBg.BackgroundImage = Resources.TextBoxBg_Active;
            this.inpNewKey.SelectAll();
        }

        private void inpNewKey_Leave(object sender, EventArgs e)
        {
            this.pnCfKeyBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void pnCfKeyBg_Click(object sender, EventArgs e)
        {
            inpCfKey_Enter(null, null);
        }

        private void pnNewKeyBg_Click(object sender, EventArgs e)
        {
            inpNewKey_Enter(null, null);
        }

        private void setInitialKeyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void inpNewKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnConfirm_Click(null, null);
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
    }
}
