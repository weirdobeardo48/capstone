﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using CFML.Properties;
using CFML.Common;
using CFML.GUIDesign.CustomizeComponent;

namespace CFML.GUI.CustomizeComponent
{
    public partial class CloseApplicationForm : Form
    {
        public CloseApplicationForm()
        {
            InitializeComponent();
            inpMasterPassword.Select();
            this.Icon = Resources.AppIcon;
        }

        private void lbForgetMasterPassword_Click(object sender, EventArgs e)
        {
            this.inpMasterPassword.Text = "";
            GUIDesign.ForgetMasterPassword.forgetPasswordLoginForm forget = new GUIDesign.ForgetMasterPassword.forgetPasswordLoginForm();
            forget.ShowDialog();
        }

        private void btnExitBtn_Click(object sender, EventArgs e)
        {
            var close = false;

            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat"))
            {
                if (Common.Common.readRegistryIfInitialKeyIsSet() == true)
                {
                    //CustomNotificationBox cnbox = new CustomNotificationBox("Masterpassword is corrupted", AlertType.Info);
                    //cnbox.Visible = true;
                }

                CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_TITLE, AlertType.Info);
                cnb.Visible = true;
                return;
            }
            else
            {
                if (Common.Common.readRegistryIfInitialKeyIsSet() == false)
                {
                    CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_TITLE, AlertType.Info);
                    cnb.Visible = true;
                    return;
                }
                try
                {
                    StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat");
                    string line = sr.ReadLine();
                    sr.Close();
                    if (line != null)
                    {
                        if (line.Equals(Common.Common.CreateMD5(inpMasterPassword.Text + Common.Const.APPEND_STRING)))
                        {

                            close = true;
                        }
                        else
                        {
                            CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.WRONG_PASSWORD_MESSAGE, AlertType.Info);
                            cnb.Visible = true;
                            Console.WriteLine("Wrong password");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

            if (close)
            {
                Environment.Exit(1);
            }
            else
            {
                //CustomNotificationBox cnb = new CustomNotificationBox(
                //    "Error Notify!",
                //    AlertType.Info);
                //cnb.Visible = true;
            }
            
        }

        private void inpMasterPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnExitBtn_Click(null, null);
            }
        }

        private void inpMasterPassword_Enter(object sender, EventArgs e)
        {
            this.inpMasterPassword.SelectAll();
            this.pnMasterPasswordBg.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void inpMasterPassword_Leave(object sender, EventArgs e)
        {
            this.pnMasterPasswordBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void pnMasterPasswordBg_Click(object sender, EventArgs e)
        {
            inpMasterPassword_Enter(null, null);
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }
    }
}
