﻿using CFML.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CFML.GUIDesign.CustomizeComponent
{
    public partial class CustomNotificationBox : Form
    {

        public CustomNotificationBox(string message, AlertType type)
        {
            InitializeComponent();
            this.lbMessage.Text = message;
            switch (type)
            {
                case AlertType.Success:
                    {
                        //change icon
                        break;
                    }
                case AlertType.Info:
                    {
                        //change icon
                        break;
                    }
                case AlertType.Error:
                    {
                        //change icon
                        break;
                    }
                case AlertType.Warining:
                    {
                        //change icon
                        break;
                    }
                case AlertType.Quit:
                    {
                        //change icon
                        break;
                    }
            }
            this.Icon = Resources.AppIcon;
        }

        private void CustomNotificationBox_Load(object sender, EventArgs e)
        {
            this.Top = Screen.PrimaryScreen.Bounds.Height - this.Height - 60;
            this.Left = Screen.PrimaryScreen.Bounds.Width - this.Width - 20;
            Timer myTimer = new Timer();
            myTimer.Tick += new EventHandler(timerDismiss_Tick);

            // Sets the timer interval to 5 seconds.
            myTimer.Interval = 5000;
            myTimer.Start();
        }

        private void timerDismiss_Tick(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void clickDismiss_Tick(object sender, MouseEventArgs e)
        {
            this.Dispose();
        }
        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
    }

    public enum AlertType
    {
        Warining, Error, Success, Info, Quit
    }
}
