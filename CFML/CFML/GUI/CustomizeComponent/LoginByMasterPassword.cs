﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using CFML.Properties;
using CFML.Common;
using CFML.Capture;

namespace CFML.GUIDesign.CustomizeComponent
{
    public partial class LoginByMasterPassword : Form
    {
        private loginForm LoginForm;
        private mainForm MainForm;
        private bool allowToClose;

        public LoginByMasterPassword()
        {
            InitializeComponent();
            Initiate();
        }

        private void Initiate()
        {
            #region Tray Icon
            this.ShowInTaskbar = true;
            this.notifyIcon.Icon = Resources.AppIcon;
            this.notifyIcon.Visible = false;
            #endregion
                        
            allowToClose = false;

            inpMasterPassword.Select();
            this.Icon = Resources.AppIcon;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }


        public LoginByMasterPassword( mainForm MainForm)
        {
            InitializeComponent();
            this.MainForm = MainForm;
            Initiate();
        }
        public LoginByMasterPassword(loginForm LoginForm)
        {
            InitializeComponent();
            this.LoginForm = LoginForm;
            Initiate();
        }

        public LoginByMasterPassword(mainForm MainForm, loginForm LoginForm)
        {
            InitializeComponent();
            this.MainForm = MainForm;
            this.LoginForm = LoginForm;
            Initiate();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            this.notifyIcon.Visible = true;
            this.allowToClose = false;
            this.inpMasterPassword.Text = String.Empty;
            this.Hide();
        }

        private void btnLoginBtn_Click(object sender, EventArgs e)
        {
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat"))
            {
                if (Common.Common.readRegistryIfInitialKeyIsSet() == true)
                {
                    //CustomNotificationBox cnbox = new CustomNotificationBox("Masterpassword is corrupted", AlertType.Info);
                    //cnbox.Visible = true;
                }

                CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_TITLE, AlertType.Info);
                cnb.Visible = true;
                return;
            }
            else
            {
                if (Common.Common.readRegistryIfInitialKeyIsSet() == false)
                {
                    CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_TITLE, AlertType.Info);
                    cnb.Visible = true;
                    return;
                }
                try
                {
                    StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat");
                    string line = sr.ReadLine();
                    sr.Close();
                    if (line != null)
                    {
                        if (line.Equals(Common.Common.CreateMD5(inpMasterPassword.Text + "5154bfd9725040c54f5f69c70c61bdc3")))
                        {
                            
                            this.Hide();
                            this.notifyIcon.Visible = false;
                            this.inpMasterPassword.Clear();
                            this.allowToClose = true;
                           
                            if (MainForm  == null) { 
                                if (LoginForm == null)
                                {
                                    LoginForm = new loginForm();
                                    LoginForm.Visible = true;
                                    LoginForm.RevolkeForm();
                                }
                                else
                                {
                                    LoginForm.RevolkeForm();
                                }
                                
                            }
                            else
                            {
                                MainForm.RevolkeForm();
                            } 
                        }
                        else
                        {
                          CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.WRONG_PASSWORD_MESSAGE, AlertType.Info);
                          cnb.Visible = true;
                          Console.WriteLine("Wrong password");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.ShowInTaskbar = true;
            this.notifyIcon.Visible = false;
            //this.WindowState = FormWindowState.Normal;
            this.allowToClose = false;
            WindowSnap.SetForegroundWindow(this.Handle);
        }

        private void LoginByMasterPassword_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if not allow to close form > e.Cancel = true;
            if (!this.allowToClose)
            {
                e.Cancel = true;
            }
        }

        private void inpMasterPassword_Enter(object sender, EventArgs e)
        {
            this.inpMasterPassword.SelectAll();
            this.pnMasterPasswordBg.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void pnMasterPasswordBg_Click(object sender, EventArgs e)
        {
            inpMasterPassword_Enter(null, null);
        }

        private void inpMasterPassword_Leave(object sender, EventArgs e)
        {
            this.pnMasterPasswordBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void lbForgetMasterPassword_Click(object sender, EventArgs e)
        {
            this.inpMasterPassword.Text = "";
            //this.Visible = false;
            ForgetMasterPassword.forgetPasswordLoginForm forget = new ForgetMasterPassword.forgetPasswordLoginForm();
            //forget.Visible = true;
            forget.ShowDialog();
        }

        private void inpMasterPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLoginBtn_Click(null, null);
            }
        }
    }
}