﻿namespace CFML.GUIDesign.CustomizeComponent
{
    partial class CustomNotificationBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomNotificationBox));
            this.bunifuElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.lbMessage = new System.Windows.Forms.Label();
            this.picInfoIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picInfoIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse
            // 
            this.bunifuElipse.ElipseRadius = 5;
            this.bunifuElipse.TargetControl = this;
            // 
            // lbMessage
            // 
            this.lbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMessage.Font = new System.Drawing.Font("Lato", 9.75F);
            this.lbMessage.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbMessage.Location = new System.Drawing.Point(98, 12);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(224, 62);
            this.lbMessage.TabIndex = 1;
            this.lbMessage.Text = "Notify Message";
            this.lbMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbMessage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.clickDismiss_Tick);
            // 
            // picInfoIcon
            // 
            this.picInfoIcon.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picInfoIcon.BackgroundImage")));
            this.picInfoIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picInfoIcon.Location = new System.Drawing.Point(21, 20);
            this.picInfoIcon.Name = "picInfoIcon";
            this.picInfoIcon.Size = new System.Drawing.Size(48, 46);
            this.picInfoIcon.TabIndex = 0;
            this.picInfoIcon.TabStop = false;
            this.picInfoIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.clickDismiss_Tick);
            // 
            // CustomNotificationBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.ClientSize = new System.Drawing.Size(334, 92);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.picInfoIcon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CustomNotificationBox";
            this.Opacity = 0.9D;
            this.Text = "CustomConfirmation";
            this.Load += new System.EventHandler(this.CustomNotificationBox_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.clickDismiss_Tick);
            ((System.ComponentModel.ISupportInitialize)(this.picInfoIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.PictureBox picInfoIcon;
    }
}