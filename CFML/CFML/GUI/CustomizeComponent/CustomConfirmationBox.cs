﻿using CFML.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CFML.GUIDesign.CustomizeComponent
{
    public partial class CustomConfirmationBox : Form
    {
        static DialogResult result;

        public CustomConfirmationBox()
        {
            InitializeComponent();
            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;
            result = DialogResult.Cancel;
            this.Icon = Resources.AppIcon;
        }

        public CustomConfirmationBox(string message, string title)
        {
            InitializeComponent();
            this.lbTitle.Text = title;
            this.lbMessage.Text = message;
            this.Icon = Resources.AppIcon;
        }

        public CustomConfirmationBox(string message, string title, AlertType type)
        {
            InitializeComponent();
            this.lbTitle.Text = title;
            this.lbMessage.Text = message;
            updateFormByAlertType(type);
            this.Icon = Resources.AppIcon;
        }

        public IntPtr getHandle()
        {
            return this.Handle;
        }

        private void updateFormByAlertType(AlertType type)
        {
            switch (type)
            {
                case AlertType.Success:
                    {
                        //change icon
                        break;
                    }
                case AlertType.Info:
                    {
                        //change icon
                        break;
                    }
                case AlertType.Error:
                    {
                        //change icon
                        break;
                    }
                case AlertType.Warining:
                    {
                        //change icon
                        break;
                    }
                case AlertType.Quit:
                    {
                        btnOK.Text = "Exit";
                        btnCancel.Text = "Just minimize";
                        break;
                    }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            result = btnOK.DialogResult;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            result = btnCancel.DialogResult;
        }

        private void CustomConfirmationBox_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = true;
        }
        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void CustomConfirmationBox_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.Dispose();
        }
    }
}
