﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.GUIDesign.CustomizeClass
{
    class CustomizeTextBox : System.Windows.Forms.TextBox
    {
        System.Drawing.Color DefaultColor;

        public string PlaceHolderText { get; set; }
        private string textboxValue;

        public CustomizeTextBox(string placeholdertext)
        {
            // get default color of text
            DefaultColor = this.ForeColor;
            // Add event handler for when the control gets focus
            this.GotFocus += (object sender, EventArgs e) =>
            {
                this.Text = String.Empty;
                this.ForeColor = DefaultColor;
                this.PasswordChar = '•';
                this.textboxValue = String.Empty;
            };

            // add event handling when focus is lost
            this.LostFocus += (Object sender, EventArgs e) => {
                if (String.IsNullOrEmpty(this.Text) || this.Text == PlaceHolderText)
                {
                    this.ForeColor = System.Drawing.Color.Gray;
                    this.Text = PlaceHolderText;
                    this.PasswordChar = ControlChars.NullChar;
                    this.textboxValue = String.Empty;
                }
                else
                {
                    this.ForeColor = DefaultColor;
                    this.PasswordChar = '•';
                    this.textboxValue = this.Text;
                }
                //Console.WriteLine(TextBoxValue());
            };



            if (!string.IsNullOrEmpty(placeholdertext))
            {
                // change style   
                this.ForeColor = System.Drawing.Color.Gray;
                // Add text
                PlaceHolderText = placeholdertext;
                this.Text = placeholdertext;
                this.PasswordChar = ControlChars.NullChar;
            }
        }

        public string TextBoxValue()
        {
            return textboxValue;
        }
    }
}
