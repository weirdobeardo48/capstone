﻿namespace CFML.GUI.CustomizeComponent
{
    partial class CloseApplicationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CloseApplicationForm));
            this.bunifuElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pnMasterPasswordBg = new System.Windows.Forms.Panel();
            this.inpMasterPassword = new System.Windows.Forms.TextBox();
            this.lbExitApplication = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnExitBtn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnQuit = new System.Windows.Forms.PictureBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.bunifuDragControl = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.lbForgetMasterPassword = new System.Windows.Forms.Label();
            this.pnMasterPasswordBg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse
            // 
            this.bunifuElipse.ElipseRadius = 5;
            this.bunifuElipse.TargetControl = this;
            // 
            // pnMasterPasswordBg
            // 
            this.pnMasterPasswordBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnMasterPasswordBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnMasterPasswordBg.Controls.Add(this.inpMasterPassword);
            this.pnMasterPasswordBg.Location = new System.Drawing.Point(66, 96);
            this.pnMasterPasswordBg.Name = "pnMasterPasswordBg";
            this.pnMasterPasswordBg.Size = new System.Drawing.Size(298, 45);
            this.pnMasterPasswordBg.TabIndex = 16;
            this.pnMasterPasswordBg.Click += new System.EventHandler(this.pnMasterPasswordBg_Click);
            // 
            // inpMasterPassword
            // 
            this.inpMasterPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpMasterPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpMasterPassword.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpMasterPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpMasterPassword.Location = new System.Drawing.Point(14, 12);
            this.inpMasterPassword.Name = "inpMasterPassword";
            this.inpMasterPassword.PasswordChar = '•';
            this.inpMasterPassword.Size = new System.Drawing.Size(268, 16);
            this.inpMasterPassword.TabIndex = 1;
            this.inpMasterPassword.Enter += new System.EventHandler(this.inpMasterPassword_Enter);
            this.inpMasterPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inpMasterPassword_KeyDown);
            this.inpMasterPassword.Leave += new System.EventHandler(this.inpMasterPassword_Leave);
            // 
            // lbExitApplication
            // 
            this.lbExitApplication.AutoSize = true;
            this.lbExitApplication.Font = new System.Drawing.Font("Lato", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbExitApplication.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbExitApplication.Location = new System.Drawing.Point(100, 59);
            this.lbExitApplication.Name = "lbExitApplication";
            this.lbExitApplication.Size = new System.Drawing.Size(233, 18);
            this.lbExitApplication.TabIndex = 15;
            this.lbExitApplication.Text = "Enter master password to exit";
            // 
            // btnExitBtn
            // 
            this.btnExitBtn.ActiveBorderThickness = 1;
            this.btnExitBtn.ActiveCornerRadius = 20;
            this.btnExitBtn.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnExitBtn.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnExitBtn.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnExitBtn.BackColor = System.Drawing.SystemColors.Control;
            this.btnExitBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExitBtn.BackgroundImage")));
            this.btnExitBtn.ButtonText = "Exit";
            this.btnExitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExitBtn.Font = new System.Drawing.Font("Lato", 12F);
            this.btnExitBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnExitBtn.IdleBorderThickness = 1;
            this.btnExitBtn.IdleCornerRadius = 20;
            this.btnExitBtn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnExitBtn.IdleForecolor = System.Drawing.Color.White;
            this.btnExitBtn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnExitBtn.Location = new System.Drawing.Point(155, 152);
            this.btnExitBtn.Margin = new System.Windows.Forms.Padding(5);
            this.btnExitBtn.Name = "btnExitBtn";
            this.btnExitBtn.Size = new System.Drawing.Size(118, 43);
            this.btnExitBtn.TabIndex = 14;
            this.btnExitBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnExitBtn.Click += new System.EventHandler(this.btnExitBtn_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuit.BackgroundImage")));
            this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQuit.Location = new System.Drawing.Point(365, 12);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(56, 25);
            this.btnQuit.TabIndex = 13;
            this.btnQuit.TabStop = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "CFML - Master Pass";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // bunifuDragControl
            // 
            this.bunifuDragControl.Fixed = true;
            this.bunifuDragControl.Horizontal = true;
            this.bunifuDragControl.TargetControl = null;
            this.bunifuDragControl.Vertical = true;
            // 
            // lbForgetMasterPassword
            // 
            this.lbForgetMasterPassword.AutoSize = true;
            this.lbForgetMasterPassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbForgetMasterPassword.Font = new System.Drawing.Font("Lato", 9.75F, System.Drawing.FontStyle.Underline);
            this.lbForgetMasterPassword.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lbForgetMasterPassword.Location = new System.Drawing.Point(140, 209);
            this.lbForgetMasterPassword.Name = "lbForgetMasterPassword";
            this.lbForgetMasterPassword.Size = new System.Drawing.Size(147, 16);
            this.lbForgetMasterPassword.TabIndex = 17;
            this.lbForgetMasterPassword.Text = "Forget Master Password";
            this.lbForgetMasterPassword.Click += new System.EventHandler(this.lbForgetMasterPassword_Click);
            // 
            // CloseApplicationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 250);
            this.Controls.Add(this.pnMasterPasswordBg);
            this.Controls.Add(this.lbExitApplication);
            this.Controls.Add(this.btnExitBtn);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.lbForgetMasterPassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CloseApplicationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CloseApplicationForm";
            this.pnMasterPasswordBg.ResumeLayout(false);
            this.pnMasterPasswordBg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel pnMasterPasswordBg;
        private System.Windows.Forms.TextBox inpMasterPassword;
        private Bunifu.Framework.UI.BunifuCustomLabel lbExitApplication;
        private Bunifu.Framework.UI.BunifuThinButton2 btnExitBtn;
        private System.Windows.Forms.PictureBox btnQuit;
        private System.Windows.Forms.Label lbForgetMasterPassword;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl;
    }
}