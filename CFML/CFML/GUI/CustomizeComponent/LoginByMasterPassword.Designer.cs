﻿namespace CFML.GUIDesign.CustomizeComponent
{
    partial class LoginByMasterPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginByMasterPassword));
            this.bunifuElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btnQuit = new System.Windows.Forms.PictureBox();
            this.btnLoginBtn = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lbLoginByMasterPassword = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pnMasterPasswordBg = new System.Windows.Forms.Panel();
            this.inpMasterPassword = new System.Windows.Forms.TextBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.bunifuDragControl = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.lbForgetMasterPassword = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).BeginInit();
            this.pnMasterPasswordBg.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse
            // 
            this.bunifuElipse.ElipseRadius = 5;
            this.bunifuElipse.TargetControl = this;
            // 
            // btnQuit
            // 
            this.btnQuit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuit.BackgroundImage")));
            this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQuit.Location = new System.Drawing.Point(362, 12);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(56, 25);
            this.btnQuit.TabIndex = 7;
            this.btnQuit.TabStop = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // btnLoginBtn
            // 
            this.btnLoginBtn.ActiveBorderThickness = 1;
            this.btnLoginBtn.ActiveCornerRadius = 20;
            this.btnLoginBtn.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLoginBtn.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnLoginBtn.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLoginBtn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoginBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLoginBtn.BackgroundImage")));
            this.btnLoginBtn.ButtonText = "Login";
            this.btnLoginBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLoginBtn.Font = new System.Drawing.Font("Lato", 12F);
            this.btnLoginBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnLoginBtn.IdleBorderThickness = 1;
            this.btnLoginBtn.IdleCornerRadius = 20;
            this.btnLoginBtn.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnLoginBtn.IdleForecolor = System.Drawing.Color.White;
            this.btnLoginBtn.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnLoginBtn.Location = new System.Drawing.Point(152, 152);
            this.btnLoginBtn.Margin = new System.Windows.Forms.Padding(5);
            this.btnLoginBtn.Name = "btnLoginBtn";
            this.btnLoginBtn.Size = new System.Drawing.Size(118, 43);
            this.btnLoginBtn.TabIndex = 9;
            this.btnLoginBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLoginBtn.Click += new System.EventHandler(this.btnLoginBtn_Click);
            // 
            // lbLoginByMasterPassword
            // 
            this.lbLoginByMasterPassword.AutoSize = true;
            this.lbLoginByMasterPassword.Font = new System.Drawing.Font("Lato", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbLoginByMasterPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbLoginByMasterPassword.Location = new System.Drawing.Point(91, 59);
            this.lbLoginByMasterPassword.Name = "lbLoginByMasterPassword";
            this.lbLoginByMasterPassword.Size = new System.Drawing.Size(249, 18);
            this.lbLoginByMasterPassword.TabIndex = 10;
            this.lbLoginByMasterPassword.Text = "Enter Master Password to Login";
            // 
            // pnMasterPasswordBg
            // 
            this.pnMasterPasswordBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnMasterPasswordBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnMasterPasswordBg.Controls.Add(this.inpMasterPassword);
            this.pnMasterPasswordBg.Location = new System.Drawing.Point(63, 96);
            this.pnMasterPasswordBg.Name = "pnMasterPasswordBg";
            this.pnMasterPasswordBg.Size = new System.Drawing.Size(298, 45);
            this.pnMasterPasswordBg.TabIndex = 11;
            this.pnMasterPasswordBg.Click += new System.EventHandler(this.pnMasterPasswordBg_Click);
            // 
            // inpMasterPassword
            // 
            this.inpMasterPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpMasterPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpMasterPassword.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpMasterPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpMasterPassword.Location = new System.Drawing.Point(14, 12);
            this.inpMasterPassword.Name = "inpMasterPassword";
            this.inpMasterPassword.PasswordChar = '•';
            this.inpMasterPassword.Size = new System.Drawing.Size(268, 16);
            this.inpMasterPassword.TabIndex = 1;
            this.inpMasterPassword.Enter += new System.EventHandler(this.inpMasterPassword_Enter);
            this.inpMasterPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inpMasterPassword_KeyDown);
            this.inpMasterPassword.Leave += new System.EventHandler(this.inpMasterPassword_Leave);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "CFML - Master Pass";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // bunifuDragControl
            // 
            this.bunifuDragControl.Fixed = true;
            this.bunifuDragControl.Horizontal = true;
            this.bunifuDragControl.TargetControl = this;
            this.bunifuDragControl.Vertical = true;
            // 
            // lbForgetMasterPassword
            // 
            this.lbForgetMasterPassword.AutoSize = true;
            this.lbForgetMasterPassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbForgetMasterPassword.Font = new System.Drawing.Font("Lato", 9.75F, System.Drawing.FontStyle.Underline);
            this.lbForgetMasterPassword.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lbForgetMasterPassword.Location = new System.Drawing.Point(137, 209);
            this.lbForgetMasterPassword.Name = "lbForgetMasterPassword";
            this.lbForgetMasterPassword.Size = new System.Drawing.Size(147, 16);
            this.lbForgetMasterPassword.TabIndex = 12;
            this.lbForgetMasterPassword.Text = "Forget Master Password";
            this.lbForgetMasterPassword.Click += new System.EventHandler(this.lbForgetMasterPassword_Click);
            // 
            // LoginByMasterPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 250);
            this.Controls.Add(this.lbForgetMasterPassword);
            this.Controls.Add(this.pnMasterPasswordBg);
            this.Controls.Add(this.lbLoginByMasterPassword);
            this.Controls.Add(this.btnLoginBtn);
            this.Controls.Add(this.btnQuit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginByMasterPassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginByMasterPassword_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).EndInit();
            this.pnMasterPasswordBg.ResumeLayout(false);
            this.pnMasterPasswordBg.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse;
        private System.Windows.Forms.PictureBox btnQuit;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLoginBtn;
        private Bunifu.Framework.UI.BunifuCustomLabel lbLoginByMasterPassword;
        private System.Windows.Forms.Panel pnMasterPasswordBg;
        private System.Windows.Forms.TextBox inpMasterPassword;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl;
        private System.Windows.Forms.Label lbForgetMasterPassword;
    }
}