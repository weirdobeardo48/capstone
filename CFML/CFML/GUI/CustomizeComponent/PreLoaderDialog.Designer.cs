﻿namespace CFML.GUIDesign.CustomizeComponent
{
    partial class PreLoaderDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreLoaderDialog));
            this.bunifuElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuDragControl = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.picLoadingCircle = new System.Windows.Forms.PictureBox();
            this.bunifuFormFadeTransition = new Bunifu.Framework.UI.BunifuFormFadeTransition(this.components);
            this.timerOff = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picLoadingCircle)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse
            // 
            this.bunifuElipse.ElipseRadius = 5;
            this.bunifuElipse.TargetControl = this;
            // 
            // bunifuDragControl
            // 
            this.bunifuDragControl.Fixed = true;
            this.bunifuDragControl.Horizontal = true;
            this.bunifuDragControl.TargetControl = this.picLoadingCircle;
            this.bunifuDragControl.Vertical = true;
            // 
            // picLoadingCircle
            // 
            this.picLoadingCircle.Cursor = System.Windows.Forms.Cursors.Default;
            this.picLoadingCircle.Image = global::CFML.Properties.Resources.LoadingCircle;
            this.picLoadingCircle.Location = new System.Drawing.Point(-37, -12);
            this.picLoadingCircle.Name = "picLoadingCircle";
            this.picLoadingCircle.Size = new System.Drawing.Size(246, 183);
            this.picLoadingCircle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoadingCircle.TabIndex = 0;
            this.picLoadingCircle.TabStop = false;
            // 
            // bunifuFormFadeTransition
            // 
            this.bunifuFormFadeTransition.Delay = 1;
            // 
            // timerOff
            // 
            this.timerOff.Interval = 3000;
            this.timerOff.Tick += new System.EventHandler(this.timerOff_Tick);
            // 
            // PreLoaderDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(173, 161);
            this.Controls.Add(this.picLoadingCircle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PreLoaderDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loading";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PreLoaderDialog_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PreLoaderDialog_FormClosed);
            this.Load += new System.EventHandler(this.PreLoaderDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLoadingCircle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl;
        private Bunifu.Framework.UI.BunifuFormFadeTransition bunifuFormFadeTransition;
        private System.Windows.Forms.PictureBox picLoadingCircle;
        private System.Windows.Forms.Timer timerOff;
    }
}