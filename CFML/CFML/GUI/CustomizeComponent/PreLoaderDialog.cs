﻿using CFML.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CFML.GUIDesign.CustomizeComponent
{
    public partial class PreLoaderDialog : Form
    {
        private PreLoaderDialog _loader;

        public PreLoaderDialog()
        {
            this.Icon = Resources.AppIcon;
            InitializeComponent();
            _loader = this;
            timerOff.Start();
        }

        private void PreLoaderDialog_Load(object sender, EventArgs e)
        {
            bunifuFormFadeTransition.ShowAsyc(_loader);
        }

        private void PreLoaderDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            bunifuFormFadeTransition.ShowAsyc(_loader);
        }

        public void Exit()
        {
            
            _loader.Close();
        }

        private void timerOff_Tick(object sender, EventArgs e)
        {
            picLoadingCircle.Enabled = false;
            timerOff.Stop();
            this.Exit();
        }

        private void PreLoaderDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.Dispose();
        }
    }
}
