﻿using CFML.Common;
using CFML.GUIDesign.CustomizeComponent;
using CFML.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CFML.GUIDesign.ForgetMasterPassword
{
    public partial class resetMasterPasswordForm : Form
    {
        public resetMasterPasswordForm()
        {
            InitializeComponent();
            inpPassword.Select();
            this.Icon = Resources.AppIcon;
        }

        private void btnResetPassword_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(inpPassword.Text) && !string.IsNullOrEmpty(inpReInputPassword.Text))
            {
                if (inpReInputPassword.Text.Equals(inpPassword.Text))
                {
                    try
                    {
                        string encryptedMasterPassword = Common.Common.CreateMD5(inpReInputPassword.Text + "5154bfd9725040c54f5f69c70c61bdc3");
                        if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team"))
                        {
                            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team");
                        }
                        File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat", encryptedMasterPassword);
                        Common.Common.writeRegistrySetInitialKeyForTheFirstTime();
                        this.Visible = false;
                        CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.MASTER_PASSWORD_RESET_SUCCESSFUL_MESSAGE, AlertType.Info);
                        cnb.Visible = true;
                        
                        //loginForm LoginForm = new loginForm();
                        //LoginForm.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
                else
                {
                    CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.WRONG_PASSWORD_MESSAGE, AlertType.Info);
                    cnb.Visible = true;
                    return;
                }
            }
            else
            {
                CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.FILL_THE_FORM_MESSAGE, AlertType.Info);
                cnb.Visible = true;               
                return;
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void resetMasterPasswordForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void pnRePasswordBg_Click(object sender, EventArgs e)
        {
            inpReInputPassword_Enter(null, null);
        }

        private void inpReInputPassword_Enter(object sender, EventArgs e)
        {
            this.inpReInputPassword.SelectAll();
            this.pnRePasswordBg.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void inpReInputPassword_Leave(object sender, EventArgs e)
        {
            this.pnRePasswordBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void inpPassword_Enter(object sender, EventArgs e)
        {
            this.inpPassword.SelectAll();
            this.inpPassword.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void inpPassword_Leave(object sender, EventArgs e)
        {
            this.inpPassword.BackgroundImage = Resources.TextBoxBg;
        }

        private void pnPassword_Click(object sender, EventArgs e)
        {
            inpPassword_Enter(null, null);
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
