﻿using CFML.Common;
using CFML.GUIDesign.CustomizeComponent;
using CFML.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CFML.GUIDesign.ForgetMasterPassword
{
    public partial class forgetPasswordLoginForm : Form
    {
        public forgetPasswordLoginForm()
        {
            InitializeComponent();
            inpUsername.Select();
            this.Icon = Resources.AppIcon;
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private void btnLogin_Click_1(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(inpUsername.Text) && !string.IsNullOrEmpty(inpPassword.Text))
            {
                loginDetail login = new loginDetail();
                login.Username = inpUsername.Text.Trim();
                if (Common.Common.checkValidLastUserName(inpUsername.Text) == -1)
                {
                    CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.LAST_LOGGED_IN_USER_DOES_NOT_MATCH_MESSAGE, AlertType.Info);
                    cnb.Visible = true;
                    return;
                }
                else
                {
                    login.Password = inpPassword.Text;
                    if (Common.Common.checkLoginInfo(login).result)
                    {
                        this.Visible = false;
                        resetMasterPasswordForm reset = new resetMasterPasswordForm();
                        reset.ShowDialog();
                    }
                    else
                    {
                        CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.WRONG_USERNAME_PASSWORD_MESSAGE, AlertType.Info);
                        cnb.Visible = true;
                    }
                }
            }
            else
            {
                CustomNotificationBox cnb = new CustomNotificationBox(ComponentDescriptionText.FILL_THE_FORM_MESSAGE, AlertType.Info);
                cnb.Visible = true;

            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                //Drop Shadow
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void forgetPasswordLoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void inpUsername_Leave(object sender, EventArgs e)
        {
            this.pnUsernameBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void inpUsername_Enter(object sender, EventArgs e)
        {
            this.inpUsername.SelectAll();
            this.pnUsernameBg.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void pnUsernameBg_Click(object sender, EventArgs e)
        {
            inpUsername_Enter(null, null);
        }

        private void inpPassword_Leave(object sender, EventArgs e)
        {
            this.pnPasswordBg.BackgroundImage = Resources.TextBoxBg;
        }

        private void inpPassword_Enter(object sender, EventArgs e)
        {
            this.inpPassword.SelectAll();
            this.pnPasswordBg.BackgroundImage = Resources.TextBoxBg_Active;
        }

        private void pnPasswordBg_Click(object sender, EventArgs e)
        {
            inpPassword_Enter(null, null);
        }

        private void btnQuit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
