﻿namespace CFML.GUIDesign.ForgetMasterPassword
{
    partial class resetMasterPasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(resetMasterPasswordForm));
            this.bunifuElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btnResetPassword = new Bunifu.Framework.UI.BunifuThinButton2();
            this.pnPassword = new System.Windows.Forms.Panel();
            this.inpPassword = new System.Windows.Forms.TextBox();
            this.pnRePasswordBg = new System.Windows.Forms.Panel();
            this.inpReInputPassword = new System.Windows.Forms.TextBox();
            this.btnQuit = new System.Windows.Forms.PictureBox();
            this.lbRePassword = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.bunifuDragControl = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.lbResetMasterPassword = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pnPassword.SuspendLayout();
            this.pnRePasswordBg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse
            // 
            this.bunifuElipse.ElipseRadius = 5;
            this.bunifuElipse.TargetControl = this;
            // 
            // btnResetPassword
            // 
            this.btnResetPassword.ActiveBorderThickness = 1;
            this.btnResetPassword.ActiveCornerRadius = 20;
            this.btnResetPassword.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnResetPassword.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnResetPassword.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnResetPassword.BackColor = System.Drawing.SystemColors.Control;
            this.btnResetPassword.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnResetPassword.BackgroundImage")));
            this.btnResetPassword.ButtonText = "Reset Password";
            this.btnResetPassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnResetPassword.Font = new System.Drawing.Font("Lato", 11.25F);
            this.btnResetPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnResetPassword.IdleBorderThickness = 1;
            this.btnResetPassword.IdleCornerRadius = 20;
            this.btnResetPassword.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnResetPassword.IdleForecolor = System.Drawing.Color.White;
            this.btnResetPassword.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnResetPassword.Location = new System.Drawing.Point(182, 205);
            this.btnResetPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnResetPassword.Name = "btnResetPassword";
            this.btnResetPassword.Size = new System.Drawing.Size(151, 47);
            this.btnResetPassword.TabIndex = 20;
            this.btnResetPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnResetPassword.Click += new System.EventHandler(this.btnResetPassword_Click);
            // 
            // pnPassword
            // 
            this.pnPassword.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnPassword.Controls.Add(this.inpPassword);
            this.pnPassword.Location = new System.Drawing.Point(182, 87);
            this.pnPassword.Name = "pnPassword";
            this.pnPassword.Size = new System.Drawing.Size(250, 45);
            this.pnPassword.TabIndex = 19;
            this.pnPassword.Click += new System.EventHandler(this.pnPassword_Click);
            // 
            // inpPassword
            // 
            this.inpPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpPassword.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpPassword.Location = new System.Drawing.Point(8, 12);
            this.inpPassword.Name = "inpPassword";
            this.inpPassword.PasswordChar = '•';
            this.inpPassword.Size = new System.Drawing.Size(231, 16);
            this.inpPassword.TabIndex = 1;
            this.inpPassword.Enter += new System.EventHandler(this.inpPassword_Enter);
            this.inpPassword.Leave += new System.EventHandler(this.inpPassword_Leave);
            // 
            // pnRePasswordBg
            // 
            this.pnRePasswordBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnRePasswordBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnRePasswordBg.Controls.Add(this.inpReInputPassword);
            this.pnRePasswordBg.Location = new System.Drawing.Point(182, 142);
            this.pnRePasswordBg.Name = "pnRePasswordBg";
            this.pnRePasswordBg.Size = new System.Drawing.Size(250, 45);
            this.pnRePasswordBg.TabIndex = 21;
            this.pnRePasswordBg.Click += new System.EventHandler(this.pnRePasswordBg_Click);
            // 
            // inpReInputPassword
            // 
            this.inpReInputPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpReInputPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpReInputPassword.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpReInputPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.inpReInputPassword.Location = new System.Drawing.Point(8, 12);
            this.inpReInputPassword.Name = "inpReInputPassword";
            this.inpReInputPassword.PasswordChar = '•';
            this.inpReInputPassword.Size = new System.Drawing.Size(231, 16);
            this.inpReInputPassword.TabIndex = 1;
            this.inpReInputPassword.Enter += new System.EventHandler(this.inpReInputPassword_Enter);
            this.inpReInputPassword.Leave += new System.EventHandler(this.inpReInputPassword_Leave);
            // 
            // btnQuit
            // 
            this.btnQuit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuit.BackgroundImage")));
            this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQuit.Location = new System.Drawing.Point(436, 7);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(56, 25);
            this.btnQuit.TabIndex = 22;
            this.btnQuit.TabStop = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // lbRePassword
            // 
            this.lbRePassword.AutoSize = true;
            this.lbRePassword.Font = new System.Drawing.Font("Lato", 9.75F);
            this.lbRePassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbRePassword.Location = new System.Drawing.Point(59, 157);
            this.lbRePassword.Name = "lbRePassword";
            this.lbRePassword.Size = new System.Drawing.Size(117, 16);
            this.lbRePassword.TabIndex = 23;
            this.lbRePassword.Text = "Re-enter Password";
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Font = new System.Drawing.Font("Lato", 9.75F);
            this.lbUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUsername.Location = new System.Drawing.Point(108, 102);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(63, 16);
            this.lbUsername.TabIndex = 18;
            this.lbUsername.Text = "Password";
            // 
            // bunifuDragControl
            // 
            this.bunifuDragControl.Fixed = true;
            this.bunifuDragControl.Horizontal = true;
            this.bunifuDragControl.TargetControl = this;
            this.bunifuDragControl.Vertical = true;
            // 
            // lbResetMasterPassword
            // 
            this.lbResetMasterPassword.AutoSize = true;
            this.lbResetMasterPassword.Font = new System.Drawing.Font("Lato", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbResetMasterPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbResetMasterPassword.Location = new System.Drawing.Point(145, 43);
            this.lbResetMasterPassword.Name = "lbResetMasterPassword";
            this.lbResetMasterPassword.Size = new System.Drawing.Size(221, 18);
            this.lbResetMasterPassword.TabIndex = 24;
            this.lbResetMasterPassword.Text = "Enter New Master Password";
            // 
            // resetMasterPasswordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 274);
            this.Controls.Add(this.lbResetMasterPassword);
            this.Controls.Add(this.btnResetPassword);
            this.Controls.Add(this.pnPassword);
            this.Controls.Add(this.pnRePasswordBg);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.lbRePassword);
            this.Controls.Add(this.lbUsername);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "resetMasterPasswordForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reset Master Password";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.resetMasterPasswordForm_FormClosing);
            this.pnPassword.ResumeLayout(false);
            this.pnPassword.PerformLayout();
            this.pnRePasswordBg.ResumeLayout(false);
            this.pnRePasswordBg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse;
        private Bunifu.Framework.UI.BunifuThinButton2 btnResetPassword;
        private System.Windows.Forms.Panel pnPassword;
        private System.Windows.Forms.TextBox inpPassword;
        private System.Windows.Forms.Panel pnRePasswordBg;
        private System.Windows.Forms.TextBox inpReInputPassword;
        private System.Windows.Forms.PictureBox btnQuit;
        private System.Windows.Forms.Label lbRePassword;
        private System.Windows.Forms.Label lbUsername;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl;
        private Bunifu.Framework.UI.BunifuCustomLabel lbResetMasterPassword;
    }
}