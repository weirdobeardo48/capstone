﻿namespace CFML.GUIDesign.ForgetMasterPassword
{
    partial class forgetPasswordLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(forgetPasswordLoginForm));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pnUsernameBg = new System.Windows.Forms.Panel();
            this.inpUsername = new System.Windows.Forms.TextBox();
            this.btnLogin = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnQuit = new System.Windows.Forms.PictureBox();
            this.pnPasswordBg = new System.Windows.Forms.Panel();
            this.inpPassword = new System.Windows.Forms.TextBox();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbPassword = new System.Windows.Forms.Label();
            this.bunifuDragControl = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.lbLoginToResetMasterPassword = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pnUsernameBg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).BeginInit();
            this.pnPasswordBg.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // pnUsernameBg
            // 
            this.pnUsernameBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnUsernameBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnUsernameBg.Controls.Add(this.inpUsername);
            this.pnUsernameBg.Location = new System.Drawing.Point(210, 87);
            this.pnUsernameBg.Name = "pnUsernameBg";
            this.pnUsernameBg.Size = new System.Drawing.Size(250, 45);
            this.pnUsernameBg.TabIndex = 14;
            this.pnUsernameBg.Click += new System.EventHandler(this.pnUsernameBg_Click);
            // 
            // inpUsername
            // 
            this.inpUsername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpUsername.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpUsername.Location = new System.Drawing.Point(8, 12);
            this.inpUsername.Name = "inpUsername";
            this.inpUsername.Size = new System.Drawing.Size(231, 16);
            this.inpUsername.TabIndex = 1;
            this.inpUsername.Enter += new System.EventHandler(this.inpUsername_Enter);
            this.inpUsername.Leave += new System.EventHandler(this.inpUsername_Leave);
            // 
            // btnLogin
            // 
            this.btnLogin.ActiveBorderThickness = 1;
            this.btnLogin.ActiveCornerRadius = 20;
            this.btnLogin.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLogin.ActiveForecolor = System.Drawing.Color.WhiteSmoke;
            this.btnLogin.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnLogin.BackColor = System.Drawing.SystemColors.Control;
            this.btnLogin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogin.BackgroundImage")));
            this.btnLogin.ButtonText = "Login";
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.Font = new System.Drawing.Font("Lato", 12F);
            this.btnLogin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.btnLogin.IdleBorderThickness = 1;
            this.btnLogin.IdleCornerRadius = 20;
            this.btnLogin.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLogin.IdleForecolor = System.Drawing.Color.White;
            this.btnLogin.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLogin.Location = new System.Drawing.Point(210, 206);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(128, 47);
            this.btnLogin.TabIndex = 15;
            this.btnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click_1);
            // 
            // btnQuit
            // 
            this.btnQuit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuit.BackgroundImage")));
            this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQuit.Location = new System.Drawing.Point(491, 12);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(56, 25);
            this.btnQuit.TabIndex = 16;
            this.btnQuit.TabStop = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click_1);
            // 
            // pnPasswordBg
            // 
            this.pnPasswordBg.BackgroundImage = global::CFML.Properties.Resources.TextBoxBg;
            this.pnPasswordBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnPasswordBg.Controls.Add(this.inpPassword);
            this.pnPasswordBg.Location = new System.Drawing.Point(210, 142);
            this.pnPasswordBg.Name = "pnPasswordBg";
            this.pnPasswordBg.Size = new System.Drawing.Size(250, 45);
            this.pnPasswordBg.TabIndex = 15;
            this.pnPasswordBg.Click += new System.EventHandler(this.pnPasswordBg_Click);
            // 
            // inpPassword
            // 
            this.inpPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.inpPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inpPassword.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inpPassword.Location = new System.Drawing.Point(8, 12);
            this.inpPassword.Name = "inpPassword";
            this.inpPassword.PasswordChar = '•';
            this.inpPassword.Size = new System.Drawing.Size(231, 16);
            this.inpPassword.TabIndex = 1;
            this.inpPassword.Enter += new System.EventHandler(this.inpPassword_Enter);
            this.inpPassword.Leave += new System.EventHandler(this.inpPassword_Leave);
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Font = new System.Drawing.Font("Lato", 9.75F);
            this.lbUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbUsername.Location = new System.Drawing.Point(111, 102);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(67, 16);
            this.lbUsername.TabIndex = 2;
            this.lbUsername.Text = "Username";
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Font = new System.Drawing.Font("Lato", 9.75F);
            this.lbPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbPassword.Location = new System.Drawing.Point(111, 157);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(63, 16);
            this.lbPassword.TabIndex = 17;
            this.lbPassword.Text = "Password";
            // 
            // bunifuDragControl
            // 
            this.bunifuDragControl.Fixed = true;
            this.bunifuDragControl.Horizontal = true;
            this.bunifuDragControl.TargetControl = this;
            this.bunifuDragControl.Vertical = true;
            // 
            // lbLoginToResetMasterPassword
            // 
            this.lbLoginToResetMasterPassword.AutoSize = true;
            this.lbLoginToResetMasterPassword.Font = new System.Drawing.Font("Lato", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbLoginToResetMasterPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbLoginToResetMasterPassword.Location = new System.Drawing.Point(161, 46);
            this.lbLoginToResetMasterPassword.Name = "lbLoginToResetMasterPassword";
            this.lbLoginToResetMasterPassword.Size = new System.Drawing.Size(244, 18);
            this.lbLoginToResetMasterPassword.TabIndex = 25;
            this.lbLoginToResetMasterPassword.Text = "Login to reset master password";
            // 
            // forgetPasswordLoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 271);
            this.Controls.Add(this.lbLoginToResetMasterPassword);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.pnPasswordBg);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.pnUsernameBg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "forgetPasswordLoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Forget Master Password";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.forgetPasswordLoginForm_FormClosing);
            this.pnUsernameBg.ResumeLayout(false);
            this.pnUsernameBg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).EndInit();
            this.pnPasswordBg.ResumeLayout(false);
            this.pnPasswordBg.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel pnUsernameBg;
        private System.Windows.Forms.TextBox inpUsername;
        private Bunifu.Framework.UI.BunifuThinButton2 btnLogin;
        private System.Windows.Forms.PictureBox btnQuit;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Panel pnPasswordBg;
        private System.Windows.Forms.TextBox inpPassword;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl;
        private Bunifu.Framework.UI.BunifuCustomLabel lbLoginToResetMasterPassword;
    }
}