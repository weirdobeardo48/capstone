﻿using System;
using System.Collections.Generic;
using System.IO;
using TensorFlow;
using System.Linq;
using System.Diagnostics;
using System.Windows.Forms;
using CFML.Common;
using CFML.GUIDesign.CustomizeComponent;

namespace CFML.PornDetector
{
    class pornDetector
    {
        private static IEnumerable<CatalogItem> catalog;
        private static TFGraph graph;
        private static TFSession session = null;
        private static List<boxDetails> listBoxes;

        public static TFSession Session
        {
            get
            {
                return session;
            }

            set
            {
                session = value;
            }
        }

        public static TFSession initGraph()
        {
            try
            {
                // allocate memory for GPU
                TFSessionOptions TFOptions = new TFSessionOptions();
                unsafe
                {
                    byte[] GPUConfig = new byte[] { 0x32, 0x0b, 0x09, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0xd3, 0x3f, 0x20, 0x01 };
                    fixed (void* ptr = &GPUConfig[0])
                    {
                        TFOptions.SetConfig(new IntPtr(ptr), GPUConfig.Length);
                    }
                }

                //get current directory
                string currentDir = Directory.GetCurrentDirectory();
                //init graph
                graph = new TFGraph();
                //Import catalog -- *.pbtxt file
                catalog = CatalogUtil.ReadCatalogItems(currentDir + "\\label.pbtxt");
                // Load model to graph
                var model = File.ReadAllBytes(currentDir + "\\frozen_inference_graph.pb");
                graph.Import(new TFBuffer(model));
                Session = new TFSession(graph, TFOptions);
            }
            catch (Exception e)
            {
                CustomNotificationBox cnb = new CustomNotificationBox(e.Message, AlertType.Info);
                cnb.Visible = true;
                return null;
            }
            return Session;
        }

        static TFSession.Runner runner;
        static TFTensor[] output;
        static TFTensor tensor;
        public static TFTensor[] detectPorn(byte[] frame, TFSession session)
        {
            tensor = ImageUtil.CreateTensorFromImageFile(frame, TFDataType.UInt8);
            runner = session.GetRunner();
            runner
                .AddInput(graph["image_tensor"][0], tensor)
                .Fetch(
                graph["detection_boxes"][0],
                graph["detection_scores"][0],
                graph["detection_classes"][0],
                graph["num_detections"][0]);
            output = runner.Run();
            tensor.Dispose();
            return output;

        }


        public static List<boxDetails> visualizeResult(TFTensor[] output, double score)
        {
            var boxes = (float[,,])output[0].GetValue(jagged: false);
            var scores = (float[,])output[1].GetValue(jagged: false);
            var classes = (float[,])output[2].GetValue(jagged: false);
            var num = (float[])output[3].GetValue(jagged: false);
            for (int i = 0; i < output.Count(); i++)
            {
                output[i].Dispose();
            }
            return getBoxes(boxes, scores, classes, score);
        }

        //Draw bounding box
        private static List<boxDetails> getBoxes(float[,,] boxes, float[,] scores, float[,] classes, double minScore)
        {
            if (listBoxes != null)
            {
                listBoxes.Clear();
            }
            else
            {
                listBoxes = new List<boxDetails>();
            }
            var x = boxes.GetLength(0);
            var y = boxes.GetLength(1);
            var z = boxes.GetLength(2);
            float ymin = 0, xmin = 0, ymax = 0, xmax = 0;

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (scores[i, j] < minScore) continue;
                    for (int k = 0; k < z; k++)
                    {
                        var box = boxes[i, j, k];
                        switch (k)
                        {
                            case 0:
                                ymin = box;
                                break;
                            case 1:
                                xmin = box;
                                break;
                            case 2:
                                ymax = box;
                                break;
                            case 3:
                                xmax = box;
                                break;
                        }
                    }
                    int value = Convert.ToInt32(classes[i, j]);
                    CatalogItem catalogItem = catalog.FirstOrDefault(item => item.Id == value);
                    if (catalogItem != null)
                    {

                        Console.WriteLine("Found object: " + catalogItem.DisplayName);
                        Console.WriteLine("Bounding box: " + ymin + " " + ymax + " " + xmin + " " + xmax);
                        Console.WriteLine("Score: " + scores[i, j]);
                        boxDetails box = new boxDetails();
                        box.Xmin = xmin;
                        box.Ymin = ymin;
                        box.Xmax = xmax;
                        box.Ymax = ymax;
                        //if (catalogItem.DisplayName.Equals("need_to_be_censored"))
                        //{
                        //    if (scores[i, j] > 0.8)
                        //    {
                        //        listBoxes.Add(box);
                        //    }
                        //}
                        //if (catalogItem.DisplayName.Equals("underwear"))
                        //{
                        //    // Do nothing for now
                        //}
                        //else if (catalogItem.DisplayName.Trim().Equals("sex_scene"))
                        //{
                        //    if (scores[i, j] > 0.65)
                        //    {
                        //        listBoxes.Add(box);
                        //    }
                        //}
                        //else if (catalogItem.DisplayName.Trim().Equals("ass"))
                        //{
                        //    if (scores[i, j] > 0.85)
                        //    {
                        //        listBoxes.Add(box);
                        //    }
                        //}
                        //else
                        //{
                            listBoxes.Add(box);
                        //}
                    }
                }
            }
            return listBoxes;
        }
    }
}


