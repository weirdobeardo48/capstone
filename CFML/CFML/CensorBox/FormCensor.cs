﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace CFML.CensorBox
{
    public partial class FormCensor : Form
    {
        private int width, height;
        private static int imgw, imgh, xmin = 0, ymin = 0, xmax = 0, ymax = 0;
        public static Image img;
        public static Color color;
        private PictureBox pictureBox = new PictureBox();
        
        public FormCensor()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;
            IntPtr hwnd = this.Handle;
            this.TopMost = true;
            this.ShowInTaskbar = false;
            FormCensor.CheckForIllegalCrossThreadCalls = false;
        }

      

        private void FormCensor_Load(object sender, EventArgs e)
        {
            FormCensor.CheckForIllegalCrossThreadCalls = false;
        }

        public void load(Image image)
        {
            if (image == null)
            {
                img = null;
            }
            else
            {
                img = image;
                imgw = img.Width;
                imgh = img.Height;
            }
        }

        public void setLocation(int minx, int miny, int maxx, int maxy)
        {
            FormCensor.CheckForIllegalCrossThreadCalls = false;
            xmin = minx;
            xmax = maxx;
            ymin = miny;
            ymax = maxy;
            this.width = Convert.ToInt32((xmax - xmin) * 1.5);
            this.height = Convert.ToInt32((ymax - ymin) * 1.5);
            this.Size = new Size(width, height);
            this.Location = new Point(xmin, ymin);
            if (color == null)
            {
                this.BackColor = Color.Black;
            }
            else this.BackColor = color;
            if (imgw != 0 && imgh != 0 && img != null)
            {
                pictureBox.Image = new Bitmap(img);
                if (((float)height / (float)imgh) >= ((float)width / (float)imgw))
                {
                    pictureBox.Size = new Size(width, imgh * width / imgw);
                    pictureBox.Location = new Point(0, (height - imgh * width / imgw) / 2);
                    this.Width = width;
                }
                else
                {
                    pictureBox.Size = new Size(imgw * height / imgh, height);
                    pictureBox.Location = new Point((width - imgw * height / imgh) / 2, 0);
                }
                pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
               
                
            }
            else
            {
                pictureBox.Image = null;
            }
            this.BeginInvoke((Action)(() =>
            {
                this.Controls.Add(pictureBox);
            }));
        }

        public void loadColor(Color c)
        {
            color = c;
        }
        

        protected override bool ShowWithoutActivation
        {
            get
            {
                return true;
            }
        }


        private const int WS_EX_NOACTIVATE = 0x08000000;
        private const int WS_EX_TOOLWINDOW = 0x80;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams createParams = base.CreateParams;
                createParams.ExStyle |= (WS_EX_NOACTIVATE | WS_EX_TOOLWINDOW) ;
                return createParams;

            }
        }

        private const int WM_MOUSEACTIVATE = 0x0021, MA_NOACTIVATE = 0x0003;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_MOUSEACTIVATE)
            {
                m.Result = (IntPtr)MA_NOACTIVATE;
                return;
            }
            base.WndProc(ref m);
        }

    }

}
    
