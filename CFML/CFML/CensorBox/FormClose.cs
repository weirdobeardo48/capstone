﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using CFML.GUIDesign;
using CFML.Properties;

namespace CFML.CensorBox
{
    public partial class FormClose : Form
    {
        public int pId;
        private float xmin, ymin;
        private int width, height;
        private Button closeBtn = new Button();
        public FormClose()
        {
            InitializeComponent();
            this.BackColor = Color.Black;
            this.FormBorderStyle = FormBorderStyle.None;
            IntPtr hwnd = this.Handle;
            this.TopMost = true;
            this.ShowInTaskbar = false;
            
            closeBtn.Text = "Close Application";
            closeBtn.ForeColor = Color.White;
            closeBtn.Width = 200;
            closeBtn.Height = 50;
            closeBtn.Click += closeBtn_Click;
            this.Controls.Add(closeBtn);
            this.Icon = Resources.AppIcon;
        }

        public void setPosition(float xmin, float ymin, float xmax, float ymax, int pId)
        {
            this.pId = pId;
            this.xmin = xmin;
            this.ymin = ymin;
            this.width = Convert.ToInt32((xmax - xmin) * 1.1);
            this.height = Convert.ToInt32((ymax - ymin) * 1.1);
            this.Location = new Point(Convert.ToInt32(xmin), Convert.ToInt32(ymin));
            this.Size = new Size(width, height);
            closeBtn.Location = new Point((this.Width - closeBtn.Width) / 2, (this.Height - closeBtn.Height) / 2);
        }
        public void closeBtn_Click(object sender, EventArgs e)
        {
            Process p = Process.GetProcessById(pId);
            try
            {
                p.Kill();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            this.Hide();
            Common.Common.setCheckFormClose();
        }
    }
}
