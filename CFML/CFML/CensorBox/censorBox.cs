﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CFML.Common;
using CFML.CensorBox;
using System.IO;
using System.Drawing;
using System.Threading;

namespace CMFL.CensorBox
{
    class censorBox : ApplicationContext
    {
        private FormCensor formCensor;
        private static List<FormCensor> lstFormCensor = new List<FormCensor>();
        private static List<FormCensor> lstFormCensor2 = new List<FormCensor>();
        private static List<boxDetails> lst = new List<boxDetails>();
        private ThreadStart childref;
        private Thread childThread;
        private static int mark = 1;

        private void onFormClosed(object sender, EventArgs e)
        {
            if (Application.OpenForms.Count == 0)
            {
                ExitThread();
            }
        }

        public void initForm(Image img, int maxCen, Color color)
        {
            for (int i = 0; i < maxCen+1; i++)
            {
                formCensor = new FormCensor();
                formCensor.Opacity = 0;
                if (img != null)
                {
                    formCensor.load(img);
                }
                formCensor.loadColor(color);
                lstFormCensor.Add(formCensor);
                formCensor = new FormCensor();
                formCensor.Opacity = 0;
                if (img != null)
                {
                    formCensor.load(img);
                }
                lstFormCensor2.Add(formCensor);
            }
        }
        public void initForm()
        {
            for (int i = 0; i < 8; i++)
            {
                formCensor = new FormCensor();
                formCensor.Opacity = 0;
                lstFormCensor.Add(formCensor);
                formCensor = new FormCensor();
                formCensor.Opacity = 0;
                lstFormCensor2.Add(formCensor);
            }
        }

        public void initForm(int maxCen, Color color)
        {
            for (int i = 0; i < maxCen+1; i++)
            {
                formCensor = new FormCensor();
                formCensor.Opacity = 0;
                formCensor.load(null);
                formCensor.loadColor(color);
                lstFormCensor.Add(formCensor);
                formCensor = new FormCensor();
                formCensor.Opacity = 0;
                formCensor.load(null);
                formCensor.loadColor(color);
                lstFormCensor2.Add(formCensor);
            }
        }

        public static void setList(List<boxDetails> list)
        {
            lst = list;
        }

        public static void callStopThread()
        {
            Thread.Sleep(300);
            if (lstFormCensor.Count != 0)
            {
                try {
                    if (mark == 1)
                    {
                        foreach (var formCensor in lstFormCensor2.ToList())
                        {
                            if (formCensor.Opacity != 0)
                            {
                                formCensor.Opacity = 0.0f;
                            }
                        }
                    }
                    else
                    {
                        foreach (var formCensor in lstFormCensor.ToList())
                        {
                            if (formCensor.Opacity != 0)
                            {
                                formCensor.Opacity = 0.0f;
                            }
                        }
                    }
                } catch(Exception e) {
                    Console.WriteLine(e);
                }
            }
        }

        public void Hide()
        {
            foreach (var formCensor in lstFormCensor2.ToList())
            {
                if (formCensor.Opacity != 0)
                {
                    formCensor.Opacity = 0.0f;
                }
            }
            foreach (var formCensor in lstFormCensor.ToList())
            {
                if (formCensor.Opacity != 0)
                {
                    formCensor.Opacity = 0.0f;
                }
            }
        }
        

        public void displayForm()
        {
            //If WinForms exposed a global event that fires whenever a new Form is created,
            //we could use that event to register for the formCensor's `FormClosed` event.
            //Without such a global event, we have to register each Form when it is created
            //This means that any lstFormCensor created outside of the ApplicationContext will not prevent the 
            //application close.
            if (lstFormCensor.Count != 0)
            {
                if (lstFormCensor[1].Opacity == 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        
                        boxDetails b = (boxDetails)lst[i];
                        int xmin, xmax, ymin, ymax;
                        xmin = Convert.ToInt32(b.Xmin * Screen.PrimaryScreen.Bounds.Width);
                        xmax = Convert.ToInt32(b.Xmax * Screen.PrimaryScreen.Bounds.Width);
                        ymin = Convert.ToInt32(b.Ymin * Screen.PrimaryScreen.Bounds.Height);
                        ymax = Convert.ToInt32(b.Ymax * Screen.PrimaryScreen.Bounds.Height);
                        formCensor = lstFormCensor[i + 1];
                        formCensor.setLocation(xmin, ymin, xmax, ymax);
                        formCensor.Opacity = 1.0f;
                        lstFormCensor[i + 1] = formCensor;
                    }

                    foreach (var formCensor in lstFormCensor)
                    {
                        formCensor.FormClosed += onFormClosed;
                    }

                    //to show all the lstFormCensor on start
                    //can be included in the previous foreach
                    foreach (var formCensor in lstFormCensor)
                    {
                        if (formCensor.Opacity != 0)
                            formCensor.Show();
                    }

                    //to show only the first formCensor on start
                    //lstFormCensor[0].Show();
                    mark = 1;
                }
                else
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        
                        boxDetails b = (boxDetails)lst[i];
                        int xmin, xmax, ymin, ymax;
                        xmin = Convert.ToInt32(b.Xmin * Screen.PrimaryScreen.Bounds.Width);
                        xmax = Convert.ToInt32(b.Xmax * Screen.PrimaryScreen.Bounds.Width);
                        ymin = Convert.ToInt32(b.Ymin * Screen.PrimaryScreen.Bounds.Height);
                        ymax = Convert.ToInt32(b.Ymax * Screen.PrimaryScreen.Bounds.Height);
                        formCensor = lstFormCensor2[i + 1];
                        formCensor.setLocation(xmin, ymin, xmax, ymax);
                        formCensor.Opacity = 1.0f;
                        lstFormCensor2[i + 1] = formCensor;
                    }

                    foreach (var formCensor in lstFormCensor2)
                    {
                        formCensor.FormClosed += onFormClosed;
                    }

                    //to show all the lstFormCensor on start
                    //can be included in the previous foreach
                    foreach (var formCensor in lstFormCensor2)
                    {
                        if (formCensor.Opacity != 0)
                            formCensor.Show();
                        
                    }
                    mark = 2;
                    //to show only the first formCensor on start
                    //lstFormCensor[0].Show();
                    
                }
                childref = new ThreadStart(callStopThread);
                childThread = new Thread(childref);
                childThread.Start();

            }
        }
        public censorBox()
        {
        }
    }
}
