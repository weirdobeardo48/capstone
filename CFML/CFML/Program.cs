﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
using CFML.GUIDesign.CustomizeComponent;
using CFML.Common;

namespace CFML
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //Bunifu.Framework.License.Authenticate("cominguppp@gmail.com", "+f/rTder1tvRj9Dqh7Lk4FnP2a/aStaUyCM2ttUdqSk=");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new GUIDesign.loginForm());
            //Application.Run(new GUIDesign.CustomizeComponent.LoginByMasterPassword());
            //Application.Run(new GUI.mainForm());

            //Application.Run(new Test());


            if (!Common.Common.IsAdministrator())
            {
                MessageBox.Show(ComponentDescriptionText.RUN_AS_ADMINISTRATOR_REQUEST);
                return;
            }
            else
            {
                Common.Common.removeControlPanelEntry();
                if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\FMP.dat"))
                {

                    if (Common.Common.readRegistryIfInitialKeyIsSet() == true)
                    {
                        CustomConfirmationBox ccb = new CustomConfirmationBox(
                            ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_MESSAGE,
                            ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_TITLE);
                        var result = ccb.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            Application.Run(new GUIDesign.ForgetMasterPassword.forgetPasswordLoginForm());
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        Application.Run(new GUIDesign.setInitialKeyForm());
                    }
                }
                else
                {
                    if (Common.Common.readRegistryIfInitialKeyIsSet() == true)
                    {
                        //Application.Run(new GUIDesign.CustomizeComponent.LoginByMasterPassword());
                        GUIDesign.loginForm LoginForm = new GUIDesign.loginForm(true);
                        
                        //LoginForm.hideFormToTray();
                        
                        Application.Run();
                        
                    }
                    else
                    {
                        CustomConfirmationBox ccb = new CustomConfirmationBox(
                            ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_MESSAGE,
                            ComponentDescriptionText.MASTER_PASSWORD_CORRUPTED_TITLE);
                        var result = ccb.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            Application.Run(new GUIDesign.ForgetMasterPassword.forgetPasswordLoginForm());
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }

        }
    }
}