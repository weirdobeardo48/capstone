﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CFML.Capture
{
    public class GDI
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        internal static extern bool Rectangle(
           IntPtr hdc,
           int ulCornerX, int ulCornerY,
           int lrCornerX, int lrCornerY);
    }
}
