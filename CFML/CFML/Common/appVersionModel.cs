﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    public class appVersionModel
    {
        public string appVersionNumber { get; set; }
        public string appLink { get; set; }
    }

    public class rootAppVersionModel
    {
        public appVersionModel[] Property { get; set; }
    }
}
