﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    public class loginDetail
    {
        private string username;
        private string password;

        public loginDetail()
        {

        }

        public loginDetail(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public void EncryptLoginInformation()
        {
            EncryptInfo();
        }

        private void EncryptInfo()
        {
            this.password = Common.CreateMD5(this.password);
        }
    }
}
