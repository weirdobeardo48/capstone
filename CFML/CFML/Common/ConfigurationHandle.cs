﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFML.Configuration;
using System.IO;

namespace CFML.Common
{
    class ConfigurationHandle
    {
        private static Configuration.Configuration config = new Configuration.Configuration();

        internal static Configuration.Configuration Config
        {
            get
            {
                return config;
            }

            set
            {
                config = value;
            }
        }

        public void setDefaultConfiguration()
        {
            Config.CensorSensitivity = 30;
            Config.CheckInactiveApplication = false;
            Config.DisabledRealTimeScanInterval = 5;
            Config.EnableMuteSound = false;
            Config.EnableObjectTracking = false;
            Config.EnableRealTimeMode = true;

            Config.CensorBgImageEnable = false;
            Config.CensorBgImageName = "";
            Config.CensorBgColor = "#000000";

            Config.PrivacyCheck = true;
        }

        public void writeConfigurationToFile()
        {
            /*censor-sensitivity = 20
            maximum-censored-object = 4 
            object-tracking = 0
            tracking-object-scan-interval = 20
            real-time-mode = 0
            disabled-real-time-scan-interval = 5
            enable-mute-sound = 0
            check-inactive-application = 1 */

            string configToWrite = "";
            if (config != null) { Config = new Configuration.Configuration(); }
            configToWrite = configToWrite + "censor-sensitivity =" + Config.CensorSensitivity + System.Environment.NewLine;
            configToWrite = configToWrite + "maximum-censored-object = " + Config.MaximumCensoredObject + System.Environment.NewLine;
            if (Config.EnableObjectTracking)
            {
                configToWrite = configToWrite + "object-tracking = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "object-tracking = " + 0 + System.Environment.NewLine;
            }
            configToWrite = configToWrite + "tracking-object-scan-interval = " + Config.ObjectTrackingScanInterval + System.Environment.NewLine;
            if (Config.EnableRealTimeMode)
            {
                configToWrite = configToWrite + "real-time-mode = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "real-time-mode = " + 0 + System.Environment.NewLine;
            }
            configToWrite = configToWrite + "disabled-real-time-scan-interval = " + Config.DisabledRealTimeScanInterval + System.Environment.NewLine;
            if (Config.EnableMuteSound)
            {
                configToWrite = configToWrite + "enable-mute-sound = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "enable-mute-sound = " + 0 + System.Environment.NewLine;
            }
            if (Config.CheckInactiveApplication)
            {
                configToWrite = configToWrite + "check-inactive-application = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "check-inactive-application = " + 0 + System.Environment.NewLine;
            }
            if (Config.CensorBgImageEnable)
            {
                configToWrite = configToWrite + "censor-bg-picture-enable = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "censor-bg-picture-enable = " + 0 + System.Environment.NewLine;
            }
            configToWrite = configToWrite + "censor-bg-picture-name = " + Config.CensorBgImageName.Trim() + System.Environment.NewLine;
            configToWrite = configToWrite + "censor-bg-color = " + Config.CensorBgColor.Trim() + System.Environment.NewLine;

            if (Config.PrivacyCheck)
            {
                configToWrite = configToWrite + "privacy-check = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "privacy-check = " + 0 + System.Environment.NewLine;
            }

            File.WriteAllText(Directory.GetCurrentDirectory() + "\\config.cfg", configToWrite);
        }

        public static void writeConfigurationToFile(Configuration.Configuration cfg)
        {
            /*censor-sensitivity = 20
            maximum-censored-object = 4 
            object-tracking = 0
            tracking-object-scan-interval = 20
            real-time-mode = 0
            disabled-real-time-scan-interval = 5
            enable-mute-sound = 0
            check-inactive-application = 1 */

            string configToWrite = "";
            if (cfg != null) { Config = new Configuration.Configuration(); }
            configToWrite = configToWrite + "censor-sensitivity =" + cfg.CensorSensitivity + System.Environment.NewLine;
            configToWrite = configToWrite + "maximum-censored-object = " + cfg.MaximumCensoredObject + System.Environment.NewLine;
            if (cfg.EnableObjectTracking)
            {
                configToWrite = configToWrite + "object-tracking = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "object-tracking = " + 0 + System.Environment.NewLine;
            }
            configToWrite = configToWrite + "tracking-object-scan-interval = " + cfg.ObjectTrackingScanInterval + System.Environment.NewLine;
            if (cfg.EnableRealTimeMode)
            {
                configToWrite = configToWrite + "real-time-mode = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "real-time-mode = " + 0 + System.Environment.NewLine;
            }
            configToWrite = configToWrite + "disabled-real-time-scan-interval = " + cfg.DisabledRealTimeScanInterval + System.Environment.NewLine;
            if (cfg.EnableMuteSound)
            {
                configToWrite = configToWrite + "enable-mute-sound = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "enable-mute-sound = " + 0 + System.Environment.NewLine;
            }
            if (cfg.CheckInactiveApplication)
            {
                configToWrite = configToWrite + "check-inactive-application = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "check-inactive-application = " + 0 + System.Environment.NewLine;
            }
            if (cfg.CensorBgImageEnable)
            {
                configToWrite = configToWrite + "censor-bg-picture-enable = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "censor-bg-picture-enable = " + 0 + System.Environment.NewLine;
            }
            configToWrite = configToWrite + "censor-bg-picture-name = " + cfg.CensorBgImageName.Trim() + System.Environment.NewLine;
            configToWrite = configToWrite + "censor-bg-color = " + cfg.CensorBgColor.Trim() + System.Environment.NewLine;

            if (cfg.PrivacyCheck)
            {
                configToWrite = configToWrite + "privacy-check = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "privacy-check = " + 0 + System.Environment.NewLine;
            }

            File.WriteAllText(Directory.GetCurrentDirectory() + "\\config.cfg", configToWrite);
        }

        public static void writeConfigurationToFileByUserType(Configuration.Configuration cfg, int userType)
        {
            /*censor-sensitivity = 20
            maximum-censored-object = 4 
            object-tracking = 0
            tracking-object-scan-interval = 20
            real-time-mode = 0
            disabled-real-time-scan-interval = 5
            enable-mute-sound = 0
            check-inactive-application = 1 */

            ConfigurationHandle lastConfig = new ConfigurationHandle();
            lastConfig.readConfigurationFromFile();

            switch (userType.ToString())
            {
                //VIP
                case "2":
                    {
                        //Update full
                        break;
                    }
                // Normal
                case "1":
                    {
                        cfg.CensorBgColor = Config.CensorBgColor.Trim();
                        cfg.CensorBgImageEnable = Config.CensorBgImageEnable;
                        cfg.CensorBgImageName = Config.CensorBgImageName.Trim();

                        cfg.CheckInactiveApplication = Config.CheckInactiveApplication;
                        cfg.EnableMuteSound = Config.EnableMuteSound;

                        cfg.PrivacyCheck = Config.PrivacyCheck;
                        break;
                    }
                // Guest = 0, by default
                default:
                    {
                        cfg.CensorBgColor = Config.CensorBgColor.Trim();
                        cfg.CensorBgImageEnable = Config.CensorBgImageEnable;
                        cfg.CensorBgImageName = Config.CensorBgImageName.Trim();

                        cfg.CheckInactiveApplication = Config.CheckInactiveApplication;
                        cfg.EnableMuteSound = Config.EnableMuteSound;

                        cfg.EnableObjectTracking = Config.EnableObjectTracking;
                        cfg.EnableRealTimeMode = Config.EnableRealTimeMode;
                        cfg.DisabledRealTimeScanInterval = Config.DisabledRealTimeScanInterval;

                        cfg.PrivacyCheck = Config.PrivacyCheck;
                        break;
                    }
            }

            string configToWrite = "";
            if (cfg != null) { Config = new Configuration.Configuration(); }
            configToWrite = configToWrite + "censor-sensitivity =" + cfg.CensorSensitivity + System.Environment.NewLine;
            configToWrite = configToWrite + "maximum-censored-object = " + cfg.MaximumCensoredObject + System.Environment.NewLine;
            if (cfg.EnableObjectTracking)
            {
                configToWrite = configToWrite + "object-tracking = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "object-tracking = " + 0 + System.Environment.NewLine;
            }
            configToWrite = configToWrite + "tracking-object-scan-interval = " + cfg.ObjectTrackingScanInterval + System.Environment.NewLine;
            if (cfg.EnableRealTimeMode)
            {
                configToWrite = configToWrite + "real-time-mode = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "real-time-mode = " + 0 + System.Environment.NewLine;
            }
            configToWrite = configToWrite + "disabled-real-time-scan-interval = " + cfg.DisabledRealTimeScanInterval + System.Environment.NewLine;
            if (cfg.EnableMuteSound)
            {
                configToWrite = configToWrite + "enable-mute-sound = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "enable-mute-sound = " + 0 + System.Environment.NewLine;
            }
            if (cfg.CheckInactiveApplication)
            {
                configToWrite = configToWrite + "check-inactive-application = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "check-inactive-application = " + 0 + System.Environment.NewLine;
            }
            if (cfg.CensorBgImageEnable)
            {
                configToWrite = configToWrite + "censor-bg-picture-enable = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "censor-bg-picture-enable = " + 0 + System.Environment.NewLine;
            }
            
            configToWrite = configToWrite + "censor-bg-picture-name = " + cfg.CensorBgImageName.Trim() + System.Environment.NewLine;
            configToWrite = configToWrite + "censor-bg-color = " + cfg.CensorBgColor.Trim() + System.Environment.NewLine;

            if (cfg.PrivacyCheck)
            {
                configToWrite = configToWrite + "privacy-check = " + 1 + System.Environment.NewLine;
            }
            else
            {
                configToWrite = configToWrite + "privacy-check = " + 0 + System.Environment.NewLine;
            }

            File.WriteAllText(Directory.GetCurrentDirectory() + "\\config.cfg", configToWrite);
        }

        public void readConfigurationFromFile()
        {
            Config = new Configuration.Configuration();
            setDefaultConfiguration();
            string fileName = Directory.GetCurrentDirectory() + "\\config.cfg";

            try
            {
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string line = String.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        //get Censor sensitivity from config file
                        if (line.Trim().Contains("censor-sensitivity"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    Config.CensorSensitivity = Int32.Parse(split[1]);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }
                        // get maximum object to ben censored
                        if (line.Trim().Contains("maximum-censored-object"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    Config.MaximumCensoredObject = Int32.Parse(split[1]);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }
                        // Enable object tracking or not??
                        if (line.Trim().Contains("object-tracking"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    int enableOrNot = Int32.Parse(split[1]);
                                    if (enableOrNot == 0)
                                    {
                                        Config.EnableObjectTracking = false;
                                    }
                                    else
                                    {
                                        Config.EnableObjectTracking = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        //Get object tracking scan interval
                        if (line.Trim().Contains("tracking-object-scan-interval"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    Config.ObjectTrackingScanInterval = Int32.Parse(split[1]);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        // Enable or disable realtime mode??
                        if (line.Trim().Contains("real-time-mode"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    int enableOrNot = Int32.Parse(split[1]);
                                    if (enableOrNot == 0)
                                    {
                                        Config.EnableRealTimeMode = false;
                                    }
                                    else
                                    {
                                        Config.EnableRealTimeMode = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        // get scan interval if realtime disabled!
                        if (line.Trim().Contains("disabled-real-time-scan-interval"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    Config.DisabledRealTimeScanInterval = Int32.Parse(split[1]);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        //Mute sound or not???
                        if (line.Trim().Contains("enable-mute-sound"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    int enableOrNot = Int32.Parse(split[1]);
                                    if (enableOrNot == 0)
                                    {
                                        Config.EnableMuteSound = false;
                                    }
                                    else
                                    {
                                        Config.EnableMuteSound = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        //Check inactive application
                        if (line.Trim().Contains("check-inactive-application"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    int enableOrNot = Int32.Parse(split[1]);
                                    if (enableOrNot == 0)
                                    {
                                        Config.CheckInactiveApplication = false;
                                    }
                                    else
                                    {
                                        Config.CheckInactiveApplication = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        //Check Enable Picture Background for Censor Form
                        if (line.Trim().Contains("censor-bg-picture-enable"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    int enableOrNot = Int32.Parse(split[1]);
                                    if (enableOrNot == 0)
                                    {
                                        Config.CensorBgImageEnable = false;
                                    }
                                    else
                                    {
                                        Config.CensorBgImageEnable = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        // Get Censor Background File Name
                        if (line.Trim().Contains("censor-bg-picture-name"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    Config.CensorBgImageName = split[1];
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        // Get Censor Background Color
                        if (line.Trim().Contains("censor-bg-color"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    Config.CensorBgColor = split[1];
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }

                        // write privacy check 
                        if (line.Trim().Contains("privacy-check"))
                        {
                            string[] split = line.Split('=');
                            if (split.Count() == 2)
                            {
                                try
                                {
                                    int enableOrNot = Int32.Parse(split[1]);
                                    if (enableOrNot == 0)
                                    {
                                        Config.PrivacyCheck = false;
                                    }
                                    else
                                    {
                                        Config.PrivacyCheck = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        public void readConfigurationFromFileByUserType(int userType)
        {
            readConfigurationFromFile();

            // Twerk by User Type
            switch (userType.ToString())
            {
                //Normal
                case "1":
                    {
                        Config.CheckInactiveApplication = false;
                        Config.EnableMuteSound = false;

                        Config.CensorBgImageEnable = false;
                        Config.CensorBgImageName = "";
                        Config.CensorBgColor = "#000000";

                        Config.PrivacyCheck = true;
                        break;
                    }
                //VIP
                case "2":
                    {
                        //Change nothin
                        break;
                    }
                //Guest = 0, by default
                default:
                    {
                        Config.EnableRealTimeMode = true;
                        Config.EnableObjectTracking = false;
                        Config.CheckInactiveApplication = false;
                        Config.EnableMuteSound = false;
                        Config.DisabledRealTimeScanInterval = 20;

                        Config.CensorBgImageEnable = false;
                        Config.CensorBgImageName = "";
                        Config.CensorBgColor = "#000000";

                        Config.PrivacyCheck = true;
                        break;
                    }
            }
        }
    }
}
