﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    public class userDetail
    {
        /* User Type Denotation
         * 0 = Guest
         * 1 = Normal
         * 2 = VIP
         */

        private string ownerName;
        private string email;
        private int userType;
        private DateTime? expiredDate;

        // Create Guest User by default
        public userDetail()
        {
            ownerName = "Guest";
            email = "";
            userType = 0;
            expiredDate = null;
        }

        public userDetail(string ownerName, string email, DateTime? expiredDate)
        {
            this.ownerName = ownerName;
            this.email = email;
            if (DateTime.Now < expiredDate)
            {
                this.userType = 2;
            }
            else this.userType = 1;
            this.expiredDate = expiredDate;
        }

        public userDetail(string ownerName, string email, int userType, DateTime? expiredDate)
        {
            this.ownerName = ownerName;
            this.email = email;
            this.userType = userType;
            this.expiredDate = expiredDate;
        }

        public userDetail(userModel model)
        {
            this.ownerName = model.userName;
            this.email = model.userEmail;
            if (model.expiredDate == null)
            {
                this.userType = 0;
            }
            else { 
                if (DateTime.Now < model.expiredDate)
                {
                    this.userType = 2;
                }
                else this.userType = 1;
            }
            this.expiredDate = model.expiredDate;
        }

        public string OwnerName
        {
            get
            {
                return ownerName;
            }

            set
            {
                ownerName = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public int UserType
        {
            get
            {
                return userType;
            }

            set
            {
                userType = value;
            }
        }

        public DateTime? ExpiredDate
        {
            get
            {
                return expiredDate;
            }

            set
            {
                expiredDate = value;
            }
        }
    }
}
