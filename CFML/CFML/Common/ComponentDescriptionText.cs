﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    public class ComponentDescriptionText
    {
        #region Login Form
        public const string LOGIN_FAILED_MESSAGE = "Can not login into your account.\r\nPlease check your login information, or internet connection.\r\nDo you want to login by Guest?";
        public const string LOGIN_FAILED_MESSAGE_TITLE = "Login Failed";
        #endregion

        #region Application
        public const string APPLICATION_NAME = "CFML";
        #endregion
        
        #region Main
        public const string MAIN_USER_GUEST = "Guest";
        public const string MAIN_USER_NORMAL = "Normal User";
        public const string MAIN_USER_VIP = "VIP User";

        public const string LOGOUT_WARNING = "Are you sure you want to logout?";
        public const string LOGOUT_WARNING_TITLE = "Logout";
        #endregion

        #region Main User Control
        public const string UC_MAIN_CHILDREN_MODE_DESCRIPTION = "In Children mode, your computer is supervised by CFML and protected from unwanted content.";
        public const string UC_MAIN_PARENT_MODE_DESCRIPTION = "In Parent mode, your device is not protected by CFML\r\nSwich to Childrent Mode if you want your device is to be supervised by CFML and protected from unwanted content.";
        #endregion

        #region Account User Control
        public const string UC_ACCOUNT_GUEST_WELCOME_NOTE = "Log in as a user help you to manage account and explire more amazing functions of this application";
        public const string UC_ACCOUNT_STATUS_VIP = "VIP Account";
        public const string UC_ACCOUNT_STATUS_NORMAL = "Normal Account";
        
        #endregion

        #region Configuration User Control
        public const string UC_CONFIGURATION_SOUND_TITLE = "";
        public const string UC_CONFIGURATION_SOUND_DESCRIPTION = "This function helps you to mute the application \r\nif CFML censors objects in the application.";

        public const string UC_CONFIGURATION_OBJECT_TRACKING_TITLE = "";
        public const string UC_CONFIGURATION_OBJECT_TRACKING_DESCRIPTION = "Enabling object tracking might decrease censoring speed \r\nand decrease CPU performance.";

        public const string UC_CONFIGURATION_REALTIME_TITLE = "";
        public const string UC_CONFIGURATION_REALTIME_DESCRIPTION = "Disabling real-time lets CFML capture screen\r\nwith speed of x (seconds) per shot instead of\r\ntracking objects. You can set the value of x\r\nby changing number in box Scan interval.";

        public const string UC_CONFIGURATION_INACTIVE_TITLE = "";
        public const string UC_CONFIGURATION_INACTIVE_DESCRIPTION = "This functions helps you to track inactive applications \r\nand minimize them if there is any detected objects.\r\nIt may decrease CPU performance.";

        public const string UC_CONFIGURATION_CENSOR_OBJECT_TITLE = "";
        public const string UC_CONFIGURATION_CENSOR_OBJECT_DESCRIPTION = "This function helps you to set the maximum number\r\nof objects censored when CFML is running.\r\nIf there are more number of objects on an application\r\nthan you set, it forces you to close the application.\r\nHowever, the higher number, the more CPU it will use.";

        public const string UC_CONFIGURATION_CENSOR_SENSITIVITY_TITLE = "";
        public const string UC_CONFIGURATION_CENSOR_SENSITIVITY_DESCRIPTION = "This function helps you to set level of censor sensitivity. \r\nThe higher number of level is, the more precise CFML censors. \r\nHowever, it will use more CPU";

        public const string UC_CONFIGURATION_ENABLE_REALTIME_WARINING = "Warning";
        public const string UC_CONFIGURATION_ENABLE_REALTIME_WARINING_TITLE = "This function stops tracking objects. Do you still want to continue?";

        public const string UC_CONFIGURATION_ENABLE_OBJECT_TRACKING_WARINING = "Warning";
        public const string UC_CONFIGURATION_ENABLE_OBJECT_TRACKING_WARINING_TITLE = "This function might decrease CPU performance. Do you still want to continue?";

        public const string DEFAULT_CENSOR_IMAGE_FOLDER = @"\img\";
        public const string DEFAULT_CENSOR_IMAGE_FILE_NAME = "CensorBg.png";
        #endregion

        #region Change Key User Control
        public const string UC_CHANGE_KEY_OLD = "  Enter the old master password";
        public const string UC_CHANGE_KEY_NEW = "  Enter the new master password";
        public const string UC_CHANGE_KEY_NEW_CF = "  Confirm the new master password";
        #endregion

        #region Customize User Control
        public const string UC_CUSTOMIZE_PICTURE_DESCRIPTON = "This may decrease performance of the application";
        #endregion

        #region Privacy User Control
        public const string UC_PRIVACY_ANONYMOUS_COLLECT_DATA_DESCRIPTON = "Your selection for the option above means you agree to our server to access your device and get data for training for better version in future";
        #endregion

        #region About Us Control
        public const string UC_ABOUT_DESCRIPTON = "Develop team: Te Team\r\nContact: CFML.contact@gmail.com";
        #endregion

        #region Configuration
        public const string SAVE_CONFIG_WARNING = "Do you want to save this configuration?";
        public const string SAVE_CONFIG_WARNING_TITLE = "Configuration";
        public const string SAVE_CONFIG_SUCCESS_MESSAGE = "Configuration has saved successful";
        public const string SAVE_CONFIG_FAILED_MESSAGE = "Configuration saving failed";
        # endregion

        #region API
        public const string URL_LOGIN_API = @"http://cfml-contentfilter.com:3000/userLogin/";
        public const string URL_APP_VERSION_API = @"http://cfml-contentfilter.com:3000/checkVersion/appVersion";
        public const string CHECK_FOR_UPDATE_FAILED = "Server is not responding. Please check for update later!";
        public const string CURRENT_APP_VERSION = "1.4";
        public const string VERSION_UP_TO_DATE_NOTIFICATION = "This is the newest application.";
        public const string VERSION_NEED_TO_UPDATE_NOTIFICATION = "New version of application is ready. Update now?";
        public const string VERSION_NEED_TO_UPDATE_NOTIFICATION_TITLE = "Your application is out of date!";
        #endregion

        #region Global Use URL
        public const string URL_REGISTER = @"https://cfml-contentfilter.com/register.xyz";
        public const string URL_EDIT_PROFILE = @"https://cfml-contentfilter.com/user/profile/userProfile.xyz";
        public const string URL_CHANGE_PASSWORD = @"https://cfml-contentfilter.com/user/profile/userProfilePassword.xyz";
        public const string URL_FORGET_PASSWORD = @"https://cfml-contentfilter.com/reset.xyz";
        public const string URL_UPGRADE_ACCOUNT = @"https://cfml-contentfilter.com/price.xyz";
        public const string URL_FEEDBACK_EMAIL = @"mailto:contact@cfml-contentfilter.com?subject=Feedback";
        public const string URL_DOWNLOAD_LATEST_VERSION = @"https://cfml-contentfilter.com/";
        #endregion

        #region Global User String
        public const string RUN_AS_ADMINISTRATOR_REQUEST = "Please run this application as administrator";
        public const string MASTER_PASSWORD_CORRUPTED_MESSAGE = "Masterpassword is corrupted!! \r\nDo you want to reset the master password?";
        public const string MASTER_PASSWORD_CORRUPTED_TITLE = "Masterpassword is corrupted";
        public const string FILL_THE_FORM_MESSAGE = "Please fill all the field";
        public const string WRONG_USERNAME_PASSWORD_MESSAGE = "Your username and password does not match";
        public const string LOGIN_SESSION_EXPIRED_MESSAGE = "Your login is expired! Please connect to the internet!";
        public const string NETWORK_NOT_AVAILABLE_MESSAGE = "Internet connection is not available. Please try again later!";
        public const string WRONG_PASSWORD_MESSAGE = "Password does not match";
        public const string CAPTURE_PROCESS_STOP_MESSAGE = "Capture Process has Stopped";
        public const string LAST_LOGGED_IN_USER_DOES_NOT_MATCH_MESSAGE = "This username is not the last one logged in\nWe can not reset your master password";
        public const string MASTER_PASSWORD_RESET_SUCCESSFUL_MESSAGE = "Masterpasssword reset successfully";
        public const string SAVE_PASSWORD_AND_BENCHMARKING_MESSAGE = "Masterpasssword set successfully!\nNow benchmarking for the best config!";
        public const string BENCHMARKING_FINISHED_MESSAGE = "Benchmark is finshed!";
        public const string MASTER_PASSWORD_CORRUPTED_REQUEST_RESTART_MESSAGE = "Masterpassword is coruppted\r\nPlease restart program and reset your masterpassword";
        public const string NEW_PASSWORD_DOES_NOT_MATCH_MESSAGE = "New password does not match!";
        public const string MASTER_PASSWORD_CHANGE_MESSAGE = "Your master password has been changed";
        public const string MASTER_PASSWORD_CHANGE_FAILED_MEMSSAGE = "There was a problem when trying to change your master password";
        public const string OLD_PASSWORD_WRONG_MESSAGE = "Your old password is wrong!";
        public const string SCAN_INTERVAL_VALIDATE_MESSAGE = "Scan interval value must be less than 31 and greater than 5.";
        public const string CAN_NOT_DELETE_IMAGE_MESSAGE = "Can not delete image because it is currently in use!";
        public const string QUIT_APPLICATION_TITLE = "Do you want to exit?";
        public const string QUIT_APPLICATION_MESSAGE = "If you exits, all the application functions will be disable.";
        public const string SAVE_CONFIG_REQUIRE_STOP_AI_MESSAGE = "CFML Detector is running!\r\nPlease stop Detector before saving your config.";
        public const string UPDATE_FAILED_MESSAGE = "It is some problems happened while updating.\r\nPlease try again later!";
        #endregion
    }
}
