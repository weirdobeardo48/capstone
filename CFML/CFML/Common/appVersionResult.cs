﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    public class appVersionResult
    {
        public string appVersionNumber { get; set; }
        public string appLink { get; set; }
        /*
         * 0 = Error
         * 1 = Up to date
         * 2 = Out of date
         */
        public int updateStatus { get; set; }

        public appVersionResult(){

        }

        public appVersionResult(string appVersionNumber, string appLink, int updateStatus)
        {
            this.appVersionNumber = appVersionNumber;
            this.appLink = appLink;
            this.updateStatus = updateStatus;
        }
        public appVersionResult(appVersionModel versionModel , int updateStatus)
        {
            this.appVersionNumber = versionModel.appVersionNumber;
            this.appLink = versionModel.appLink;
            this.updateStatus = updateStatus;
        }
    }
}
