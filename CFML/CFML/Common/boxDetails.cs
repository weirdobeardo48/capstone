﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    public class boxDetails
    {
        private float xmin;
        private float ymin;
        private float xmax;
        private float ymax;

        public boxDetails()
        {

        }

        public boxDetails(float xmin, float ymin, float xmax, float ymax)
        {
            this.xmin = xmin;
            this.ymin = ymin;
            this.xmax = xmax;
            this.ymax = ymax;
        }

        public float Xmin
        {
            get
            {
                return xmin;
            }

            set
            {
                xmin = value;
            }
        }

        public float Ymin
        {
            get
            {
                return ymin;
            }

            set
            {
                ymin = value;
            }
        }

        public float Xmax
        {
            get
            {
                return xmax;
            }

            set
            {
                xmax = value;
            }
        }

        public float Ymax
        {
            get
            {
                return ymax;
            }

            set
            {
                ymax = value;
            }
        }


        public override string ToString()
        {
            try
            {
                return "[(" + xmin + ", " + ymin + "), " + "(" + xmax + ", " + ymax + ")]";
            }
            catch (Exception e)
            {
                return "[" + e.Message + "]";
            }
        }
    }
}
