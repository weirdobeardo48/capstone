﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{

    public class rootObjectUserModel
    {
        public userModel[] rootProperty { get; set; }
    }

    public class userModel
    {
         public userModel()
        {
            this.userName = null;
            this.userPhone = null;
            this.userEmail = null;
            this.expiredDate = null;
        }

        [JsonProperty("userName")]
        public string userName { get; set; }
        [JsonProperty("userPhone")]
        public string userPhone { get; set; }
        [JsonProperty("userEmail")]
        public string userEmail { get; set; }
        [JsonProperty("expiredDate")]
        public DateTime? expiredDate { get; set; }
    }
}
