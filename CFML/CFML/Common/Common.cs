﻿using CFML.Capture;
using CFML.CensorBox;
using CFML.GUIDesign.CustomizeComponent;
using CMFL.CensorBox;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    public class Common
    {

        public static string censorImgName = "";
        public  static Image censorImg;
        public static int censorMax = 8;
        public static Color censorBgColor = Color.Black;
        static censorBox cen = new censorBox();
        static bool checkFormClose = false;
        private static ConfigurationHandle configHandler = new ConfigurationHandle();

        public static void removeControlPanelEntry()
        {
            RegistryKey key = null;
            try
            {
                key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{A982188C-8659-4190-B96E-A4E5763A2161}", true);
                key.DeleteValue("ModifyPath", true); key.DeleteValue("UnInstallString", true);
                key.DeleteValue("Version", true); key.DeleteValue("VersionMajor", true);
                key.DeleteValue("VersionMinor", true);
                key.DeleteValue("WindowsInstaller", true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (key != null)
                { key.Close(); }
            }
        }

        public static void setCheckFormClose()
        {
            if (checkFormClose) checkFormClose = false;
            else checkFormClose = true;
        }

        public static bool getCheckFormClose()
        {
            return checkFormClose;
        }

        public static boxDetails getWindowBox(string pid)
        {
            try
            {

                var a = System.Diagnostics.Process.GetProcessById(Convert.ToInt32(pid));
                var handle = WindowSnap.GetParent(a.MainWindowHandle);
                
                Point minpoint = WindowSnap.getUpperLeftPoint(handle);

                if (minpoint == null)
                {
                    Console.WriteLine("Wrong Handle (note:handle must be entered as decimal (hex is not acceptable)");
                    return null;
                }

                Size windowSize = WindowSnap.geWindowSize(handle);

                Point maxpoint = new Point(minpoint.X + windowSize.Width, minpoint.Y + windowSize.Height);

                boxDetails boxDetail = new boxDetails((float)minpoint.X, (float)minpoint.Y, (float)maxpoint.X, (float)maxpoint.Y);

                if (boxDetail == null)
                {
                    Console.WriteLine("No visiable windows has captured");
                }
                else
                {
                    Console.WriteLine("PID: " + pid + "; Windows Box Location:" + boxDetail.ToString());
                }
                return boxDetail;
            }
            catch
            {
                Console.WriteLine("Wrong Handle (note:handle must be entered as decimal (hex is not acceptable)");
                return null;
            }
        }

        public static boxDetails getForegroundWindowBox()
        {
            try
            {
                var handle = WindowSnap.GetForegroundWindow();

                Point minpoint = WindowSnap.getUpperLeftPoint(handle);

                if (minpoint == null)
                {
                    Console.WriteLine("Wrong Handle (note:handle must be entered as decimal (hex is not acceptable)");
                    return null;
                }

                Size windowSize = WindowSnap.geWindowSize(handle);

                Point maxpoint = new Point(minpoint.X + windowSize.Width, minpoint.Y + windowSize.Height);

                boxDetails boxDetail = new boxDetails((float)minpoint.X, (float)minpoint.Y, (float)maxpoint.X, (float)maxpoint.Y);

                if (boxDetail == null)
                {
                    Console.WriteLine("No visiable windows has captured");
                }
                else
                {
                    Console.WriteLine("PID: " + WindowSnap.GetForegroundWindowPid().ToString() + "; Windows Box Location:" + boxDetail.ToString());
                }
                return boxDetail;
            }
            catch
            {
                Console.WriteLine("Wrong Handle (note:handle must be entered as decimal (hex is not acceptable)");
                return null;
            }
        }

        public static string CreateMD5(string input)
        {
            if (input == null || input.Equals(String.Empty))
            {
                return String.Empty;
            }
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static int checkValidOfflineLogin(string userName)
        {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software\\Wow6432Node\\T-Team");
            try
            {
                string lastLoggedInUserName = key.GetValue("lastLoggedInUser").ToString();
                if (!CreateMD5(userName.Trim() + Const.APPEND_STRING).Equals(lastLoggedInUserName))
                {
                    return -1;
                }
                string loggedInTimeString = key.GetValue("auth").ToString();
                for (int i = 1; i <= 10; i++)
                {
                    if (loggedInTimeString.Equals(CreateMD5(userName.Trim() + i + Const.APPEND_STRING)))
                    {
                        return i;
                    }
                }
            }
            catch (Exception)
            {
                return -1;
            }
            finally
            {
                key.Close();
            }
            return -1;
        }

        public static bool validConnection()
        {
            using (var ping = new Ping())
            {
                try
                {
                    var reply = ping.Send("www.google.com");
                    System.Threading.Thread.Sleep(1000);
                    if (reply != null && reply.Status == IPStatus.Success)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static int checkValidLastUserName(string userName)
        {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software\\Wow6432Node\\T-Team");
            try
            {
                string lastUserLoggedIn = key.GetValue("lastLoggedInUser").ToString();
                if (lastUserLoggedIn.Equals(CreateMD5(userName.Trim() + Const.APPEND_STRING)))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return 0;
            }
            finally
            {
                key.Close();
            }
        }

        public static void writeLastLoggedInUserToRegistry(string userName, int numberOfLoggedinTimes)
        {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software\\Wow6432Node\\T-Team");
            try
            {
                key.SetValue("lastLoggedInUser", CreateMD5(userName + Const.APPEND_STRING));
                key.SetValue("auth", CreateMD5(userName + numberOfLoggedinTimes + Const.APPEND_STRING));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                key.Close();
            }
        }

        public static bool writeLoggedInUserToFile(loginDetail login, userDetail user)
        {
            try
            {
                if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team"))
                {
                    Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team");
                }
                string line = CreateMD5(login.Username + login.Password + Const.APPEND_STRING) + Environment.NewLine;
                line = line + user.ExpiredDate + Environment.NewLine;
                line = line + CreateMD5(user.ExpiredDate + Const.APPEND_STRING);
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\loggedInUser.dat", line);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static userModel checkOfflineLoginFromFile(loginDetail user)
        {
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\loggedInUser.dat"))
            {
                userModel returnUser = new userModel();
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\T-Team\\loggedInUser.dat");
                    string line;
                    string expiriedDate = "";
                    int countLine = 0;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (countLine == 0)
                        {
                            if (!line.Equals(CreateMD5(user.Username + user.Password + Const.APPEND_STRING)))
                            {
                                return null;
                            }
                        }
                        if (countLine == 1)
                        {
                            expiriedDate = line.Trim();
                        }
                        if (countLine == 2)
                        {
                            if (line.Trim().Equals(CreateMD5(expiriedDate + Const.APPEND_STRING)))
                            {
                                try
                                {
                                    returnUser.userName = user.Username;
                                    returnUser.expiredDate = DateTime.Parse(expiriedDate);
                                    returnUser.userEmail = "N/A";
                                    returnUser.userPhone = "N/A";
                                    return returnUser;
                                }
                                catch (Exception)
                                {
                                    return null;
                                }
                            }
                        }
                        if (countLine > 2)
                        {
                            return null;
                        }
                        countLine++;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    if (sr != null)
                    {
                        sr.Close();
                    }
                }
                return null;
            }
            else
            {
                return null;
            }
        }

        public static bool matchLoggedInUserToRegistry(userDetail user)
        {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software\\Wow6432Node\\T-Team");
            try
            {
                if (key.GetValue("lastLoggedInUser").ToString().Equals(CreateMD5(user.OwnerName + Const.APPEND_STRING)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                key.Close();
            }
        }

        public static void writeRegistrySetInitialKeyForTheFirstTime()
        {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software\\Wow6432Node\\T-Team");
            try
            {
                key.SetValue("setMP", true);
            }
            catch (Exception)
            {
            }
            finally
            {
                key.Close();
            }
        }

        public static bool readRegistryIfInitialKeyIsSet()
        {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software\\Wow6432Node\\T-Team");
            try
            {
                if (key.GetValue("setMP").ToString().ToLower().Equals("true"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                key.Close();
            }

        }

        public static bool IsAdministrator()
        {
            WindowsIdentity current = WindowsIdentity.GetCurrent();
            WindowsPrincipal windowsPrincipal = new WindowsPrincipal(current);
            if (windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator))
            {
                return true;
            }
            return false;
        }

        public static loginResult checkLoginInfo(loginDetail loginForm)
        {
            if (loginForm.Username == null || loginForm.Password == null)
            {
                return new loginResult(new userModel(), false);
            }
            //Encrypt
            loginForm.EncryptLoginInformation();

            var url = ComponentDescriptionText.URL_LOGIN_API + loginForm.Username + @"&" + loginForm.Password;
            try
            {
                userModel userLoginResult = _download_serialized_json_data<userModel>(url);
                if (userLoginResult.userName == null ||
                    userLoginResult.userEmail == null
                    )
                {
                    return new loginResult(userLoginResult, false);
                }
                return new loginResult(userLoginResult, true);
            }
            catch
            {
            }
            return new loginResult(new userModel(), false);
        }

        private static T _download_serialized_json_data<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;

                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception)
                {
                    Console.WriteLine("Server Not Working");
                }

                T result;
                try
                {
                    Console.WriteLine(json_data);
                    Console.WriteLine(json_data.ToString()[0].ToString());
                    if (json_data.ToString()[0].ToString().Equals("["))
                    {
                        json_data = json_data.Substring(1, json_data.Length - 2);
                    }
                    result = JsonConvert.DeserializeObject<T>(json_data);

                    return result;
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.Message);
                    return new T();
                }
            }

        }
        
        public static void InitCensorForm()
        {
            configHandler.readConfigurationFromFile();   
            try
            {
                censorBgColor = ColorTranslator.FromHtml(ConfigurationHandle.Config.CensorBgColor);
                censorMax = ConfigurationHandle.Config.MaximumCensoredObject;
                if (!ConfigurationHandle.Config.CensorBgImageEnable)
                {
                    cen.initForm(censorMax, censorBgColor);
                }
                else
                {
                    censorImgName = ConfigurationHandle.Config.CensorBgImageName;
                    FileStream stream = new FileStream(System.IO.Directory.GetCurrentDirectory() + "\\img\\" + censorImgName.Trim(), FileMode.Open, FileAccess.Read);
                    censorImg = Image.FromStream(stream);
                    stream.Close();
                    cen.initForm(censorImg, censorMax, censorBgColor);
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                cen.initForm();
            }
        }

        public static void InitCensorForm(int userType)
        {
            configHandler.readConfigurationFromFileByUserType(userType);   
            censorBgColor = ColorTranslator.FromHtml(ConfigurationHandle.Config.CensorBgColor);
            censorMax = ConfigurationHandle.Config.MaximumCensoredObject;
            try
            {
                if (!ConfigurationHandle.Config.CensorBgImageEnable)
                {
                    cen.initForm(censorMax, censorBgColor);
                }
                else
                {
                    censorImgName = ConfigurationHandle.Config.CensorBgImageName;
                    FileStream stream = new FileStream(System.IO.Directory.GetCurrentDirectory() + "\\img\\" + censorImgName.Trim(), FileMode.Open, FileAccess.Read);
                    censorImg = Image.FromStream(stream);
                    stream.Close();
                    cen.initForm(censorImg, censorMax, censorBgColor);
                }
                
               
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                cen.initForm();
            }
        }

        /*
         * 0: No result
         * 1: Up to date
         * 2: Out of date
        */
        public static appVersionResult checkAppVersion()
        {
            //Encrypt
            string currentVersion = ComponentDescriptionText.CURRENT_APP_VERSION;

            var url = ComponentDescriptionText.URL_APP_VERSION_API;
            try
            {
                appVersionModel siteAppVersionModel = _download_serialized_json_data<appVersionModel>(url);
                if (siteAppVersionModel.appVersionNumber == null)
                {

                    return new appVersionResult(siteAppVersionModel, 0);
                }
                // Compare
                try
                {
                    if (siteAppVersionModel.appVersionNumber.CompareTo(currentVersion) == 0)
                    {
                        return new appVersionResult(siteAppVersionModel, 1);
                    }
                    else
                    {
                        return new appVersionResult(siteAppVersionModel, 2);
                    }
                }
                catch
                {
                    return new appVersionResult(siteAppVersionModel, 0);
                }
            }
            catch
            {
            }
            return new appVersionResult(new appVersionModel(), 0);
        }
    }
}
