﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CFML.Common
{
    public class boxDetailsConvertToRect
    {
        public Rectangle boxDetailsToRect(boxDetails box, Bitmap test)
        {
            return new Rectangle(Convert.ToInt32(box.Xmin * test.Width), Convert.ToInt32(box.Ymin * test.Height),
                                Convert.ToInt32((box.Xmax - box.Xmin) * test.Width), Convert.ToInt32((box.Ymax - box.Ymin) * test.Height));
        }
        public boxDetails RectToBoxDetails(Rectangle rect, Bitmap test)
        {
            boxDetails box = new boxDetails();
            box.Xmin = (float)(rect.X * 1.0 / test.Width);
            box.Ymin = (float)(rect.Y * 1.0 / test.Height);
            box.Xmax = (float)(((rect.Width + rect.X) * 1.0 / test.Width) );
            box.Ymax = (float)(((rect.Height + rect.Y) * 1.0 / test.Height));
            return box;
        }
    }
}
