﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing;
using Emgu.CV.CvEnum;

namespace CFML
{
    public class ObjectTracking
    {
        public static List<Image<Bgr, Byte>> template;
        public static Image<Bgr, Byte> tempFrame;
        public Rectangle matchTemplate(Image<Bgr, Byte> source, Image<Bgr, Byte> template)
        {
            Rectangle rect = new Rectangle(0, 0, 0, 0);
            using (Image<Gray, float> result = source.MatchTemplate(template, TemplateMatchingType.CcoeffNormed))
            {
                double[] minValues, maxValues;
                Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);
                Console.WriteLine(maxValues[0]);
                if (maxValues[0] > 0.6)
                {
                    rect = new Rectangle(maxLocations[0].X, maxLocations[0].Y, template.Cols, template.Rows);
                    var temp = source.Copy();
                    temp.ROI = rect;
                    template = temp;
                }
            }

            return rect;
        }
    }
}
