﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFML.Common
{
    public class loginResult
    {
        public bool result { get; set; }
        public userModel user { get; set; }

        public loginResult()
        {

        }

        public loginResult(userModel user, bool result)
        {
            this.user = user;
            this.result = result;
        }
    }
}
