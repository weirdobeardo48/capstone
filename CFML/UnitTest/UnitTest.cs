﻿using CFML.Common;
using CFML.GUIDesign;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;

namespace UnitTest
{
    [TestFixture]
    public class UnitTest
    {

        #region Common.checkLoginInfo
        [Test]
        public void test_checkLoginInfo_CorrectAccount()
        {
            if (!Common.validConnection())
            {
                NUnit.Framework.Assert.Inconclusive("Internet Connection Problem!");
            }

            loginDetail form = new loginDetail("useraccount1", "T-team123");

            loginResult login = Common.checkLoginInfo(form);

            NUnit.Framework.Assert.AreEqual(true, login.result);
        }

        [Test]
        public void test_checkLoginInfo_WrongPassword()
        {
            if (!Common.validConnection())
            {
                NUnit.Framework.Assert.Inconclusive("Internet Connection Problem!");
            }

            loginDetail form = new loginDetail("useraccount1", "wrongPassword");

            loginResult login = Common.checkLoginInfo(form);

            NUnit.Framework.Assert.AreEqual(false, login.result);
        }

        [Test]
        public void test_checkLoginInfo_NotExistedAccount()
        {
            if (!Common.validConnection())
            {
                NUnit.Framework.Assert.Inconclusive("Internet Connection Problem!");
            }

            loginDetail form = new loginDetail("unknownAccount", "password");

            loginResult login = Common.checkLoginInfo(form);

            NUnit.Framework.Assert.AreEqual(false, login.result);
        }

        [Test]
        public void test_checkLoginInfo_NullLoginInfo()
        {
            if (!Common.validConnection())
            {
                NUnit.Framework.Assert.Inconclusive("Internet Connection Problem!");
            }

            loginDetail form = new loginDetail(null, null);

            loginResult login = Common.checkLoginInfo(form);

            NUnit.Framework.Assert.AreEqual(false, login.result);
        }

        [Test]
        public void test_checkLoginInfo_EmptyLoginInfo()
        {
            if (!Common.validConnection())
            {
                NUnit.Framework.Assert.Inconclusive("Internet Connection Problem!");
            }

            loginDetail form = new loginDetail("", "");

            loginResult login = Common.checkLoginInfo(form);

            NUnit.Framework.Assert.AreEqual(false, login.result);
        }
        #endregion

        #region Common.validConnection
        [Test]
        public void test_validConnection()
        {
            var actual = Common.validConnection();
            var expected = NetworkInterface.GetIsNetworkAvailable();

            NUnit.Framework.Assert.AreEqual(expected, actual);
        }
        #endregion

        #region Common.checkAppVersion
        [Test]
        public void test_checkAppVersion()
        {
            var actual = Common.checkAppVersion();

            string appVersionNumber = ComponentDescriptionText.CURRENT_APP_VERSION;

            if (actual.updateStatus == 0)
            {
                NUnit.Framework.Assert.Inconclusive("Problem when checking app version");
            }

            // Up to date
            if (appVersionNumber.Equals(actual.appVersionNumber))
            {
                NUnit.Framework.Assert.AreEqual(1, actual.updateStatus);
            }
            // Out of date
            else
            {
                NUnit.Framework.Assert.AreEqual(2, actual.updateStatus);
            }
        }
        #endregion

        #region Common.CreateMD5
        [Test]
        public void test_CreateMD5_InputStringOnly()
        {
            var actual = Common.CreateMD5("testString");
            var expected = "536788F4DBDFFEECFBB8F350A941EEA3";
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [Test]
        public void test_CreateMD5_InputEmpty()
        {
            var actual = Common.CreateMD5(String.Empty);
            var expected = String.Empty;
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [Test]
        public void test_CreateMD5_InputNull()
        {
            var actual = Common.CreateMD5(null);
            var expected = String.Empty;
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [Test]
        public void test_CreateMD5_InputStringNumberOnly()
        {
            var actual = Common.CreateMD5("0265465132");
            var expected = "BEACF8577EB4F6E14FABF485BD00D594";
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [Test]
        public void test_CreateMD5_InputStringMix()
        {
            var actual = Common.CreateMD5("asd*!@#&cnxzkj1654dsahu261!@#!@");
            var expected = "073E798F57E9BEC3329255C4029E0E64";
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }
        #endregion

        #region Init User Type by using Expired Date
        /* User Type Denotation
         * 0 = Guest
         * 1 = Normal
         * 2 = VIP
         */

        [Test]
        public void test_InitUserType_Guest()
        {
            var user = new userDetail();

            var actual = 0;

            NUnit.Framework.Assert.AreEqual(user.UserType, actual);
        }

        [Test]
        public void test_InitUserType_Normal()
        {
            var user = new userDetail("Nguyen Xuan Truong", "truongnx@gmail.com", 1, null);

            var actual = 1;

            NUnit.Framework.Assert.AreEqual(user.UserType, actual);
        }

        [Test]
        public void test_InitUserType_VIP()
        {
            DateTime expDate = new DateTime(2020, 12, 30);
            var user = new userDetail("Nguyen Xuan Truong", "truongnx@gmail.com", 2, expDate);

            var actual = 2;

            NUnit.Framework.Assert.AreEqual(user.UserType, actual);
        }
        #endregion

        #region Config User Control - Validate Interval Number
        [Test]
        public void test_validateScanInterValue_IntUnderRange()
        {
            PrivateObject accessor = new PrivateObject(typeof(configUserControl));
            accessor.Invoke("validateScanInterValue", new Object[] {0});
            var actual = Convert.ToBoolean(accessor.Invoke("validateScanInterValue", new Object[] { 0 }));
            var expected = false;
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [Test]
        public void test_validateScanInterValue_IntOverRange()
        {
            PrivateObject accessor = new PrivateObject(typeof(configUserControl));
            accessor.Invoke("validateScanInterValue", new Object[] { 0 });
            var actual = Convert.ToBoolean(accessor.Invoke("validateScanInterValue", new Object[] { 99 }));
            var expected = false;
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [Test]
        public void test_validateScanInterValue_IntInRange()
        {
            PrivateObject accessor = new PrivateObject(typeof(configUserControl));
            accessor.Invoke("validateScanInterValue", new Object[] { 0 });
            var actual = Convert.ToBoolean(accessor.Invoke("validateScanInterValue", new Object[] { 20 }));
            var expected = true;
            NUnit.Framework.Assert.AreEqual(expected, actual);
        }
        #endregion


        #region check Valid Login
        [Test]
        public void test_checkValidOfflineLoginTrue()
        {
            Common.writeLastLoggedInUserToRegistry("useraccount1", 1);
            loginDetail loginDt = new loginDetail();
            loginDt.Username = "useraccount1";
            loginDt.Password = "T-team123";
            userDetail userDt = new userDetail();
            userDt.Email = "N/A";
            string dateTime = "09/09/2018";
            userDt.ExpiredDate = Convert.ToDateTime(dateTime);
            userDt.OwnerName = "useraccount1";
            userDt.UserType = 0;

            Common.writeLoggedInUserToFile(loginDt, userDt);


            NUnit.Framework.Assert.AreEqual(1, Common.checkValidOfflineLogin("useraccount1"));
        }

        [Test]
        public void test_checkValidOfflineLoginFalse()
        {
            Common.writeLastLoggedInUserToRegistry("useraccount1", 11);
            loginDetail loginDt = new loginDetail();
            loginDt.Username = "useraccount1";
            loginDt.Password = "T-team123";
            userDetail userDt = new userDetail();
            userDt.Email = "N/A";
            string dateTime = "09/09/2018";
            userDt.ExpiredDate = Convert.ToDateTime(dateTime);
            userDt.OwnerName = "useraccount1";
            userDt.UserType = 0;

            Common.writeLoggedInUserToFile(loginDt, userDt);


            NUnit.Framework.Assert.AreNotEqual(1, Common.checkValidOfflineLogin("useraccount1"));
        }

        [Test]
        public void test_checkValidLastUserNameTrue()
        {
            Common.writeLastLoggedInUserToRegistry("useraccount1", 1);
            NUnit.Framework.Assert.AreEqual(1,Common.checkValidLastUserName("useraccount1"));       
        }

        [Test]
        public void test_checkValidLastUserNameFalse()
        {
            Common.writeLastLoggedInUserToRegistry("useraccount", 1);
            NUnit.Framework.Assert.AreNotEqual(1, Common.checkValidLastUserName("useraccount1"));
        }

        [Test]
        public void test_checkLoginOfflineFromFileTrue()
        {
            Common.writeLastLoggedInUserToRegistry("useraccount1", 11);
            loginDetail loginDt = new loginDetail();
            loginDt.Username = "useraccount1";
            loginDt.Password = "T-team123";
            userDetail userDt = new userDetail();
            userDt.Email = "N/A";
            string dateTime = "09/09/2018";
            userDt.ExpiredDate = Convert.ToDateTime(dateTime);
            userDt.OwnerName = "useraccount1";
            userDt.UserType = 0;

            Common.writeLoggedInUserToFile(loginDt, userDt);


            NUnit.Framework.Assert.AreNotEqual(null, Common.checkOfflineLoginFromFile(new loginDetail("useraccount1", "T-team123")));
        }

        [Test]
        public void test_checkLoginOfflineFromFileFalse()
        {
            Common.writeLastLoggedInUserToRegistry("useraccount1", 11);
            loginDetail loginDt = new loginDetail();
            loginDt.Username = "useraccount1";
            loginDt.Password = "T-team123";
            userDetail userDt = new userDetail();
            userDt.Email = "N/A";
            string dateTime = "09/09/2018";
            userDt.ExpiredDate = Convert.ToDateTime(dateTime);
            userDt.OwnerName = "useraccount1";
            userDt.UserType = 0;

            Common.writeLoggedInUserToFile(loginDt, userDt);


            NUnit.Framework.Assert.AreEqual(null, Common.checkOfflineLoginFromFile(new loginDetail("useraccount", "T-team123")));
        }

        [Test]
        public void test_matchLoggedInUserToRegistryTrue()
        {
            Common.writeLastLoggedInUserToRegistry("useraccount1", 1);
            userDetail userDt = new userDetail();
            userDt.Email = "N/A";
            string dateTime = "09/09/2018";
            userDt.ExpiredDate = Convert.ToDateTime(dateTime);
            userDt.OwnerName = "useraccount1";
            userDt.UserType = 0;
            NUnit.Framework.Assert.AreEqual(true, Common.matchLoggedInUserToRegistry(userDt));
        }

        [Test]
        public void test_matchLoggedInUserToRegistryFalse()
        {
            Common.writeLastLoggedInUserToRegistry("useraccount", 1);
            userDetail userDt = new userDetail();
            userDt.Email = "N/A";
            string dateTime = "09/09/2018";
            userDt.ExpiredDate = Convert.ToDateTime(dateTime);
            userDt.OwnerName = "useraccount1";
            userDt.UserType = 0;
            NUnit.Framework.Assert.AreNotEqual(true, Common.matchLoggedInUserToRegistry(userDt));
        }
        #endregion

    }
}
